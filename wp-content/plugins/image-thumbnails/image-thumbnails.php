<?php
/*
Plugin Name: Manage Thumbnails
*/
define('MT_PLUGIN_PATH', dirname(__FILE__));
define('MT_PLUGIN_FOLDER', basename(dirname(__FILE__)));
define('MT_PLUGIN_URL', plugins_url().'/'.MT_PLUGIN_FOLDER);

function mt_plugin_menu() {   
    add_menu_page(' Manage Thumbnails',' Manage Thumbnails', 'manage_options', 'manage-thumbnails', 'mt_option_page', 
                            MT_PLUGIN_URL.'/picture-attachment-16.gif');  
} 
add_action('admin_menu', 'mt_plugin_menu');


function mt_option_page(){

$thumbnai_val = get_option( 'thumbnail_type' );

if(isset($_POST['save'])){
	if(is_null($thumbnai_val)) {
		add_option( 'thumbnail_type', $_POST['thumbnail_type'] );		
	} else {
		update_option( 'thumbnail_type', $_POST['thumbnail_type'] );
	}
	$thumbnai_val = $_POST['thumbnail_type'];	
}

	
?>
<div class="wrap">
	<div><h2>Manage Thumbnails</h2></div>
    
    
    <form method="post">
    	<p><input type="radio" value="0" name="thumbnail_type" id="t_0" <?php if($thumbnai_val==0):?>checked="checked"<?php endif; ?> /> <label for="t_0">Black and White to Colored effect upon rollover.</label></p>
        <p><input type="radio" value="1" name="thumbnail_type" id="t_1" <?php if($thumbnai_val==1):?>checked="checked"<?php endif; ?> /> <label for="t_1">Keep Black and White</label></p>
        <p><input type="radio" value="2" name="thumbnail_type" id="t_2" <?php if($thumbnai_val==2):?>checked="checked"<?php endif; ?> /> <label for="t_2">Keep Colored</label></p>
        <input type="submit" value="Save" class="button action" name="save">
    </form>
    
</div>
<?php
}

?>