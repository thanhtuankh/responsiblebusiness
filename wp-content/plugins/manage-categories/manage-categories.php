<?php
/*
Plugin Name: Manage Categories
*/
define('MG_PLUGIN_PATH', dirname(__FILE__));
define('MG_PLUGIN_FOLDER', basename(dirname(__FILE__)));
define('MG_PLUGIN_URL', plugins_url().'/'.MG_PLUGIN_FOLDER);

function mg_activate(){
    global $wpdb;
    $table_name = $wpdb->prefix . "managecategories";
     if ($wpdb->get_var('SHOW TABLES LIKE '. $table_name) != $table_name){
        $sql = 'CREATE TABLE ' . $table_name . '( 
            id INTEGER(10) UNSIGNED AUTO_INCREMENT,            
            category_ids text,			          
            PRIMARY KEY (id) )
            CHARACTER SET utf8 COLLATE utf8_general_ci';
        
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);        
    }   
}
register_activation_hook(__FILE__,'mg_activate');

function mg_get_categories(){
	global $wpdb;		
	$table_name = $wpdb->prefix ."managecategories";
	$alldata  = $wpdb->get_results("SELECT * from ".$wpdb->prefix ."managecategories LIMIT 1", ARRAY_A);  	
	$sel_cat = array();
	if(isset($alldata[0])){
		$sel_cat = explode(',', $alldata[0]['category_ids']);
	}
	return $sel_cat;
}


function mg_plugin_menu() {   
    add_menu_page('Manage Categories','Manage Categories', 'manage_options', 'manage-tour-slide', 'mg_option_page', 
                            MG_PLUGIN_URL.'/check-mark-3-16.png');  
} 
add_action('admin_menu', 'mg_plugin_menu');

function mg_option_page(){	
	global $wpdb;		
	$table_name = $wpdb->prefix ."managecategories";
	$alldata  = $wpdb->get_results("SELECT * from ".$wpdb->prefix ."managecategories LIMIT 1", ARRAY_A);  	
	$sel_cat = array();
	if(isset($alldata[0])){
		$sel_cat = explode(',', $alldata[0]['category_ids']);
	}	
?>
<div class="wrap">
	<style>
		.cat:hover, .sub_cat:hover{
			color:#999999;
		}
		
		.sub_cat{
			padding-left:20px;
		}
		
		.cat label, .sub_cat label{
			padding-left:5px;
		}
		
			
	</style>
    <script>
	jQuery(function ($) {			
		$('.sub_cat input[type="checkbox"]').click(function(){	
			if($(this).is(':checked')){
				$("#"+$(this).attr("data-parent")).attr('checked','checked');
			}				
		});		
	});
	</script>
	<div><h2>Manage Categories</h2></div>
    <form method="post">
    <?php
	$str = "";
    if(isset($_POST['save'])){
		
		if(count($_POST['ids'])>0){
			$str = implode(',', $_POST['ids']);		
			$data = array(			     
				'category_ids' => $str
			);  
			
			if(!isset($alldata[0]['id'])){		
				$wpdb->insert($table_name, $data);
			} else {		
				 $wpdb->update($table_name, $data, array('id' =>  $alldata[0]['id']));
			}
			
			$sel_cat = array();
			$sel_cat = $_POST['ids'];			
		} elseif(isset($alldata[0]['id'])) {
			$wpdb->query("DELETE FROM {$table_name} WHERE id=".$alldata[0]['id']); 
			$sel_cat = array();
		}
	}
	?>
    
    <?php
    $categories = get_all_category_ids();	
	$categories_p = array();
	$categories_c = array();
	
	foreach($categories as $c){		
		$cat = get_category( $c );						
		if($cat->category_parent == 0){
			$categories_p[$cat->term_id] = $cat->name;
		}else {
			$categories_c[$cat->category_parent][$cat->term_id] = $cat->name;
		}
				
	}
	asort($categories_p);
	
	foreach($categories_p as $key => $value){
		$sel ="";
		if(in_array($key, $sel_cat)){
			$sel = 'checked="checked"';
		}
		?>
        <p class="cat"><input type="checkbox" name="ids[]" id="ch_<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo $sel; ?> /> <label for="ch_<?php echo $key; ?>"><?php echo $value; ?>
        <?php
        $cat = get_category( $key );
		if(isset($cat)){
			echo "(".$cat->count.")";	
		}				
		?>
        </label>
        </p>
        <?php
		if(isset($categories_c[$key])){
			$ch= $categories_c[$key];
			asort($ch);
			foreach($ch as $key2 => $value2){
			$sel2 ="";
			if(in_array($key2, $sel_cat)){
				$sel2 = 'checked="checked"';
			}
			?>
            <p class="sub_cat"><input type="checkbox" name="ids[]" id="ch_<?php echo $key2; ?>" value="<?php echo $key2; ?>" <?php echo $sel2; ?> data-parent="ch_<?php echo $key; ?>" /> <label for="ch_<?php echo $key2; ?>"><?php echo $value2; ?>
             <?php
			$cat = get_category( $key2 );
			if(isset($cat)){
				echo "(".$cat->count.")";	
			}				
			?>
            </label></p>
			<?php
			}
		}
	}	
	?>
   <input type="submit" value="Save" class="button action" name="save">
   </form>
</div>
<?php
}

?>