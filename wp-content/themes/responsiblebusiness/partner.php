<?php

/*

Template Name: partner-template

*/

?>

<?php get_header(); ?>



<style>

.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar{

 background:#fff; /* rgba fallback */

 background:rgba(1,174,240,0.75);

 filter:"alpha(opacity=75)"; -ms-filter:"alpha(opacity=75)"; /* old ie */

}

.mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar{

 background:rgba(1,174,240,0.85);

 filter:"alpha(opacity=85)"; -ms-filter:"alpha(opacity=85)"; /* old ie */

}

.mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,

.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar{

 background:rgba(1,174,240,0.9);

 filter:"alpha(opacity=90)"; -ms-filter:"alpha(opacity=90)"; /* old ie */

}

</style>

<script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery.mCustomScrollbar.concat.min.js'></script>

<link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/jquery.mCustomScrollbar2.css' rel='stylesheet' media='screen' />
<?php /*?>
<script>

$(document).ready(function() {



	//rotation speed and timer

	var speed = 3000;

	var run = setInterval('rotate()', speed);	

	

	//grab the width and calculate left value

	var item_width = $('#slides li').outerWidth(); 

	var left_value = item_width * (-1); 

        

    //move the last item before first item, just in case user click prev button

	$('#slides li:first').before($('#slides li:last'));

	

	//set the default item to the correct position 

	$('#slides ul').css({'left' : left_value});



    //if user clicked on prev button

	$('#prev').click(function() {



		//get the right position            

		var left_indent = parseInt($('#slides ul').css('left')) + item_width;



		//slide the item            

		$('#slides ul:not(:animated)').animate({'left' : left_indent}, 200,function(){    



            //move the last item and put it as first item            	

			$('#slides li:first').before($('#slides li:last'));           



			//set the default item to correct position

			$('#slides ul').css({'left' : left_value});

		

		});



		//cancel the link behavior            

		return false;

            

	});



 

    //if user clicked on next button

	$('#next').click(function() {

		

		//get the right position

		var left_indent = parseInt($('#slides ul').css('left')) - item_width;

		

		//slide the item

		$('#slides ul:not(:animated)').animate({'left' : left_indent}, 200, function () {

            

            //move the first item and put it as last item

			$('#slides li:last').after($('#slides li:first'));                 	

			

			//set the default item to correct position

			$('#slides ul').css({'left' : left_value});

		

		});

		         

		//cancel the link behavior

		return false;

		

	});        

	

	//if mouse hover, pause the auto rotation, otherwise rotate it

	$('#slides').hover(

		

		function() {

			clearInterval(run);

		}, 

		function() {

			run = setInterval('rotate()', speed);	

		}

	); 

        

});



//a simple function to click next link

//a timer will call this function, and the rotation will begin :)  

function rotate() {

	$('#next').click();

}

        

        

        

</script>

<script>

$(document).ready(function() {



	//rotation speed and timer

	var speed = 3000;

	var run = setInterval('rotate_2()', speed);	

	

	//grab the width and calculate left value

	var item_width = $('#slides_2 li').outerWidth(); 

	var left_value = item_width * (-1); 

        

    //move the last item before first item, just in case user click prev button

	$('#slides_2 li:first').before($('#slides_2 li:last'));

	

	//set the default item to the correct position 

	$('#slides_2 ul').css({'left' : left_value});



    //if user clicked on prev button

	$('#prev_2').click(function() {



		//get the right position            

		var left_indent = parseInt($('#slides_2 ul').css('left')) + item_width;



		//slide the item            

		$('#slides_2 ul:not(:animated)').animate({'left' : left_indent}, 200,function(){    



            //move the last item and put it as first item            	

			$('#slides_2 li:first').before($('#slides_2 li:last'));           



			//set the default item to correct position

			$('#slides_2 ul').css({'left' : left_value});

		

		});



		//cancel the link behavior            

		return false;

            

	});



 

    //if user clicked on next button

	$('#next_2').click(function() {

		

		//get the right position

		var left_indent = parseInt($('#slides_2 ul').css('left')) - item_width;

		

		//slide the item

		$('#slides_2 ul:not(:animated)').animate({'left' : left_indent}, 200, function () {

            

            //move the first item and put it as last item

			$('#slides_2 li:last').after($('#slides_2 li:first'));                 	

			

			//set the default item to correct position

			$('#slides_2 ul').css({'left' : left_value});

		

		});

		         

		//cancel the link behavior

		return false;

		

	});        

	

	//if mouse hover, pause the auto rotation, otherwise rotate it

	$('#slides_2').hover(

		

		function() {

			clearInterval(run);

		}, 

		function() {

			run = setInterval('rotate_2()', speed);	

		}

	); 

        

});



//a simple function to click next link

//a timer will call this function, and the rotation will begin :)  

function rotate_2() {

	$('#next_2').click();

}

        

        

        

</script>



<script>

$(document).ready(function() {



	//rotation speed and timer

	var speed = 3000;

	var run = setInterval('rotate_3()', speed);	

	

	//grab the width and calculate left value

	var item_width = $('#slides_3 li').outerWidth(); 

	var left_value = item_width * (-1); 

        

    //move the last item before first item, just in case user click prev button

	$('#slides_3 li:first').before($('#slides_3 li:last'));

	

	//set the default item to the correct position 

	$('#slides_3 ul').css({'left' : left_value});



    //if user clicked on prev button

	$('#prev_3').click(function() {



		//get the right position            

		var left_indent = parseInt($('#slides_3 ul').css('left')) + item_width;



		//slide the item            

		$('#slides_3 ul:not(:animated)').animate({'left' : left_indent}, 200,function(){    



            //move the last item and put it as first item            	

			$('#slides_3 li:first').before($('#slides_3 li:last'));           



			//set the default item to correct position

			$('#slides_3 ul').css({'left' : left_value});

		

		});



		//cancel the link behavior            

		return false;

            

	});



 

    //if user clicked on next button

	$('#next_3').click(function() {

		

		//get the right position

		var left_indent = parseInt($('#slides_3 ul').css('left')) - item_width;

		

		//slide the item

		$('#slides_3 ul:not(:animated)').animate({'left' : left_indent}, 200, function () {

            

            //move the first item and put it as last item

			$('#slides_3 li:last').after($('#slides_3 li:first'));                 	

			

			//set the default item to correct position

			$('#slides_3 ul').css({'left' : left_value});

		

		});

		         

		//cancel the link behavior

		return false;

		

	});        

	

	//if mouse hover, pause the auto rotation, otherwise rotate it

	$('#slides_3').hover(

		

		function() {

			clearInterval(run);

		}, 

		function() {

			run = setInterval('rotate_3()', speed);	

		}

	); 

        

});



//a simple function to click next link

//a timer will call this function, and the rotation will begin :)  

function rotate_3() {

	$('#next_3').click();

}

        

        

        

</script><?php */?>


<div id="header-wrap">

    <?php /*?><div id="header-wrap-img">     

       

         <a href="http://www.responsiblebusiness.com/events/changing-the-rules-of-the-game/">

            <img width="647px" height="280px" class="slidegrayscale" style="width:647px; height:280px; float:left;" src="<?php echo bloginfo('template_directory');?>/forum-images/img_masthead_rbf2012_about1.jpg"></a>

    

         <div id="event-text">

          <div id="event-text-wrap">

             <div class="event-cat">

                 <strong>RESPONSIBLE BUSINESS FORUM<br>

                 <span class="t_white">ON SUSTAINABLE DEVELOPMENT</span> </strong>

             </div>

                <div class="event-details">

                 <div class="t14"><strong>

                     Transformation, Growth and the Green Economy 

                     </strong></div>

                    <div class="t10">

                        <strong>                            

                            <span class="t_white">

                             Singapore<br> 

        25 - 26 November 2013                          </span> 

                        </strong>

                    </div>     

                </div> 

            </div>

         </div> 

    </div><?php */?>    

    <?php include("form_banner.php");?>     

</div>





<div id="content" class="t14 ccontent" style="margin-top:30px;">

    <div class="left" style="width:700px;">

      <h1>Partners</h1>
      
          <?php
	
	//for($i=0;$i<3;$i++)
	$i=0;
	$categories=  get_categories('child_of=69'); 
  foreach ($categories as $category)
	{
	
	?>

      <h2 style="font-size: 12pt; color:#00ccff;"><?php echo $category->cat_name; ?></h2>
      <div class="speakers">

      

		<?php

			query_posts('cat='.$category->cat_ID.'&orderby=title&order=ASC'); 

			if (have_posts()) : 	

					

			$post_count_mc=0; while (have_posts()) : the_post(); $post_count_mc++;

		?>

                    

      <div class="boxs  height_bx_290"> 

         <?php  //the_post_thumbnail('org_partn_thumb'); ?>

         <div style="width:110px;">

            <?php //if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail'); endif; ?>
            <?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail',NULL, 'org_partn_thumb', NULL, TRUE); endif; ?>
            <br />

          

          <?php // if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail_2'); endif; ?>
<?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail_2',NULL, 'org_partn_thumb', NULL, TRUE); endif; ?>
            

            </div>

          <div class="desc desc_partnr">

           

          <?php

	 	$content = apply_filters('the_content', $post->post_content); 

	  	echo $content; 

	?>



          

          </div>

        </div>

        <?php endwhile; endif; wp_reset_query();?>

        <!--boxs ends here-->



        

        <div class="clear"></div>

      </div>
      
      
      <?php
	}
	?>
      
      <?php /*?>
                      <?php
        $partner_count=get_posts('cat=68');
if ($partner_count) {
//found posts

?>
       <h2 style="font-size: 12pt; color:#00ccff;">Sponsors </h2>

      

      

      <div class="speakers">

		<?php

			query_posts('cat=68&orderby=title&order=ASC'); 

			if (have_posts()) : 	

					

			$post_count_mc=0; while (have_posts()) : the_post(); $post_count_mc++;

		?>      

      <div class="boxs  height_bx_290"> 

            <?php  //the_post_thumbnail('org_partn_thumb'); ?>

            <div style="width:110px;">

           <?php //if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail'); endif; ?>
            <?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail',NULL, 'org_partn_thumb', NULL, TRUE); endif; ?>
            <br />

          

          <?php // if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail_2'); endif; ?>
<?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail_2',NULL, 'org_partn_thumb', NULL, TRUE); endif; ?>

            

            </div>

          <div class="desc desc_partnr">

           	 <?php

                $content = apply_filters('the_content', $post->post_content); 

                echo $content; 

            ?>

        

          </div>

        </div>

        

         <?php endwhile; endif; wp_reset_query();?>

        

        

   

        

        <div class="clear"></div>

      </div>

      <?php
}
?>

      

       <h2 style="font-size: 12pt; color:#00ccff;">Forum Partners </h2>

      

      

      <div class="speakers">

		<?php

			query_posts('cat=64&orderby=title&order=ASC'); 

			if (have_posts()) : 	

					

			$post_count_mc=0; while (have_posts()) : the_post(); $post_count_mc++;

		?>      

      <div class="boxs  height_bx_290"> 

            <?php  //the_post_thumbnail('org_partn_thumb'); ?>

            <div style="width:110px;">

           <?php //if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail'); endif; ?>
            <?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail',NULL, 'org_partn_thumb', NULL, TRUE); endif; ?>
            <br />

          

          <?php // if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail_2'); endif; ?>
<?php if (class_exists('MultiPostThumbnails')) : MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'small_post_thumbnail_2',NULL, 'org_partn_thumb', NULL, TRUE); endif; ?>

            

            </div>

          <div class="desc desc_partnr">

           	 <?php

                $content = apply_filters('the_content', $post->post_content); 

                echo $content; 

            ?>

        

          </div>

        </div>

        

         <?php endwhile; endif; wp_reset_query();?>

        

        

   

        

        <div class="clear"></div>

      </div>

      

      
<?php */?>
      

      

      

    </div>

    <?php include("forum_rightbox.php"); ?>    

    <div class="clear"></div>

  </div>



<script src="forum-js/jquery.mCustomScrollbar.concat.min.js"></script>

	<script>

		(function($){

			$(window).load(function(){

				$(".desc").mCustomScrollbar();

			});

		})(jQuery);

	</script>









<?php get_footer(); ?>