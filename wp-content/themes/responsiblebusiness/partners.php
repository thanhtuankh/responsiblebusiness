<?php if(count($event['partners_logo'])>0): ?>
    <div id="partners">
        <div class="line-separator" style="margin-top:-2px;"></div>
        <h3 class="section-title">Partners</h3>
        <div class="line-separator"></div>
        <ul>
        <?php foreach($event['partners_logo'] as $key => $logo): ?>    
        	<?php $url = $event['partners_logo_data'][$key]['alt'];	?>        
            <li>
            	<?php if($url!=""): ?>
                	<a href="<?php echo $url; ?>">
                <?php endif; ?>
            		<img src="<?php echo $logo[0]; ?>" width="180" />
                <?php if($url!=""): ?>
                	</a>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
        </ul>
    </div>    	
<?php endif; ?>