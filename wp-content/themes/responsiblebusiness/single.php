<?php get_header(); ?>
<?php 
$category = get_the_category();
$hierarchy = array_reverse( get_ancestors( $category[0]->term_id, 'category' ) );
$hierarchy[] = $category[0]->term_id;

if($hierarchy[0] == 122): // gallery post
include "bTemplate/gallerySingle.php";

elseif( ($category[0]->term_id==160) || ($category[1]->term_id==160) ): // 158 young leader programs
include "bTemplate/youngLeader.php";
?>

<?php else: 
include "bTemplate/generalSingle.php";
endif;
?>
