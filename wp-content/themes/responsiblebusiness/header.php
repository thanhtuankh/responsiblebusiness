<!DOCTYPE html>
<?php

if(!is_home()){
$id=get_the_id(); 
$thum_url=wp_get_attachment_url( get_post_thumbnail_id($idd));


$cates = get_the_category($id);
$cate = $cates->term_id;
$desc = get_field('large_text',$id);
$descWebinar = get_field('small_text',$id);
$pageCommitLink = get_page_link($id);

$pageCommunity = get_category_link($cates[0]->term_id);
$content_post = get_post($id);
$body = $content_post->post_content;
$isPost = is_single();
}
?>


<html>

    <head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      
    <!--   <meta property="og:url" content="<?php echo get_home_url(); ?>" />

    <meta property="og:title" content="<?php bloginfo('name'); ?>" />

    <meta property="og:image" content="http://responsiblebusiness.com/wp-content/themes/responsiblebusiness/images/logo_200.png" />

    <meta property="og:description" content="<?php echo get_bloginfo ( 'description' ); ?>" /> -->
    <?php if( ($cates[0]->term_id ==152) ||($pageCommitLink=='http://www.responsiblebusiness.com/commitments')) : ?>

    <meta property="og:description" content="<?php echo $desc; ?>" />
   
    <meta property="og:image" content=" <?php echo $thum_url; ?>" />

        <meta name="twitter:card" content="photo" />
    <meta name="twitter:site" content="@Responsible_Biz" />
    <meta name="twitter:title" content="Responsible_Biz" />
    <meta name="twitter:image" content="<?php echo $thum_url; ?>" />
    <meta name="twitter:url" content="http://www.responsiblebusiness.com/commitments" />
    <meta name="twitter:domain" content="responsiblebusiness.com">
    <meta id="tw_description" name="twitter:description" content="Delivering the #SDGs with @Responsible_Biz">


    <?php endif; ?>

    <?php if(($cates[1]->term_id ==154)||($cates[0]->term_id ==154)): //TODO L Webinar?>
   
    <meta property="og:description" content="<?php echo $descWebinar; ?>" />
    <meta property="og:image" content="<?php echo $thum_url; ?>" />
    <meta name="twitter:card" content="photo" />
    <meta name="twitter:site" content="@Responsible_Biz" />
    <meta name="twitter:title" content="Responsible_Biz" />
    <meta name="twitter:image" content="<?php echo $thum_url; ?>" />
    <meta name="twitter:url" content="http://bit.ly/1IcT2Bh" />
    <meta name="twitter:domain" content="responsiblebusiness.com">
    <meta id="tw_description" name="twitter:description" content="Webinars">
    <?php endif; ?>

        <?php if(($cates[1]->term_id ==108)||($cates[0]->term_id ==108)): //TODO L Webinar?>
    <meta property="og:description" content="<?php echo strip_tags($body); ?>" />
    <meta property="og:image" content="<?php echo $thum_url; ?>" />
    <meta name="twitter:card" content="photo" />
    <meta name="twitter:site" content="@Responsible_Biz" />
    <meta name="twitter:title" content="Responsible_Biz" />
    <meta name="twitter:image" content="<?php echo $thum_url; ?>" />
    <meta name="twitter:domain" content="responsiblebusiness.com">
    <meta id="tw_description" name="twitter:description" content="<?php echo strip_tags($body); ?>">
    <?php endif; ?>


    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo bloginfo('template_directory');?>/images/favicon.ico">

    <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery-1.8.2.min.js'></script>

    

    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <script src="<?php echo bloginfo('template_directory');?>/js/fwslider.js"></script>



<?php include("pop-video-support.php"); ?>



                               



    <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jQuery.BlackAndWhite.min.js'></script>

    <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/easySlider1.7.js?v=5'></script>

    <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/functions.js?v=18'></script>

    <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery.mCustomScrollbar.concat.min.js'></script>

    <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery.simplemodal.js'></script>

    <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/flipclock.js'></script>

    <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/lstyle.js'></script>

    <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/myjs.js'></script>

    <script type='text/javascript' src='http://s7.addthis.com/js/300/addthis_widget.js'></script>


    <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/jquery.mCustomScrollbar.css' rel='stylesheet' media='screen' />

    <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/basic.css' rel='stylesheet' media='screen' />

    <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/forumstyle.css' rel='stylesheet' media='screen' />

    <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/flipclock.css' rel='stylesheet' media='screen' />

    <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/screen.css?v=2' rel='stylesheet' media='screen' />

    <link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_directory');?>/css/fwslider.css">

    <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/mystyle.css' rel='stylesheet' media='screen' />

    <link type='text/css' href='http://s7.addthis.com/js/300/addthis_widget.js' rel='stylesheet' media='screen' />

    <!-- news letter pop up starts here -->



    <script type="text/javascript" src="<?php bloginfo('template_directory')?>/js/jquery.reveal.js"></script>



    <!-- news letter pop up ends here -->



    <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>

    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-517642bf5ed47108"></script>

    <script src="http://jwpsrv.com/library/5UsIaq8FEeKsVSIACpYGxA.js"></script>



    <!-- IE6 "fix" for the close png image -->



    <!--[if lt IE 7]>







        <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/basic_ie.css' rel='stylesheet' media='screen' />







        <![endif]-->



    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=30" />

    <title>

    <?php if(is_front_page()): ?>

    <?php bloginfo('name'); ?>

    <?php else: ?>

    <?php wp_title( '', true);?>

    <?php endif; ?>

    </title>

    <?php wp_head(); ?>

    <script type="text/javascript">















  var _gaq = _gaq || [];







  _gaq.push(['_setAccount', 'UA-5927070-1']);







  _gaq.push(['_trackPageview']);















  (function() {







    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;







    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';







    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);







  })();















</script>



    <!-- POPUP STRTAS HERE -->



    <?php /*?> <script src="<?php bloginfo('template_directory')?>/Scripts/jquery-1.4.1.js" type="text/javascript"></script><?php */?>



    <!--    <script src="Scripts/jquery.msgBox.min.js" type="text/javascript"></script>-->



    <script src="<?php bloginfo('template_directory')?>/Scripts/jquery.msgBox.js" type="text/javascript"></script>

    <link href="<?php bloginfo('template_directory')?>/Scripts/msgBoxLight.css" rel="stylesheet" type="text/css" />



    <!--<link href="Scripts/styles.css" rel="stylesheet" type="text/css" />-->



    <script type="text/javascript">











      



    </script>



    <!-- pop up ends here -->

    

    

    

    <script type="text/javascript" charset="utf-8">

      $(function() {

        

 	//Home Learn More Click

 	$('.baseBlock').click(function(e) {

 		e.preventDefault();

 		nextPage = $(this).attr('title');

 		$('html, body').animate({

        	scrollTop: $("#"+nextPage).offset().top

     	}, 500);

 	});

 	//Home Learn More Click

 	$('.baseblock2').click(function(e) {

 		e.preventDefault();

 		nextPage = $(this).attr('title');

 		$('html, body').animate({

        	scrollTop: $("#"+nextPage).offset().top

     	}, 500);

 	});

 	//Home Learn More Click

 	$('.baseblock3').click(function(e) {

 		e.preventDefault();

 		nextPage = $(this).attr('title');

 		$('html, body').animate({

        	scrollTop: $("#"+nextPage).offset().top

     	}, 500);

 	});

 	//Home Learn More Click

 	$('.baseblock4').click(function(e) {

 		e.preventDefault();

 		nextPage = $(this).attr('title');

 		$('html, body').animate({

        	scrollTop: $("#"+nextPage).offset().top

     	}, 500);

 	});

 	//Home Learn More Click

 	$('.baseblock5').click(function(e) {

 		e.preventDefault();

 		nextPage = $(this).attr('title');

 		$('html, body').animate({

        	scrollTop: $("#"+nextPage).offset().top

     	}, 500);

 	});

      });

  </script>

  

		<script type="text/javascript" src="<?php echo bloginfo('template_directory');?>/jquery.reveal.js"></script>

	  	<link rel="stylesheet" href="<?php echo bloginfo('template_directory');?>/reveal.css">

		<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_directory');?>/menu_style.css">

        

             <?php 
if(!is_home()):
?>

<script type="text/javascript">

    var re = '<?php echo $cates[0]->term_id ?>';
    var reLink = '<?php echo $pageCommitLink ?>';
    var pageCommunity = '<?php echo $pageCommunity ?>';
    var isPost = '<?php echo $isPost ?>';
    //alert(re);
    if(re == '152'){
        window.location.replace(reLink);
    }

    <?php endif; ?>

</script>


    </head>



    <body>

<div id="loader" style="display:none;"><img src="<?php echo bloginfo('template_directory');?>/images/ajax-loader.gif" /></div>

<div id="basic-modal-content"></div>

<div id="wrapper">

<div id="header">

        <div class="line-separator"></div>

        <div id="header-left"> <a href="<?php echo get_home_url(); ?>"><img src="<?php echo bloginfo('template_directory');?>/images/logo.png" alt="" /></a> </div>

        <div id="header-right">

        <div id="top">

                <?php /*?><div id="search-form">

                <form role="search" method="get" id="searchform" action="<?php echo get_home_url(); ?>">

                        <label class="upper">

                        <input class="search" type="submit" value="SEARCH" />

                    </label>

                        <input type="text" value="" name="s" id="s" />

                    </form>

            </div><?php */?>

                <ul id="top-menu">
    
                      <li class="b_ss"><a href="<?php echo get_option('social_slide'); ?>" target="_blank"></a></li>
                      <li class="b_in"><a href="<?php echo get_option('social_instar'); ?>" target="_blank"></a></li>
                      <li class="b_yt"><a href="<?php echo get_option('social_yt'); ?>" target="_blank"></a></li>
                      <li class="b_link"><a href="<?php echo get_option('social_linke'); ?>" target="_blank"></a></li>
                      <li class="b_tw"><a href="<?php echo get_option('social_tw'); ?>" target="_blank"></a></li>
                      <li class="b_fb"><a href="<?php echo get_option('social_fb'); ?>" target="_blank"></a></li>

                <!--<li class="menu-items-separator"></li>http://www.linkedin.com/groups/Responsible-Business-4975780-->

            </ul>

            <a href="http://www.globalinitiatives.com/" class="top_lL" target="_blank"><img src="<?php echo bloginfo('template_directory');?>/images/global-initiatives-logo.png" /></a>

            </div>

        <div class="clear"></div>



    </div>

    

        <div class="line-separator" style="margin-top:3px;"></div>

    </div>



        <div class="clear"></div>
