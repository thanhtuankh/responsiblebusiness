<?php

/*

Template Name: Television

*/

?>

<style>
.form_title{display:none !important; }
.awqsf-label-keyword{display:none !important; }
#aqsfformid{border:none !important; }
#awqsf_submit
{
float: right;
margin-top: -30px;
z-index: 15;
margin-right: -1px;
position: relative;
background: #6DCFF6;
border: none;
color: #fff;
height: 30px;
cursor:pointer;
}
/*input#awqsf_keyword {
width: 100%;
height: 30px;
border:1px solid #6DCFF6;
}*/
input#awqsf_keyword {
width: 100%;
height: 32px;
border:1px solid #515151;
}
input#awqsf_keyword{padding-left: 6px;color: #767676;font-size:12px;}
#aqsfformid
{
	margin-top:15px;
}
#searchbox
{
float:right;
margin:0px;
width:22%;
}
.video_cat{float:left;width:64%;margin:15px 0 0;}
/*.video_cat{float:left;width: 89%;margin: 0;}*/
.video_cat a{color:#515050;display: block; margin-bottom: 8px;}
.video_cat a:hover{color:#46A4D5;text-decoration:underline;}
.video_cat_lef{width:91px;color:#46A4D5;float:left;margin:15px 0 0;}
.video_cat_lef a{}
.video_cat_fch{margin-left: 5px;color:#515050;}
.video_cat_fch:hover{color:#46A4D5;}
.video_cat li{width:32.8% !important;}
.cat_r{width:31.8% !important;float: left;margin-bottom: 5px;margin-right: 7px;}
.video_cat li a{color:#515050;}
.video_cat li a:hover{color:#46A4D5;}
.video_cat a{margin-bottom:0px;font-size: 12px;}
.video_cat{width: 78%;}
.video_cat_lef{float:none;width:115px;}
.cat_r{width:32.3% !important;}
.television .videos{margin-top:0 !important;}
.video_cat_fch{text-transform:uppercase;font-weight:bold;margin-left:0;}
.videos.telvsn_vdeo .video-wrap{height:396px;}
.videos.telvsn_vdeo .video-thumb{height:173px;}
.videos.telvsn_vdeo .video-thumb-text{padding: 26px 32px 35px 35px;width: 78%;height: 65px;}
.videos.telvsn_vdeo .t_12.video-thumb-text .video_titl{padding-bottom:13px;text-transform: uppercase;}
.video_cat{margin-top:0 !important;}
#home-videos-cat {float: left;width: 100%;}
#aqsfformid{margin-top:0;}
input#awqsf_keyword{border-radius:2px;-moz-border-radius:2px;-webkit-border-radius:2px;}
/*.video_cat li:nth-child(1){width:65px;color:#46A4D5;}
.video_cat li:nth-child(1) a{}
.video_cat li:nth-child(1){width:36px;}
.video_cat li:nth-child(1) a{color:#515050;}
.video_cat li:nth-child(2){width:17%;}
.video_cat li:nth-child(2) a{color:#515050;}
.video_cat li:nth-child(3){width:22%;}
.video_cat li:nth-child(3) a{color:#515050;}
.video_cat li:nth-child(4){width:20%;}
.video_cat li:nth-child(4) a{color:#515050;}
.video_cat li:nth-child(5){width:22%;}
.video_cat li:nth-child(5) a{color:#515050;}*/
.video_cat li:last-child{margin-right: 0;}
.various{display: inline-block;}
#videos-forum{margin-bottom:6px !important;}
#footer {margin-top: 18px !important;}

.video_cat{width: 74%;}
#searchbox{width: 26%; margin-top: 12px;}
#aqsfformid{margin-right:6px;}
.b-opacity{
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
    background-color: #f2f2f2;
    z-index: 2147483647;
    display: none;
    opacity: 0.3;
}

.video-wrap:hover .b-opacity{display: block;}

@media only screen and (max-width: 1051px){
	
	.video_cat{width: 69%;}
	#searchbox{width: 19%;}
}
@media only screen and (max-width: 963px){
	.video_cat {    width: 65%;}
}
@media only screen and (max-width: 892px){
.video_cat {width: 64%;}
.cat_r {width: 29.8% !important;}

}
@media only screen and (max-width: 826px){
	
	.video_cat{width: 85%;}
	#searchbox {float: none;    width: 54%;}
}
@media only screen and (max-width: 768px){
	.video_cat_lef{width: 113px;}
	.video_cat_lef a {    display: inline-block;    width: auto;}
}
@media only screen and (max-width: 640px){
	.cat_r {    float: none;    margin-bottom: 10px;    width: 100% !important;}
}
@media only screen and (max-width: 479px){
	
	#searchbox {float: none;    width:250px;}
}
</style>
<script>

function load_youtube_videos(value)
{
	
		if(value!="")
			{
				$('#home-videos-cat').html('<img src="<?php echo bloginfo('template_directory');?>/images/ajax-loader.gif" />');
				
				
							
				$.post("<?php bloginfo( 'home' ); ?>/video-youtube-category/", {value:value},
					function(data)
					{

		
							$('#home-videos-cat').html(data);//('');	

				
										  
					});
				
			
			}
			else
			{
				
				$('#home-videos-cat').html('');
			}
}
</script>


<?php get_header(); ?>
<div class="top_smnu"></div>
<div class="clear"></div>
<script>

jQuery(function ($) {

//	loadVideos('videos-tv', 20, 1, 11);

//	loadVideos('videos-forum', 16, 1, 11);

});	

</script>

<div class="con_lef">
<?php include("side_new_menu.php");?>
</div>
<div class="con_rig">
<div id="content" class="television">

	<div id="header-wrap" class="wr-top-banner-video-page">
            <?php $banner_video_page = get_field('top_banner');
                if(!empty($banner_video_page)):
             ?>
            <img class="banner_top_video_page" src="<?php echo $banner_video_page ?>">

            <?php endif; ?>
    <div class="clear"></div>
    </div>

   	<div class="clear"></div>

	<div id="content-left">

        <div class="t14">

            <?php

            $content = apply_filters('the_content', $post->post_content); 

            echo $content;  

            ?>

        </div>

        <div class="clear"></div>        

        

<div class="tel_sec">
<div id="videos"></div>



<div id="home-videos-cat">

	<div class="line-separator"></div>

<?php 


	if(isset($_REQUEST['cat_id']))
						{
							$cat_id=$_REQUEST['cat_id'];
						}
						else
						{
							$cat_id=97;
						}
						
						

$args = array(
	'type'                     => 'youtubevideo',
	'child_of'                 => 75,
	 'orderby'       =>  'term_order',
	 'exclude'                  => '97',
	'hide_empty'               => 0
	/*
	'parent'                   => '',
	'orderby'                  => 'title',
	'order'                    => 'DESC',
	'hide_empty'               => 1,
	'hierarchical'             => 1,
	
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'category',
	'pad_counts'               => false */

); 

$categories = get_categories( $args ); 
?>

<div class="video_cat_lef"  style="display:none">
<a href="<?php echo get_permalink(7);?>?cat_id=97"  class="video_cat_fch <?php if($cat_id==97) { ?> active  <?php } ?>">SHOWING</a>

 <a href="<?php echo get_permalink(7);?>?cat_id=75"  class="video_cat_fch <?php if($cat_id==75) { ?> active  <?php } ?>">All</a>

</div>
<div class="video_cat" style="display:none">

	<?php
	$cont=2;
    foreach($categories as $vide_categorie)
    {
		$cont++;
		?>
		<?php /*?><li><a href="javascript:void(0);" onclick="load_youtube_videos(<?php echo $vide_categorie->term_id;?>);" ><?php echo $vide_categorie->name;?></a></li><?php */?>
       <?php  if(($cont%3)==0): ?>
        <div class="cat_r">
        <?php endif; ?>
        
        <a href="<?php echo get_permalink(7);?>?cat_id=<?php echo $vide_categorie->term_id;?>" <?php if($cat_id==$vide_categorie->term_id) { ?> class="active"  <?php } ?> ><?php echo $vide_categorie->name;?></a>
        
        <?php  if(($cont%3)==2): ?> 
        </div>
         <?php endif; ?>
		<?php	
        
    }
    ?> 

</div></div>

<h1 class="b_video_name"><?php echo get_cat_name($_GET['cat_id'] ); ?></h1>
  <div id="searchbox">
 <?php echo do_shortcode('[awsqf-form id=3301]'); ?> <?php //echo do_shortcode('[awsqf-form id=3208]'); ?>
 </div> 
<div class="clear"></div>
    <!--<h3 class="section-title upper">HIGHLIGHTS</h3>-->

    <div class="line-separator"></div>

    

    <div id="i551" class="actions">           		

        <div class="row">
                   
                        <div class="videos telvsn_vdeo">
                            <div > 
                          
                            <?php							
						//query_posts('cat=76&showposts=-1&post_type=youtubevideo'); //cat 10 
					
							$args = array('cat'=>$cat_id,'post_type'=>'youtubevideo','posts_per_page'=>12);
						custom_query_posts($args);
						
						if (have_posts()) :
						while (have_posts()) : the_post();	
						$report_count++ ;
						

                        $x=get_post_meta($post->ID, 'video_iframe_url', true);//http://img.youtube.com/vi/o4Yy39o4J7Q/1.jpg
                        if($x!="")
                        {
							$c=strpos($x, "embed/");
							$lst= strpos($x, "/",$c+6);
							$code=substr($x,$c+6,$lst-$c-6);
							$explode=explode('"',$code);
							$youtube_img= substr($explode[0],0,11);
                        }
		
				
            ?>
            
                            	<div class="various b_various" href="<?php echo bloginfo('home');?>/view-youtube-video/?myvar=<?php echo $post->ID; ?>">
                                    <span class="video-img" style="top:38px;"></span>
                                <div class="vcol2">
                                    <div class="video-wrap">
                                         <div class="player"></div> 
                                         <div class="b-opacity"></div>
                                        <div class="video-thumb"> 

                                            <span>
                                        <div style="position:relative; display:block;" id="w_755" class="bwWrapper"><div class="f-div-sml"> 
                                            <div class="b_over"></div>
                                            <?php    
                    if($x!="")
                    {
                    ?>
                   <?php /*?> <img src="http://img.youtube.com/vi/<?php echo $youtube_img;?>/0.jpg"/ width="218" height="95"><?php */?>
                   
                   <?php
                   // Check for Plugin
              if (class_exists('MultiPostThumbnails')) {

                // Set Thumbnail
                $thumb = MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'youtube-thumbanail-image',NULL,  'ytube-thumbanail-image');
                $has_thumb = MultiPostThumbnails::has_post_thumbnail(get_post_type(), 'youtube-thumbanail-image', strval(get_the_ID()));

                // Thumbnail exist? Else show Not Found
				?>
				
                <?php
                if ($has_thumb) : echo $thumb; else : ?>  <img src="http://img.youtube.com/vi/<?php echo $youtube_img;?>/0.jpg"/ width="218" height="164"><?php endif;

                // Plugin not found.
              } else {

                echo "Thumbnails Not Found.";

              }
			  ?>
              </div>
              
                   
                    <?php
                    }
                    ?> </div>
                                    
                                            </span> 
                                        
                                        </div>
                                        <div class="t_12 video-thumb-text">
                                      
                                            <div class="t_blue video_titl"> <?php $title_vid= get_the_title(); echo limit_words($title_vid,4);?>
                                            </div>
                                            <?php echo limit_words(get_post_meta($post->ID, 'short_description', true),6); ?>
                                             </div>

                                              <p class="wr-cate-video" style="padding:0px 3px">
                                                    <?php //List category
                                                    // $pId =  get_the_id();  

                                                    // $cat = get_the_category($pId);

                                                    //         foreach($cat as $key => $val){
                                                    //            // echo "<a href=''>".$val->term_id .', ';
                                                    //             $url = get_permalink(7).'?cat_id='.$val->term_id;
                                                    //             printf('<a href="%s" class="b_link_cate_video">%s</a>, ',$url,$val->name);
                                                    //         }

                                                    if(get_field('upload_logo')):
                                                    ?>
                                                     <img class="logo-img" src="<?php echo the_field('upload_logo') ?>" width='250' height='85'>

                                                <?php endif; ?>

                                              </p>


                                       
                                    </div>
                                </div>
                                </div>
                                
                   <?php endwhile; ?> <div style="text-align:center;">  <?php  wp_pagenavi(); ?>    </div><?php
				   else:
				   
				   echo "NO VIDEOS AVAILABLE";
				   
				   endif; wp_reset_query();?>
                                
                          
                                
                                </div>
                        </div>
                  
                    <div class="cell2"></div>
                    <div class="cell3">
                        <div id="videos-forum" class="videos"></div>
                    </div>
                </div>                

    </div>    

    

</div>



<div class="clear"></div>



<div class="line-separator"></div> 

</div>

  
<div class="clear"></div>
<?php get_footer(); ?>

    </div>

    <?php /*?><div id="content-right">

    	<div id="last-participant">

        	<div class="line-separator" style="margin-bottom:4px;"></div>

            <span class="t_16 t_blue f_Brandon_bld" style="padding-left:13px;">PAST PARTICIPANTS</span>	

            <div class="line-separator" style="margin-top:4px;"></div>	

            <div id="content_1" class="content" style="margin-bottom:20px;">

                <ul>

                <?php

				wp_reset_postdata();

                $args = array(

					"orderby" => "post_title",

					"order" => "DESC",

					"posts_per_page" => "-1",

					"post_type" => "video",

					"cat" => -16

				);

				?>	

                <?php  $cat_query = new WP_Query($args); ?>

                

                <?php $all_videos = array(); if( $cat_query->have_posts() ): ?>		

					 <?php while ($cat_query->have_posts()) : $cat_query->the_post();  ?>

                     <?php

                     $all_videos[$post->ID] = $post->post_title;

					 ?>

                     <?php endwhile; ?>

				<?php endif; ?>      	

                <?php if( count($all_videos)>0 ):  asort($all_videos); ?>

                    <?php foreach($all_videos as $key => $value):  ?>

                         <li><a class="show-video" data="<?php echo $key; ?>"><?php echo $value; ?></a></li>                    

                    <?php endforeach; ?>

                <?php endif; ?>      				

                </ul>

             </div> 

        </div>

    </div><?php */?>

    <div class="clear"></div>

</div>
</div>
    <div class="clear"></div>

<?php //get_footer(); ?>
<style>
.wp-pagenavi{margin:auto !important;margin-left:0 !important;}
</style>