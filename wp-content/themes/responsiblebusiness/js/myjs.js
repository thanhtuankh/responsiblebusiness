(function(){
	function convertDate(_string){
		var dt  = _string.split('-');
		return new Date(dt[0], dt[1]-1, dt[2], dt[3], dt[4], dt[5]);
	}

	var clock;
	function hoverGallery(){
		$('.menuGallery li a').mouseenter(function(event) {
			/* Act on the event */
			$(this).parent().find('i').css({"display":"block"});
		});
		$('.menuGallery li').mouseout(function(event) {
			/* Act on the event */
			$(this).parent().find('i').css({"display":"none"});
		});
	}
	$(document).ready(function() {
			var valueY = $(".year").val();
			var valueM = $(".month").val();
			var valueD = $(".day").val();
			var valueH = $(".hour").val();
			var valueMi = $(".minute").val();
			var valueS = $(".second").val();
			// Grab the current date
			var date = convertDate(valueY + "-" + valueM +"-"+ valueD + "-"+ valueH + "-"+ valueMi + "-"+ valueS  );
			var now   = new Date();


			var diff = (date-now)/1000;
			if (diff < 0 ){
				diff = 0;
			}
			// Instantiate a coutdown FlipClock
			clock = $('.time').FlipClock(diff, {
				clockFace: 'DailyCounter',
				countdown: true,
				showSeconds: false
			});
			console.log(diff);
		hoverGallery();
	});
})();