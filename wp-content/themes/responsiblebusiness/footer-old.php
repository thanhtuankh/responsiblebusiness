        <div id="footer">
        	<div class="line-separator" style="margin-top:20px;"></div>
            <div class="col1">
            	<span class="t_16 t_blue f_Brandon_bld">LINKS</span>
            </div>
            <div class="col2">
			<?php generate_social_media_menu(); ?>
            </div>
            <div class="col3">
            <?php generate_useful_links_menu(); ?>
            </div>
            <div class="col4">
            <?php //generate_social_media_menu(); ?>
            </div>
            <div class="clear"></div>
            <div class="line-separator"></div>
        </div>
        </div>       
            
	<?php wp_footer(); ?>    	
    </body>
</html>