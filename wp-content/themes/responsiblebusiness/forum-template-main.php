<?php
  
  
  
  /*
    
    
    
    Template Name: Forum Template Main 
    
    
    
  */
  
  
  
?>

<?php get_header(); ?>
<script>
  $(function () {
    
    var counter = 0,
        divs = $('#one,#two');
    
    function showDiv () {
      divs.fadeOut('slow') // hide all divs
            .filter(function (index) { return index == counter % 2; }) // figure out correct div to show
            .fadeIn('slow'); // and show it
      
      counter++;
    }; // function to loop through divs and show correct div
    
    showDiv(); // show first div    
    
    setInterval(function () {
      showDiv(); // show next div
    }, 5 * 1000); // do this every 10 seconds    
    
  });
</script>

<div class="top_smnu"></div>

<div class="clear"></div>

<script>
  
  
  
  jQuery(function ($) {
    
    
    
    //loadVideos('videos-tv', 20, 1, 2);
    
    
    
    //loadVideos('videos-forum', 16, 1, 2);
    
    
    
  }); 
  
  
  
</script>

<?php
  
  
  
  $vid = "";
  
  
  
  if(isset($_GET['vid'])){
    
    
    
    $vid = $_GET['vid'];
    
    
    
  ?>
  
  <script>
    
    
    
    video_popup("<?php echo $vid;?>");
    
    
    
  </script>
  
  <?php
    
    
    
  }
  
  
  
  $featuredposts = get_home_slide_posts();
  
  
  
  $images = array();
  
  
  
  if(count($featuredposts)>0){
    
    
    
    foreach($featuredposts as $key => $p){
      
      
      
      $images[] = get_featured_post_thumbnails($p->ID); 
      
      
      
      $post_data[]  = array('ID' => $p->ID, 'title' => $p->post_title, 'post_excerpt' => $p->post_excerpt, 'post_content' => $p->post_content, 'post_type' => $p->post_type);     
      
      
      
    }
    
    
    
  }
  
  
?>

<?php if(count($post_data)==4): ?>

<div class="con_lef">
  
  <?php include("side_new_menu.php");?>
  
</div>

<div class="con_rig">
  
 <?php echo b_get_slide_by_cat(147); //Events slide ?>

    <div id="content" class="home" style="color:#7d7d7d">

We enable our clients to participate in global forums across the world, where they engage with various stakeholders to share knowledge and discuss the most effective practices. Our events and community strive to create and maintain strategic partnerships, which will inspire change and help build our vision of a sustainable future.   

    </div>
  
  <div id="header-wrap">
    
    <?php foreach($post_data as $key => $p): ?>
    
    <?php
      
      
      
      //  var_dump($p);
      
      
      
    ?>
    
    <div id="c<?php echo ($key+1);?>" style="display:none;">
      
      <div class="s<?php echo ($key+1);?>">
        
        <?php
          
          
          
          if($images[$key]['small-thumbnail'] == ""){
            
            
            
            $images[$key]['small-thumbnail'] = $images[$key]['medium-thumb'];
            
            
            
          }
          
          
          
        ?>
        
        <div class="slide-small" style="background:url(<?php echo $images[$key]['small-thumbnail']; ?>) no-repeat #01AEF0;">
          
          <div class="slide-small-wrap"> <a href="<?php echo get_permalink($p['ID']); ?>" style="color:#FFFFFF;">
            
          <div class="t_10 t_white"><strong><?php echo $p['title']; ?></a></strong></div>
          
          <?php if($p['post_type'] != "video"): ?>
          
          <div class="read_more"><a href="<?php echo get_permalink($p['ID']); ?>" class="t_10"><strong>Read More</strong></a></div>
          
          <?php endif; ?>
          
          </div>
          
        </div>
        
      </div>
      
    </div>
    
    <?php endforeach;  ?>
    
    
    
  </div>
  
  <?php endif; ?>
  
  <div id="content" class="home" style="color:#7d7d7d">
    
    <?php
      
      
      
      // $content = apply_filters('the_content', $post->post_content); 
      
      
      
      // echo $content;  
      
      
      
    ?>
    
  </div>
  
  <?php //var_dump($events);?>
  
  <?php $events = get_events(); ?>
  
  
  
  <?php $past_events = get_past_events(); ?>  
  
  
  
  <?php //include(get_theme_root('template_directory').'/'.get_template().'/slider-forum.php'); ?>   
  
  
  
    
  
  
  
    <?php //var_dump($events);?>
  
  
  
    <?php $num = count($events); ?>
  
  
  
    <?php $past_num = count($past_events); ?> 
  
  
  
    <?php if($num==0 && ($past_num==0 || $past_num>0)): ?>
  
  
  
    <style>   
    
    
    
    .text-paragraph {   
    
    
    
    margin-top: 20px!important;
    
    
    
    }
    
    
    
  </style>
  
  
  
    <?php endif; ?>
  
  
  
    <?php if($num>0 || $past_num>0): ?>
  
  
  
  
  
  
  
  
  
    
  
  <div class="hlt formIndex">
    
    <div id="event-list">
      
      
      
      <div id="i551" class="actions">
        
        
        
        <!--<div class="row">
          
          
          
          <div class="cell1">
          
          
          
          <div class="line-separator"></div>
          
          
          
          <h3 class="section-title">PAST EVENTS</h3>
          
          
          
          <div class="line-separator"></div>
          
          
          
          </div>
          
          
          
        </div>-->
        
        
        
        <div class="">
          
          
          
          <!--<div class="cell1">
            
            
            
            <div class="line-separator"></div>
            
            
            
            <h3 class="section-title">UPCOMING EVENTS</h3>
            
            
            
            <div class="line-separator"></div>
            
            
            
          </div>  -->
          
          
          
          <!--<div class="cell2"></div>       -->   
          
          
          
          
          
          <div class="row">
            
            
            
            <div class="cell1">
              
              
              
              <div class="line-separator"></div>
              
              
              
              <h3 class="section-title">COMING EVENTS</h3>
              
              
              
              <div class="line-separator"></div>
              
              
              
            </div>  
            
            
            
            <div class="cell2"></div>              
            
            
            
            
            
          </div>
          
          
          
          
          
          
          
          <div class="">
            
            
            
            <div class="">
              
              
              
              <?php $num = count($events); ?>  
              
              
              
              <div id="event-list-left" class="event-list">
                
                
                
                <ul>
                  
                  
                  
                  <?php $i=0;  foreach($events as $event):  $i++; ?>
                  
                  
                  
                  <?php   //print_r($event);?>
                  
                  
                  
                  
                  
                  <li> 
                    
                    <div class="evnt_img">
                      
                      <?php //echo $event['post_id'];//the_post_thumbnail();?>
                      
                      <?php
                        
                        $feat_image = wp_get_attachment_url( get_post_thumbnail_id($event['post_id']) );
                        
                        
                        
                        //echo "<img src=$feat_image;
                        
                        
                        
                      ?>
                      
                      <img src="<?php echo $feat_image;?>" />
                     
                      <div class="lopMask">
                        <div class="imgText">
                          <img class="icon-view" src="<?php echo bloginfo('template_directory'); ?>/images/icon-view.png">
                          <p>
                           <a href="<?php echo get_permalink($event['post_id']); ?>"> VIEW event details</a>
                          </p>
                        </div>
                      </div>
                       <a href="<?php echo get_permalink($event['post_id']); ?>" class="wr-a-forum"></a>
                     
                      
                    </div>
                     </a>
                    
                    
                    <?php /*?><div class="event-wrap-left t10 upper"> <strong><?php //echo $event['parent_category']; ?><br>
                      
                      
                      
                    <span class="t_gray"><?php  // echo $event['category'];?></span></strong> </div><?php */?>
                    
                    
                    
                    <div class="event-wrap-right"> <a href="<?php echo get_permalink($event['post_id']); ?>" class="c_event"><?php echo $event['event_name']; ?></a>
                      
                      
                      
                      <div class="t9 c_event_loc"><?php echo generate_event_dates($event['start'], $event['end']); ?><br>
                        <?php 
                          
                          if(get_post_meta($event['post_id'], 'event_location', true)!="")
                          {
                            
                            echo get_post_meta($event['post_id'], 'event_location', true);
                          }
                          else
                          {
                            echo $event['location_name'];
                          }
                        ?>
                        
                        
                      <?php //echo $event['location_name'] ?>  <?php //echo $event['country'] ?>   </div>
                      
                      
                      
                    </div>
                    
                    
                    
                  </li>
                  
                  
                  
                  <?php endforeach; ?>
                  
                 
                </ul>
                
                
                
                
                
                
                
                
                
              </div>
              
              
              
            </div>
            
            
            
          </div>
          
          
          
          
          
          <div class="clear"></div> 
          
          
          
          <div>
            
            
            
            
            
            
            
            <h3 class="section-title">PAST EVENTS</h3><!--event-list-right-->
            
            
            
            
            
            
            
          </div>         
          
          
          
          
          
          
          
          <div class="">
            
            
            
            <div class="">
              
              
              
              <?php $num = count($past_events); ?>  
              
              
              
              <div id="event-list-left">
                
                
                
                <ul>
                  
                  
                  
                  <?php $i=0;  foreach($past_events as $event):  $i++; ?>
                  
                  
                  
                  <?php   //print_r($event);?>
                  
                  
                  
                  
                  
                  <li> 
                    
                    <div class="evnt_img sss_sss">
                      
                      <?php
                        
                        $feat_image = wp_get_attachment_url( get_post_thumbnail_id($event['post_id']) );
                        
                        
                        
                        //echo "<img src=$feat_image;
                        
                        
                        
                      ?>
                      
                      <img src="<?php echo $feat_image;?>" />
                        <div class="lopMask">
                        <div class="imgText">
                          <img class="icon-view" src="<?php echo bloginfo('template_directory'); ?>/images/icon-view.png">
                          <p>
                           <a href="<?php echo get_permalink($event['post_id']); ?>"> VIEW event details</a>
                          </p>
                        </div>
                      </div>
                      <a href="<?php echo get_permalink($event['post_id']); ?>" class="wr-a-forum"></a>
                      
                    </div>
                    
                    
                    
                    <?php /*?><div class="event-wrap-left t10 upper"> <strong><?php //echo $event['parent_category']; ?><br>
                      
                      
                      
                    <span class="t_gray"><?php  // echo $event['category'];?></span></strong> </div><?php */?>
                    
                    
                    
                    <div class="event-wrap-right">
                      
                      <div class="past_ev_year"><?php echo date('Y', $event['start']);//date('Y', strtotime($post->post_date));?></div>
                      
                      <a href="<?php echo get_permalink($event['post_id']); ?>" class="c_event"><?php echo $event['event_name']; ?></a>
                      
                      
                      
                      <div class="t9 c_event_loc"><?php echo generate_event_dates($event['start'], $event['end']); ?><br>
                        
                        
                        
                        <?php 
                          
                          if(get_post_meta($event['post_id'], 'event_location', true)!="")
                          {
                            
                            echo get_post_meta($event['post_id'], 'event_location', true);
                          }
                          else
                          {
                            echo $event['location_name'];
                          }
                        ?>  <?php //echo $event['country'] ?> 
                        
                        
                      </div>
                      
                      
                      
                    </div>
                    
                    
                    
                  </li>
                  
                  
                  
                  <?php endforeach; ?>
                  
                  
                  
                </ul>
                
                
                
                
                
                
                
                
                
              </div>
              
              
              
            </div>
            
            
            
          </div>
          
        </div> 
        
        
        
      </div>
      
      
      
    </div>
    
    
  </div>
    <div class="clear"></div>
  
  
  
  <?php endif; ?> 
  
  <!--div class="hlrs">
    
    <div class="sep_le">
      
      <div class="glsm">
        <div style="height:227px;width:270px;overflow:hidden">
          <?php $id=5044; 
            $post = get_post($id); 
            $url = wp_get_attachment_url( get_post_thumbnail_id());
          ?>
          <div id="one">
            <a href="<?php echo $post->post_excerpt;?>" target="_blank">
              <img src="http://www.responsiblebusiness.com/wp-content/uploads/2015/05/image2.jpg" />
              <?php /*?><img src="<?php  echo $url; ?>"   /><?php */?>
            </a>
          </div>
          <div id="two">
            <a href="<?php echo $post->post_excerpt;?>" target="_blank">
              <img src="http://www.responsiblebusiness.com/wp-content/uploads/2015/05/image1.jpg" />
            <?php /*?><img src="<?php  echo $url; ?>"   /><?php */?></a>
          </div>
        </div></div>
        
        <div class="tweets-feed twt">
          
          <h3>Recent Tweets</h3>
          
          <style>
            
            
            
            
            
            
            
            /*.dummy_div {
            
            
            
            background:#FFF;
            
            
            
            bottom: 11px;
            
            
            
            height: 13px;
            
            
            
            left: 5px;
            
            
            
            position: absolute;
            
            
            
            width: 216px;
            
            
            
            }*/
            
            
            
            .tweets-feed iframe{width:100% !important}
            
            #main{width:100% !important}
            
            .tweets-feed iframe #header{width:100% !important}
            
            .blockItem{width:100% !important}
            
            
            
            .dummy_div {
            
            
            
            background:#FFF;
            
            
            
            bottom: 6px;
            
            
            
            height: 30px;
            
            
            
            left: 5px;
            
            
            
            position: absolute;
            
            
            
            width: 216px;
            
            
            
            }
            
            
            
            
            
            
            
            .slides0 ul li {
            
            
            
            padding-top:0px !important;
            
            }
            
            .slides0 {
            
            height: 180px !important;
            
            }
            
            
            
            
            
            .single_logo {
            
            float: left;
            
            height: 150px;
            
            margin: 10px 0 0;
            
            text-align: center;
            
            width: 250px;
            
            }
            
          </style>
          
          
          
          <!-- BEGIN: Twitter website widget (http://twitterforweb.com) >
          
          
          
          <div style="" > 
            
            
            
            <!--  <div class="dummy_div"></div>> 
            
            
            
            <?php /*?>   <script type="text/javascript">
              
              
              
              
              
              
              
            document.write(unescape("%3Cscript src='http://twitterforweb.com/twitterbox.js?username=@Responsible_Biz&settings=1,0,3,225,350,ffffff,0,ffffff,101010,1,1,336699' type='text/javascript'%3E%3C/script%3E"));</script><?php */?>
            
            <?php /*?><a class="twitter-timeline"  href="https://twitter.com/Responsible_Biz"  data-widget-id="454328311032713216">Tweets by @Responsible_Biz</a>
              
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script><?php */?>
            <a class="twitter-timeline" href="https://twitter.com/Responsible_Biz" data-widget-id="454328311032713216">Tweets by @Responsible_Biz</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            
            
            
            
            
          </div>
          
          
          
          <!-- END: Twitter website widget (http://twitterforweb.com) >

          <?php /*?> </div>

            <div class="clear"></div>

          </div><?php */?>
          
        </div>
        
        <div class="col4 jntwr" >
          
          <h3>Join Our Network</h3>
          
          <p>Receive our newsletters, exclusive content previews, event invitations and much more</p>
          
          <?php //dynamic_sidebar( 'newsletter-sidebar' ); ?>
          
          <a href="#" data-reveal-id="join_our_network" class="sgnp" data-animation="fade">Signup</a>
          
          <?php //generate_social_media_menu(); ?>
          
        </div>
        
        <div class="clear"></div>
        
        <?php /*?>      <div class="col3_app appfrm">
          
          <h3>GET RB FORUM APP</h3>
          
        <a href="http://play.google.com/store/apps/details?id=com.rbforum" target="_blank"><img src="<?php bloginfo('template_directory')?>/images/google.png"   /></a> <a href="http:///itunes.apple.com/us/app/rb-forum/id741520067?mt=8" target="_blank"><img src="<?php bloginfo('template_directory')?>/images/app.png" /></a> </div><?php */?>
        
    </div>
    
  </div-->
  
  <div class="clear"></div>
  
  <?php get_footer(); ?>
  
</div>
