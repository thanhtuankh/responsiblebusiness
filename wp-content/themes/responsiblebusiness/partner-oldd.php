<?php
/*
Template Name: partner-template
*/
?>
<?php get_header(); ?>

<style>
.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar{
 background:#fff; /* rgba fallback */
 background:rgba(1,174,240,0.75);
 filter:"alpha(opacity=75)"; -ms-filter:"alpha(opacity=75)"; /* old ie */
}
.mCSB_scrollTools .mCSB_dragger:hover .mCSB_dragger_bar{
 background:rgba(1,174,240,0.85);
 filter:"alpha(opacity=85)"; -ms-filter:"alpha(opacity=85)"; /* old ie */
}
.mCSB_scrollTools .mCSB_dragger:active .mCSB_dragger_bar,
.mCSB_scrollTools .mCSB_dragger.mCSB_dragger_onDrag .mCSB_dragger_bar{
 background:rgba(1,174,240,0.9);
 filter:"alpha(opacity=90)"; -ms-filter:"alpha(opacity=90)"; /* old ie */
}
</style>
<script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery.mCustomScrollbar.concat.min.js'></script>
<link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/jquery.mCustomScrollbar2.css' rel='stylesheet' media='screen' />
<script>
$(document).ready(function() {

	//rotation speed and timer
	var speed = 3000;
	var run = setInterval('rotate()', speed);	
	
	//grab the width and calculate left value
	var item_width = $('#slides li').outerWidth(); 
	var left_value = item_width * (-1); 
        
    //move the last item before first item, just in case user click prev button
	$('#slides li:first').before($('#slides li:last'));
	
	//set the default item to the correct position 
	$('#slides ul').css({'left' : left_value});

    //if user clicked on prev button
	$('#prev').click(function() {

		//get the right position            
		var left_indent = parseInt($('#slides ul').css('left')) + item_width;

		//slide the item            
		$('#slides ul:not(:animated)').animate({'left' : left_indent}, 200,function(){    

            //move the last item and put it as first item            	
			$('#slides li:first').before($('#slides li:last'));           

			//set the default item to correct position
			$('#slides ul').css({'left' : left_value});
		
		});

		//cancel the link behavior            
		return false;
            
	});

 
    //if user clicked on next button
	$('#next').click(function() {
		
		//get the right position
		var left_indent = parseInt($('#slides ul').css('left')) - item_width;
		
		//slide the item
		$('#slides ul:not(:animated)').animate({'left' : left_indent}, 200, function () {
            
            //move the first item and put it as last item
			$('#slides li:last').after($('#slides li:first'));                 	
			
			//set the default item to correct position
			$('#slides ul').css({'left' : left_value});
		
		});
		         
		//cancel the link behavior
		return false;
		
	});        
	
	//if mouse hover, pause the auto rotation, otherwise rotate it
	$('#slides').hover(
		
		function() {
			clearInterval(run);
		}, 
		function() {
			run = setInterval('rotate()', speed);	
		}
	); 
        
});

//a simple function to click next link
//a timer will call this function, and the rotation will begin :)  
function rotate() {
	$('#next').click();
}
        
        
        
</script>

<div id="header-wrap">
    <div id="header-wrap-img">     
       
         <a href="http://www.responsiblebusiness.com/events/changing-the-rules-of-the-game/">
            <img width="647px" height="280px" class="slidegrayscale" style="width:647px; height:280px; float:left;" src="<?php echo bloginfo('template_directory');?>/forum-images/img_masthead_rbf2012_about1.jpg"></a>
    
         <div id="event-text">
          <div id="event-text-wrap">
             <div class="event-cat">
                 <strong>RESPONSIBLE BUSINESS FORUM<br>
                 <span class="t_white">ON SUSTAINABLE DEVELOPMENT</span> </strong>
             </div>
                <div class="event-details">
                 <div class="t14"><strong>
                     Transformation, Growth and the Green Economy 
                     </strong></div>
                    <div class="t10">
                        <strong>                            
                            <span class="t_white">
                             Singapore<br> 
        18 - 19 November 2013                            </span> 
                        </strong>
                    </div>     
                </div> 
            </div>
         </div> 
    </div>        
</div>


<div id="content" class="t14 ccontent" style="margin-top:30px;">
    <div class="left" style="width:700px;">
      <h1>Partners</h1>
      <h2 style="font-size: 12pt; color:#00ccff;">Organisers </h2>
      <div class="speakers">
      <div class="boxs  height_bx_290"> 
          <img src="<?php echo bloginfo('template_directory');?>/forum-images/partner-1.jpg" width="100" height="95" alt="Global Initiatives">
          <div class="desc desc_partnr">
            <p>
            
           Global Initiatives is a Singapore-based media company promoting solutions to global challenges through film, television and high-level international events. The Responsible Business TV series has been produced by Global Initiatives since 2007, broadcast globally on CNBC and other networks. The B4E, Business for the Environment, global event series brings together leaders from business, governments and NGOs to agree on industry commitments and policy proposals towards a more sustainable future.  
             </p>
          <span class="read-case-study"><a href="http://www.globalinitiatives.com/" target="_blank">www.globalinitiatives.com</a></span>
          </div>
        </div>
        <!--boxs ends here-->
<div class="boxs height_bx_290"> 
          <img src="<?php echo bloginfo('template_directory');?>/forum-images/partner-3.jpg" width="100" height="95" alt=" TEEB">
          <div class="desc desc_partnr">
            <p>The goal of TEEB for Business Coalition is to achieve a shift in corporate behaviour to preserve and enhance rather than deplete our natural capital. To achieve this, our activities focus on global stakeholder engagement, focused research and development of methods for natural capital accounting. This is the business application of G8 and UNEP supported TEEB programme, led by Pavan Sukhdev which provides a compelling economic case for the conservation of natural capital and is the cornerstone of current Green Economy policy. </p>
         <span class="read-case-study"><a href="http://www.teebforbusiness.org/" target="_blank">www.teebforbusiness.org</a></span>
          </div>
        </div>
        <!--boxs ends here-->
        <div class="boxs height_bx_290"> 
          <img src="<?php echo bloginfo('template_directory');?>/forum-images/partner-2.jpg" width="100" height="95" alt="WWF">
          <div class="desc desc_partnr">
            <p>WWF has the global insights to help businesses navigate through key sustainability risks and opportunities. As the world's leading independent conservation organization, we work with businesses, communities and governments to create innovative solutions to safeguard the natural world, tackle climate change and enable the sustainable use of our natural resources. Through our endeavors we seek to embed sustainability and social well-being in both economic and environmental systems. </p>
         <span class="read-case-study"><a href="http://www.wwf.org/" target="_blank">www.wwf.org </a></span>
          </div>
        </div>
        <!--boxs ends here-->
         
         <div class="boxs height_bx_290"> 
          <img src="<?php echo bloginfo('template_directory');?>/forum-images/partner-4.jpg" width="100" height="95" alt="Eco-Business">
          <div class="desc desc_partnr">
            <p>Eco-Business.com is the leading online publication for Asia Pacific's cleantech, CSR & sustainable business community, providing the largest selection of sustainable business news, features and expert opinion for the region - covering Clean Energy, Smart Grids, Green Buildings, Transport & Logistics, Urban Development, Eco-Cities, Manufacturing & Supply Chain, Waste, Water, Food & Agriculture, CSR, Carbon & Climate, and SRI/Impact Investing. The site includes Asia Pacific's most trusted and comprehensive directory of clean, green & responsible organisations and provides them with a sophisticated client account that allows them to publish jobs, events, press releases and research. Eco-Business offers powerful marketing channels including online, CxO round tables, writing services, the annual Responsible Business Forum, and global television with Responsible Business Television. </p>
         <span class="read-case-study"><a href="http://www.eco-business.com/" target="_blank">www.eco-business.com </a></span>
          </div>
        </div>
        <!--boxs ends here-->
        <div class="boxs height_bx_290"> 
         <div >  <img src="<?php echo bloginfo('template_directory');?>/forum-images/partner-5.jpg" width="100" height="95" alt="WBCSD">
          <img src="<?php echo bloginfo('template_directory');?>/forum-images/bcsd-singapore.jpg" width="100" height="95" alt="WBCSD"></div>
          <div class="desc desc_partnr">
            <p>The World Business Council for Sustainable Development (WBCSD) is a CEO-led, global coalition of some 200 companies advocating for progress on sustainable development. Its mission is to be a catalyst for innovation and sustainable growth in a world where resources are increasingly limited. The Council provides a platform for companies to share experiences and best practices on sustainable development issues and advocate for their implementation, working with governments, non-governmental and intergovernmental organizations. The membership has annual revenues of USD 7 trillion, spans more than 35 countries and represents 20 major industrial sectors. The Council also benefits from a network of 60 national and regional business councils and partner organizations, a majority of which are based in developing countries. </p>
         <span class="read-case-study"><a href="http://www.wbcsd.org/home.aspx" target="_blank">www.wbcsd.org </a></span>
          </div>
        </div>
        <!--boxs ends here-->
        
        <div class="clear"></div>
      </div>
    </div>
    <div class="right">
<div id="partners">
        <div style="margin-top:-2px;" class="line-separator"></div>
         <h3 class="section-title">Organisers</h3>
        
        <div class="line-separator"></div>
        <div id="carousel">

	<div id="buttons" style="display:none;">
		<a href="#" id="prev">prev</a>
		<a href="#" id="next">next</a>
		<div class="clear"></div>
	</div>
	
	<div class="clear"></div>

	<div id="slides"> 
		<ul>
       <li><img src=" <?php echo bloginfo('template_directory');?>/forum-images/global.jpg" width="180" alt="Global-Initiatives"/></li>
			<li><img src="<?php echo bloginfo('template_directory');?>/forum-images/TEEB.jpg" width="160" height="150" alt="TEEB"/></li>
			<li><img src="<?php echo bloginfo('template_directory');?>/forum-images/WWF.jpg" width="160" height="150" alt="WWF"/></li>
			<li><img src="<?php echo bloginfo('template_directory');?>/forum-images/eco-bsns.jpg" width="160" height="150" alt="eco-business"/></li>
            <li><img src="<?php echo bloginfo('template_directory');?>/forum-images/wbcsd.jpg" width="160" height="150" alt="WBCSD"/></li>
		</ul>
		<div class="clear"></div>
	</div>

</div>
<div class="tweets-feed">
<div class="forum-tweet-title">Tweets</div>
<div class="twts_box">
  <img src="<?php echo bloginfo('template_directory');?>/forum-images/twt.jpg" width="39" height="39" alt="Responsible Business">
  <div class="twt_txt">
  <span><strong>Responsible Business </strong>

@Responsible_Biz</span>
<span style="border-bottom:1px dashed #CCC;">
<a href="https://twitter.com/@Responsible_Biz" target="_blank">
@tonygourlayGI thanks! We are excited to share more as we approach #RBForum coming up this November in Singapore!
</a></span>
<span><a href="http://www.responsiblebusiness.com/agenda-3/" target="_blank">
#RBForum Agenda is online, with working groups announced for Singapore 2013
Have a look to see where you fit in!
http://www.responsiblebusi<br />
ness.com/agenda-3/ </a></span>
 
  </div>
  <div class="clear"></div>
  </div>
</div>
    </div>
</div>
    <div class="clear"></div>
  </div>

<script src="forum-js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script>
		(function($){
			$(window).load(function(){
				$(".desc").mCustomScrollbar();
			});
		})(jQuery);
	</script>




<?php get_footer(); ?>