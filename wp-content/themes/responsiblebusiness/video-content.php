<?php

require('../../../wp-blog-header.php');

wp_head();

$video=wp_get_single_post($_POST['id'], ARRAY_A);

?>

<div id="video-wrap" style="overflow-y:auto; height:471px;">

    <div class="line-separator" style="margin-bottom:10px;"></div>    

    <div id="video-wrap-content">

   		<div id="show-video">

        	<div id="myElement">Loading the player...</div>

            <?php $videofile = get_post_meta( $_POST['id'], 'video_file', true ); ?>    

           <?php

           $img_data = get_post_thumbnails( $_POST['id']);		   

		   ?>

			<script type="text/javascript">

               jwplayer("myElement").setup({

					file: "http://www.responsiblebusiness.com/res_video/<?php echo $videofile; ?>",					

					width: "480",

					height: "270"					

                });

            </script>	

            <div id="video_url" style="display:none; "><?php echo $videofile; ?></div>		

        </div>

        <?php $img_data = get_post_thumbnails($_POST['id']); ?>     

        <div id="video-wrap-left">

            <div class="t_24 t_blue"><?php echo $video['post_title'];?></div>            

            <div class="t_14"><?php echo $video['post_content']; ?></div>

        </div>

        <div id="video-wrap-right">

        <!-- AddThis Button BEGIN --> 

  		<div class="addthis_toolbox addthis_default_style" addthis:url="<?php echo get_permalink($_POST['id']); ?>" addthis:title="<?php echo htmlentities($video['post_title'], ENT_QUOTES); ?>" addthis:description="<?php echo htmlentities($video['post_excerpt'], ENT_QUOTES); ?>" addthis:image="<?php if(isset($img_data['image'])) echo $img_data['image']; ?>"> 

		<a href="http://www.addthis.com/bookmark.php?v=250&pubid=ra-517642bf5ed47108" class="addthis_button_compact"><img src="<?php echo bloginfo('template_directory');?>/images/share.png" width="66" height="24" border="0" alt="Share" /></a> 

  	</div> 

        </div>

        <div class="clear"></div>

    </div>

   <div class="line-separator bottom"></div>

</div>