<?php
/*
Template Name: Event Template 
*/
?>
<?php get_header(); ?>
<div id="container" class="event-child">
<?php $event = get_event(get_the_ID()); ?>	
<?php include(get_theme_root('template_directory').'/'.get_template().'/eventHeader.php'); ?>        
<div class="clear"></div>     
<div id="content">
	<div id="content-left">
    	<div class="t14">
        	<span class="t_blue"><?php echo $event['parent_category']. ' ' .$event['category']; ?></span> <?php echo $event['event-paragraph']; ?>
        </div>
    	<div class="clear"></div>
    	<?php /*?><div class="left_column t_blue t11"><?php echo $event['event-paragraph-left']; ?></div><?php */?>
        <div class="right_column ccontent"><?php echo $event['post_content']; ?></div>
    </div>
    <div id="content-right">
    	<?php include(get_theme_root('template_directory').'/'.get_template().'/partners.php'); ?>    	
    </div>
</div>
<div class="clear" style="padding-bottom:10px;"></div>     
</div>
<?php get_footer(); ?>