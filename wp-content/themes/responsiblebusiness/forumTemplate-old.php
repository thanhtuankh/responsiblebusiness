<?php

/*

Template Name: Forum Template 

*/

?>

<?php get_header(); ?>

<script>

jQuery(function ($) {

	loadVideos('videos', 16, 1, 4);

});	

</script>

<div id="forum">

	<?php $events = get_events(); ?>

	<?php $past_events = get_past_events(); ?>  

	<?php include(get_theme_root('template_directory').'/'.get_template().'/slider-forum.php'); ?>   

    <div id="content" class="t14 ccontent">

        <?php

            $content = apply_filters('the_content', $post->post_content); 

            echo $content;  

        ?>

    </div>

    <?php //var_dump($events);?>

    <?php $num = count($events); ?>

    <?php $past_num = count($past_events); ?>	

    <?php if($num==0 && ($past_num==0 || $past_num>0)): ?>

    <style>		

	.text-paragraph {		

		margin-top: 20px!important;

	}

	</style>

    <?php endif; ?>

    <?php if($num>0 || $past_num>0): ?>

    <div id="event-list">   

           <div id="i551" class="actions">           		

               	<div class="row">

                    <div class="cell1">

                        <div class="line-separator"></div>

                        <h3 class="section-title">UPCOMING EVENTS</h3>

                        <div class="line-separator"></div>

                    </div>  

                    <div class="cell2"></div>              

                    <div class="cell3">

                        <div class="line-separator"></div>

                        <h3 class="event-list-right section-title">PAST EVENTS</h3>

                        <div class="line-separator"></div>          

                    </div>          

                </div>                

                <div class="row">

                    <div class="cell1">

                         <div id="event-list-left"> 

                            <?php $i=0; foreach($events as $event): $i++;?>

                            	<?php 

								$event_logo = get_event_logo($event['post_id']);								

								?>

                                <?php if(count($event_logo)>0): ?>

                                	<div class="event-wrap-left" style="background:none; padding-left:0px!important; margin-left:0px!important;">

                                    	<img src="<?php echo $event_logo[0]; ?>" style="max-width:150px; max-height:60px;" style="border:1px solid black" />

                                    <?php else: ?>

                                   	<div class="event-wrap-left t10 upper">

                                    	<strong><?php echo $event['parent_category']; ?><br />

                                		<span class="t_gray"><?php echo $event['category']; ?></span></strong>

                                    <?php endif; ?>                                	

                                </div>

                                <div class="event-wrap-right">

                                    <strong class="upper t10"><a href="<?php echo get_permalink($event['post_id']); ?>" class="t_blue"><?php echo $event['event_name']; ?></a></strong>

                                    <div class="t9"><em><strong>										

                                        <?php echo generate_event_dates($event['start'], $event['end']); ?><br  />

                                        <?php echo $event['country'] ?>

                                     </strong></em></div>

                                </div>

                                <div class="clear"></div>

                                <?php if($i==$num): ?>

                                    <div style="margin-top:40px;"></div>

                                <?php else: ?>

                                    <div class="line-separator" style="margin-top:15px;"></div>

                                <?php endif;?>                                

                            <?php endforeach; ?>

                            &nbsp;

        				</div>

                    </div>  

                    <div class="cell2"></div>              

                    <div class="cell3">

                         <?php $num = count($past_events); ?>                         

                        <div id="event-list-right" class="t9">

                            <?php $i=0;  foreach($past_events as $event):  $i++; ?>

                            <div class="past-event-wrap">

                                    <strong class="t_blue"><?php echo $event['parent_category']. ' '.$event['category']; ?></strong>

                                    <a href="<?php echo get_permalink($event['post_id']); ?>"><strong><?php echo $event['event_name']; ?></strong></a><br />

                                    <em>

                                    	<?php echo generate_event_dates($event['start'], $event['end']); ?><br  />

										<?php echo $event['country'] ?>	

                                    </em>

                             </div>  

                           	<?php if($i==$num): ?>

                            	<div style="margin-top:40px;"></div>

                            <?php endif;?>              

                            <?php endforeach; ?>

                        </div>

                    </div>          

                </div>

			</div>

    </div>    

    <?php endif; ?> 

    <div id="videos" class="videos"></div>

    <div class="clear"></div>

</div>

<?php get_footer(); ?>