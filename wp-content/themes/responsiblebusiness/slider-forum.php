<?php
if(count($events)>0){
?>
<div id="header-wrap">
 
<div id="slider2" data-slider-count="<?php echo count($events); ?>">
    <ul>	  
        <?php foreach($events as $key => $event): ?>
        
        <li style="background:#01AEF0;">				   
        	<?php if( $event['image']): ?>  
            <a href="<?php echo get_permalink($event['post_id']); ?>">	    
        		<img src="<?php echo $event['image']; ?>" style="width:647px; height:280px; float:left;" />
            </a>	
            <?php endif; ?>  
            <div class="img-text">
               <div class="img-text-wrap">
               		<a href="<?php echo get_permalink($event['post_id']); ?>">	  
               		<div class="t_26"><?php  echo $event['parent_category']. ' '.$event['category']; ?></div>
               		<div class="t10">
                    <span class="t_white">
                    	<strong><?php echo $event['country'] ?><br />
						<?php echo generate_event_dates($event['start'], $event['end']); ?>
                        </strong>
                    </span>										
                    </div>
                    <?php if(trim($event['post_excerpt'])!=""): ?>
                		<div class="t11">							
                            <?php echo truncate_text($event['post_excerpt'], 120); ?>  
                        </div>
                    <?php endif; ?>    
                	<div class="read_more"><em><a href="<?php echo get_permalink($event['post_id']); ?>">READ MORE</a></em></div>
                    </a>	
               </div>
           </div>
        </li>
        	
        <?php endforeach; ?>
    </ul>
</div>
</div>
<?php	
}
?>