<?php 
/*Template Name: Commitments page*/
?>


<?php get_header(); ?>
<div class="top_smnu"></div>
<div class="clear"></div>
<div id="container" class="blog">
<div class="con_lef">
<?php include("side_new_menu.php");?>
</div>
<div class="con_rig">
   <?php echo b_get_slide_by_cat(153); //community cat slide ?>

     <div id="content" class="home" style="color:#7d7d7d">
The year 2015 is a historic one for climate change and global sustainable development. We are calling on all companies making commitments on climate change or on the sustainable development goals to share these with us. 

    </div>

<div id="content" class="contentCommnunity">

    <div class="line-separator nws_bdr"></div>

    <h3 class="section-title">
      
      Commitments
    
    </h3> <input type="hidden" name="asd" value="ss"> 

    <div class="line-separator" style="margin-bottom:3px;"></div>

  
                <div class="menuGallery menuCommit">
                <ul class="tabCommitments">
                 <li class="liTabCommit activeCommit" tit="infoCommitments"> Your Commitments</li>
                 <li class="liTabCommit" tit="infoGoals">Share your goals</li>
                  <div class="clear"></div>
                </ul>
              </div>





             
            
            
            <div class="commitArea" id="infoCommitments">
              <?php
                
                  $args = array(
                    'cat'              => 152,
                    'post_type'   => 'any',
                    'order'               => 'ASC',
                    'orderby'             => 'menu_order',
                    'posts_per_page'         => 99, 
                  );
                
                $query = new WP_Query( $args );
                while($query->have_posts()):$query->the_post();
                 $id=get_the_id(); $thum_url=wp_get_attachment_url( get_post_thumbnail_id($id) ); 

               ?>
               <div class="imgTextCommu">
                    <div class="imgCommu wr_img_commit">
                      <img class="img-responsive" src="<?php echo $thum_url;?>">
                    </div>
                      <div class="descCommu"> 
                        <strong class="title">
                          <?php the_title(); ?>
                        </strong>
                        <div class="line"></div>
                        <div class="desc b_desc_community">
                          <p class="b_large_text"><?php the_field('large_text') ?></p> 
                                <p class="b_small_text"><?php the_field('small_text') ?></p>
                                <a class="com_learn_more" href="<?php the_field('link_learn_more') ?>">Learn More</a>
                        </div>
                              <!-- Go to www.addthis.com/dashboard to customize your tools -->
<div class="addthis_toolbox addthis_default_style" addthis:url="<?php the_permalink();?>" style="margin-top: 25px;" addthis:title="Delivering the #SDGs with @Responsible_Biz" addthis:image="http://www.responsiblebusiness.com/wp-content/uploads/2015/10/Novatis2.jpg">
      <a class="addthis_button_facebook addthis_button_preferred_1" title="Facebook"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/facebook.png" border="0" alt="Facebook"/></a>
      <a class="addthis_button_twitter addthis_button_preferred_3"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/twitter.png" border="0" alt="Twitter"/></a>
      <div class="wr-linke-share"> <a class="addthis_button_linkedin_counter at300b b_linke_share_btn"></a><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/linkedin.png" border="0" alt="Linkeln"/></div>
      <a class="aaddthis_button_email"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/email.png" border="0" alt="Email"/></a>
      <a class="addthis_button_compact at300m" href="#"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/addthis.png" border="0" alt="Twitter"/></a>                                    
    </div>
                      </div>
                    <div class="clear"></div>
                  </div>

              <?php endwhile; wp_reset_postdata();?>
            </div>



            <div class="commitArea" id="infoGoals">
              <?php 
$code = get_field('your_code_tab',8190);
               echo do_shortcode(" $code");
               ?>

            </div>
            

</div>


<div class="line-separator" style="margin:20px 0 23px;"></div>




 <?php $sc = get_field('your_code_tab') ; ?>
<input type="hidden" name='sc' value="<?php echo $sc; ?>" class="commitHd">

<script type="text/javascript">
// $(document).ready(function(){
//  var markup = $('.commitHd').val();
//   $('#infoGoals').html(markup);
// });
</script>


<?php get_footer(); ?>




