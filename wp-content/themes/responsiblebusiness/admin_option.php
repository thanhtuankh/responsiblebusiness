<?php 
add_action('admin_menu', 'waz_create_menu');
function waz_create_menu() {

	//create new top-level menu
	add_menu_page('Theme Settings', 'ThemeOptions', 'administrator', __FILE__, 'waz_settings_page',get_stylesheet_directory_uri('stylesheet_directory')."/images/icon.png",1);

	//call register settings function
	add_action( 'admin_init', 'register_mysettings' );
}
function register_mysettings() {
	//register our settings
	register_setting( 'waz-settings-group', 'fb_url' );
	register_setting( 'waz-settings-group', 'twt_url' );
	register_setting( 'waz-settings-group', 'ytb_url' );
	register_setting( 'waz-settings-group', 'gpl_url' );
	register_setting( 'waz-settings-group', 'flk_url' );
	register_setting( 'waz-settings-group', 'lnk_url' );
	register_setting( 'waz-settings-group', 'vmo_url' );
	register_setting( 'waz-settings-group', 'lvj_url' );
	
}
function waz_settings_page() {
?>
<div class="wrap">
<h2><b style="color:#F00;">Social Media Links</b></h2>
<form method="post" action="options.php">
    <?php settings_fields( 'waz-settings-group' ); ?>
   <?php /*?> <?php do_settings( 'waz-settings-group' ); ?><?php */?>
   <table>
    <tr valign="top">
    
        <th scope="row" style="color:#f7941e;">FaceBook ::</th>
        <td><input type="text" name="fb_url" value="<?php echo get_option('fb_url'); ?>"  style="width:350px;" /></td>
    </tr>
    
     <tr>
       <th scope="row" style="color:#f7941e;">Twitter ::</th>
        <td><input type="text" name="twt_url" value="<?php echo get_option('twt_url'); ?>"  style="width:350px;" /></td>
   
     
     </tr>
          <tr>
       <th scope="row" style="color:#f7941e;">Youtube ::</th>
        <td><input type="text" name="ytb_url" value="<?php echo get_option('ytb_url'); ?>"  style="width:350px;" /></td>
   
     
     </tr>
          <tr>
       <th scope="row" style="color:#f7941e;">Google + ::</th>
        <td><input type="text" name="gpl_url" value="<?php echo get_option('gpl_url'); ?>"  style="width:350px;" /></td>
   
     
     </tr>
               <tr>
       <th scope="row" style="color:#f7941e;">Linked In ::</th>
        <td><input type="text" name="lnk_url" value="<?php echo get_option('lnk_url'); ?>"  style="width:350px;" /></td>
   
     
     </tr>
          <tr>
       <th scope="row" style="color:#f7941e;">Flicker ::</th>
        <td><input type="text" name="flk_url" value="<?php echo get_option('flk_url'); ?>"  style="width:350px;" /></td>
   
     
     </tr>
         <tr>
       <th scope="row" style="color:#f7941e;">Vimeo ::</th>
        <td><input type="text" name="vmo_url" value="<?php echo get_option('vmo_url'); ?>"  style="width:350px;" /></td>
   
     
     </tr>
     </tr>
         <tr>
       <th scope="row" style="color:#f7941e;">Live Journal ::</th>
        <td><input type="text" name="lvj_url" value="<?php echo get_option('lvj_url'); ?>"  style="width:350px;" /></td>
   
     
     </tr>
         </table>
    
    <p class="submit">
    <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>

</form>
</div>
<?php } ?>