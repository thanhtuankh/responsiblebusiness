<?php if(count($event)>0): ?>
<div id="header-wrap">
    <div id="header-wrap-img">     
    	<?php if( $event['image']): ?>  
        	<a href="<?php echo get_permalink($lastpost->ID); ?>"><img src="<?php echo $event['image']; ?>" style="width:647px; height:280px; float:left;" width="647px" height="280px" class="slidegrayscale" /></a>
		<?php endif; ?>  
         <div id="event-text">
         	<div id="event-text-wrap">
            	<div class="event-cat">
                	<strong><?php echo $event['parent_category']; ?><br />
                	<span class="t_white"><?php echo $event['category']; ?></span> </strong>
            	</div>
                <div class="event-details">
                	<div class="t14"><strong>
                    	<?php echo truncate_text($event['event_name'], 90, true); ?> 
                     </strong></div>
                    <div class="t10">
                        <strong>                            
                            <span class="t_white">
                            	<?php echo $event['country'] ?><br />	
								<?php echo generate_event_dates($event['start'], $event['end']); ?>
                            </span>	
                        </strong>
                    </div>     
                </div> 
            </div>
         </div> 
    </div>        
</div>
<?php endif; ?>