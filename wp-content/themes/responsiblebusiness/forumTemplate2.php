<?php

/*

Template Name: Forum Template1 

*/

?>

<?php get_header(); ?>


<div class="top_smnu"></div>
<div class="clear"></div>

<!-- for videos -->

<script>

jQuery(function ($) {

	loadVideos('videos-tv', 20, 1, 2);

	loadVideos('videos-forum', 16, 1, 2);

});	

</script>

<?php

$vid = "";

if(isset($_GET['vid'])){

	$vid = $_GET['vid'];

	?>

    <script>

	video_popup("<?php echo $vid;?>");

	</script>

    <?php

}

?>


<div class="con_lef">
<?php include("side_new_menu.php");?>
</div>
<div class="con_rig">
<div id="fwslider">
        <div class="slider_container">
            <div class="slide"> 
                    <img src="<?php echo bloginfo('template_directory');?>/images/banner1.jpg">
                <div class="slide_content">
                    <div class="slide_content_wrap">
                        <h4 class="title">
                        <img src="<?php echo bloginfo('template_directory');?>/images/banner-sub-logo.png" width="266" height="228"> </h4>
                        <p class="description">
                        Driving growth, Improving lives
                        <span>14 - 15 July 2014   I   Shangri-La Hotel, Manila</span>
                        </p>
                       
                  </div>
                </div>           
            </div>
           
            <div class="slide">
                <img src="<?php echo bloginfo('template_directory');?>/images/banner1.jpg">
                <div class="slide_content">
                    <div class="slide_content_wrap">
                       <h4 class="title">
                        <img src="<?php echo bloginfo('template_directory');?>/images/banner-sub-logo.png" width="266" height="228"> </h4>
                          <p class="description">
                        Driving growth, Improving lives
                        <span>14 - 15 July 2014   I   Shangri-La Hotel, Manila</span>
                        </p>
                    </div>
                </div>
            </div>

            <div class="slide">
                <img src="<?php echo bloginfo('template_directory');?>/images/banner1.jpg">
                <div class="slide_content">
                    <div class="slide_content_wrap">
                       <h4 class="title">
                        <img src="<?php echo bloginfo('template_directory');?>/images/banner-sub-logo.png" width="266" height="228"> </h4>
                         <p class="description">
                        Driving growth, Improving lives
                        <span>14 - 15 July 2014   I   Shangri-La Hotel, Manila</span>
                        </p>
                    </div>
                </div>
            </div>

        </div>

        <div class="timers"></div>

        <div class="slidePrev"><span></span></div>
        <div class="slideNext"><span></span></div>

    </div>
<div id="header-wrap">

<?php //include("form_banner.php");?>

</div>

<div id="content" class="t14 ccontent" style="margin-top:30px;">

<div class="hlt">
  
  <!--Commented--> 
  <?php /*?> <div class="left">

        <h1>  <?php echo get_post_meta($post->ID, 'sub_head', true);?></h1>

        <p><?php

	 	$content = apply_filters('the_content', $post->post_content); 

	  	echo $content; 

	?></p>

    <?php $events = get_events(); ?>

	<?php $past_events = get_past_events(); ?>  

	<?php //include(get_theme_root('template_directory').'/'.get_template().'/slider-forum.php'); ?>   

    

    <?php //var_dump($events);?>

    <?php $num = count($events); ?>

    <?php $past_num = count($past_events); ?>	

    <?php if($num==0 && ($past_num==0 || $past_num>0)): ?>

    <style>		

	.text-paragraph {		

		margin-top: 20px!important;

	}

	</style>

    <?php endif; ?>

    <?php if($num>0 || $past_num>0): ?>



      

<div id="event-list">

	<div id="i551" class="actions">

		<div class="row">         

			<div class="cell1">

				<div class="line-separator"></div>

				<h3 class="event-list-right section-title">PAST EVENTS</h3>

				<div class="line-separator"></div>          

			</div>          

		</div>

        <div class="row">

            <div class="cell1">

            <?php $num = count($past_events); ?>  

                <div id="event-list-left">

                 <?php $i=0;  foreach($past_events as $event):  $i++; ?>

                    <div class="event-wrap-left t10 upper"> <strong><?php echo $event['parent_category']; ?><br>

                        <span class="t_gray"><?php echo $event['category'];?></span></strong> </div>

                    <div class="event-wrap-right"> <strong class="upper t10"><a href="<?php echo get_permalink($event['post_id']); ?>" class="t_blue"><?php echo $event['event_name']; ?></a></strong>

                        <div class="t9"><em><strong><?php echo generate_event_dates($event['start'], $event['end']); ?><br>

                            <?php echo $event['country'] ?>	 </strong></em></div>

                    </div>

                    <div class="clear"></div>

                    <div style="margin-top:40px;"></div>

                    &nbsp; 

                    <?php endforeach; ?>

                    </div>

            </div>

        </div>

    </div>

</div>

  <?php endif; ?> 

 


<div class="clear"></div>

    </div>
    
    <?php */?>
    
<div id="videos"></div>



<div id="home-videos">

	<div class="line-separator"></div>

    <h3 class="section-title upper">Current Events</h3>

    <div class="line-separator"></div>

    

    <div id="i551" class="actions">           		

        <div class="row">

            <div class="cell1">
                <div id="videos-tv" class="videos"></div>
            </div>  

            <div class="cell2"></div>              

            <div class="cell3">
                <div id="videos-forum" class="videos"></div>
            </div>          

        </div>                

    </div>    

    

</div>



<div class="clear"></div>



<div class="line-separator"></div> 

    
</div>

<div class="hlrs">
<div class="sep_le">
    <?php include("forum_rightbox.php"); ?>

    </div> 
</div>  
<div class="clear"></div>

    <div class="clear"></div>

<?php get_footer(); ?>
</div>
</div>


