<?php
/*
Template Name: Event Child
*/
?>
<?php get_header(); ?>
<div id="container" class="event-child child">
	<?php
	$parent_event = "";
    $connected = new WP_Query( array(
              'connected_type' => 'events_to_subpage',
              'connected_items' => get_queried_object(),
              'nopaging' => true,
            ) );
	if ( $connected->have_posts() ){
		if ( $connected->have_posts() ) : $connected->the_post(); 
			$parent_event = get_the_ID();
		endif; 
	wp_reset_postdata();
	}
	?> 
    
	<?php if($parent_event!=""): ?>
    	<?php $event = get_event($parent_event); ?>
    	<?php
        $img_data = get_post_thumbnails($post->ID);
//		var_dump($img_data);
		if(count($img_data)>0){
			$event['image'] = $img_data ['image'];
		}
		?>
    
		
        <?php include(get_theme_root('template_directory').'/'.get_template().'/eventHeader.php'); ?>  
    <?php endif; ?>
    <div class="clear"></div>     
    <div id="content">
        <div id="content-left">
        	<div class="left_column"><h3 class="section-title"><?php echo $post->post_title; ?></h3></div>
            <div class="right_column">
            	<?php
                 $content = apply_filters('the_content', $post->post_content); 
            	echo $content;  
				?>
            </div>            
        </div>
        <div id="content-right">
            <?php include(get_theme_root('template_directory').'/'.get_template().'/partners.php'); ?>    	
        </div>
    </div>
    <div class="clear" style="padding-bottom:10px;"></div>  
</div>
<?php get_footer(); ?>