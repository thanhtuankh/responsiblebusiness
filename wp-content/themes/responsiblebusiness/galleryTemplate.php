<?php
  
  
  
  /*
    
    
    
    Template Name: Gallery page
    
    
    
  */
  
  
  
?>

<?php get_header(); ?>
<script>
  $(function () {
    
    var counter = 0,
        divs = $('#one,#two');
    
    function showDiv () {
      divs.fadeOut('slow') // hide all divs
            .filter(function (index) { return index == counter % 2; }) // figure out correct div to show
            .fadeIn('slow'); // and show it
      
      counter++;
    }; // function to loop through divs and show correct div
    
    showDiv(); // show first div    
    
    setInterval(function () {
      showDiv(); // show next div
    }, 5 * 1000); // do this every 10 seconds    
    
  });
</script>

<div class="top_smnu"></div>

<div class="clear"></div>

<script>
  
  
  
  jQuery(function ($) {
    
    
    
    //loadVideos('videos-tv', 20, 1, 2);
    
    
    
    //loadVideos('videos-forum', 16, 1, 2);
    
    
    
  }); 
  
  
  
</script>

<?php
  
  
  
  $vid = "";
  
  
  
  if(isset($_GET['vid'])){
    
    
    
    $vid = $_GET['vid'];
    
    
    
  ?>
  
  <script>
    
    
    
    video_popup("<?php echo $vid;?>");
    
    
    
  </script>
  
  <?php
    
    
    
  }
  
  
  
  $featuredposts = get_home_slide_posts();
  
  
  
  $images = array();
  
  
  
  if(count($featuredposts)>0){
    
    
    
    foreach($featuredposts as $key => $p){
      
      
      
      $images[] = get_featured_post_thumbnails($p->ID); 
      
      
      
      $post_data[]  = array('ID' => $p->ID, 'title' => $p->post_title, 'post_excerpt' => $p->post_excerpt, 'post_content' => $p->post_content, 'post_type' => $p->post_type);     
      
      
      
    }
    
    
    
  }
  
  
  
?>

<?php if(count($post_data)==4): ?>

<div class="con_lef">
  
  <?php include("side_new_menu.php");?>
  
</div>

<div class="con_rig">
  
  <div id="fwslider">
    
    <div class="slider_container">
      
      <div class="slider_container">
        
        
        <?php query_posts('showposts=100 & post_type=bannerslider');
          
        if (have_posts()) : ?>
        
        <?php while (have_posts()) : the_post(); ?>
        
        
        
        <div class="slide">   
          
          <a href="<?php echo get_post_meta($post->ID, 'linked_to', true);?>">   
            
            
            <div class="slide_content">
              
              <div class="slide_content_wrap">
                

                
              </div>
              
            </div>   
            
          </a>
          
          
                    <?php  the_post_thumbnail('homebanner'); ?>
          
                    <div class="wr-learn-more">
            <a href="<?php echo get_field('linked_to'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/learn_more.png"></a>
          </div>
          
        </div>
        
        
        
        
        
        <?php endwhile; endif; wp_reset_query(); ?>
        
        
        
       
        
        
        
      </div>
      
    </div>
    
    <div class="timers"></div>
    
    <div class="slidePrev"><span></span></div>
    
    <div class="slideNext"><span></span></div>
    
  </div>

      <div id="content" class="home" style="color:#7d7d7d">

        <?php



        $content = apply_filters('the_content', $post->post_content); 



        echo $content;  



    ?>

    </div>
  
  <div id="header-wrap">
    
    <?php foreach($post_data as $key => $p): ?>
    
    <?php
      
      
      
      //  var_dump($p);
      
      
      
    ?>
    
    <div id="c<?php echo ($key+1);?>" style="display:none;">
      
      <div class="s<?php echo ($key+1);?>">
        
        <?php
          
          
          
          if($images[$key]['small-thumbnail'] == ""){
            
            
            
            $images[$key]['small-thumbnail'] = $images[$key]['medium-thumb'];
            
            
            
          }
          
          
          
        ?>
        
        <div class="slide-small" style="background:url(<?php echo $images[$key]['small-thumbnail']; ?>) no-repeat #01AEF0;">
          
          <div class="slide-small-wrap"> <a href="<?php echo get_permalink($p['ID']); ?>" style="color:#FFFFFF;">
            
          <div class="t_10 t_white"><strong><?php echo $p['title']; ?></a></strong></div>
          
          <?php if($p['post_type'] != "video"): ?>
          
          <div class="read_more"><a href="<?php echo get_permalink($p['ID']); ?>" class="t_10"><strong>Read More</strong></a></div>
          
          <?php endif; ?>
          
          </div>
          
        </div>
        
      </div>
      
    </div>
    
    <?php endforeach;  ?>
    
    
    
  </div>
  
  <?php endif; ?>
  
  <div id="content" class="home" style="color:#7d7d7d">
    
    <?php
      
      
      
      // $content = apply_filters('the_content', $post->post_content); 
      
      
      
      // echo $content;  
      
      
      
    ?>
    
  </div>
  
  <?php //var_dump($events);?>
  
  <?php $events = get_events(); ?>
  
  
  
  <?php $past_events = get_past_events(); ?>  
  
  
  
  <?php //include(get_theme_root('template_directory').'/'.get_template().'/slider-forum.php'); ?>   
  
  
  
    
  
  
  
    <?php //var_dump($events);?>
  
  
  
    <?php $num = count($events); ?>
  
  
  
    <?php $past_num = count($past_events); ?> 
  
  
  
    <?php if($num==0 && ($past_num==0 || $past_num>0)): ?>
  
  
  
    <style>   
    
    
    
    .text-paragraph {   
    
    
    
    margin-top: 20px!important;
    
    
    
    }
    
    
    
  </style>
  
  
  
    <?php endif; ?>
  
  
  
    <?php if($num>0 || $past_num>0): ?>
  
  
  
  
  
  
  
  
  
    
  
  <div class="hlt formIndex">
    
    <div id="event-list">
      
      
      
      <div id="i551" class="actions">
        
        
        
        <!--<div class="row">
          
          
          
          <div class="cell1">
          
          
          
          <div class="line-separator"></div>
          
          
          
          <h3 class="section-title">PAST EVENTS</h3>
          
          
          
          <div class="line-separator"></div>
          
          
          
          </div>
          
          
          
        </div>-->
        
        
        
        <div class="">
          
          
          
          <!--<div class="cell1">
            
            
            
            <div class="line-separator"></div>
            
            
            
            <h3 class="section-title">UPCOMING EVENTS</h3>
            
            
            
            <div class="line-separator"></div>
            
            
            
          </div>  -->
          
          
          
          <!--<div class="cell2"></div>       -->   
          
          
          
          
          
          <div class="row">
            
            
            
            <div class="cell1">
              
              
              
              <div class="line-separator"></div>
              
              
              
              <h3 class="section-title">Gallery</h3>
              
              
              
              <div class="line-separator"></div>
              
              
              
            </div>  
            
            
            
            <div class="cell2"></div>              
            
            
            
            
            
          </div>
                 
          
          <div class="clear"></div> 
          

<!--==============Content gallery================-->
          <div class="">
            
            
            
            <div class="">
              
              <!--===== Loop =========-->
              
         
              
              
              
              
              
            </div>
            
            
            
          </div>


          <!--====== End content gallery======-->
          
        </div> 
        
        
        
      </div>
      
      
      
    </div>
    
    
  </div>
    <div class="clear"></div>
  
  
  
  <?php endif; ?> 

  <div class="clear"></div>
  
  <?php get_footer(); ?>
  
</div>
