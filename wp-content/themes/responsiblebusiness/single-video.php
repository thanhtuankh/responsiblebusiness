<!DOCTYPE html>
<html>    
	<head>        	
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
    <meta property="og:url" content="<?php echo get_permalink(); ?>" />        
    <meta property="og:title" content="<?php echo htmlentities(get_the_title(), ENT_QUOTES); ?>" />  
    <?php $img_data = get_post_thumbnails(get_the_ID()); ?>     
    <?php if(isset($img_data['image'])): ?>  
    	<meta property="og:image" content="<?php echo $img_data['image'];?>" />
    <?php endif; ?>              
    <meta property="og:description" content="<?php echo htmlentities($post->post_excerpt, ENT_QUOTES); ?>" />               
	<?php wp_head(); ?>         
	<script>			
		window.location = "<?php echo get_home_url(); ?>?vid=<?php echo get_the_ID(); ?>";		
    </script>		
    </head>    
    
    <body>     
    </body>
</html>    