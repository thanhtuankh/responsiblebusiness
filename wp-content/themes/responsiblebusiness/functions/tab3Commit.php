<?php
$short_name_cd="tab3";
$theme_options_cd=array(
				array(
					'name'=>'',
					'type'=>'section',
					),
				array('type'=>'table'),
				array('type'=>'open'),

			

				array('name'=>'tab3','type'=>'textarea','id'=>$short_name_cd.'_name','desc'=>'','std'=>''),
				


				array('type'=>'close'),
				array('type'=>'closetable'),

				///////////////////////////////////////

		
);


function add_setting_page_tab3()
{
	add_menu_page(__('Tab 3 Community'.''),__('Tab 3 Community'.''),'manage_options','settingtab3','theme_settings_page_tab3' );
	//add_menu_page(__('Dia chi'.''),__('Dia chi'.''),'manage_options','settings','dia_chi_page' );
}



function theme_setting_init_tab3()
{
	register_setting('theme_setting','theme_setting');
}





function theme_settings_page_tab3(){

	global $theme_options_cd;
	$i=0;
	$message='';
	if($_REQUEST['action']=='save')
	{
		foreach($theme_options_cd as $value)
		{
			update_option( $value['id'], $_REQUEST[$value['id']] );
		}
		foreach($theme_options_cd as $value)
		{
			if(isset($_REQUEST[$value['id']]))
			{
				update_option($value['id'],$_REQUEST[$value['id']] );
			}
			else{delete_option($value['id'] );}
		}
		$message='save';
	}
	else if('reset'==$_REQUEST['action'])
	{
		foreach($theme_options_cd as $value)
		{
			delete_option($value['id'] );
		}
		$message='reset';
	}
	

?>
<div class="wrapper_option">
	<div id="icon-options-general" class="icon32" style="color:#686868;font-size:18px;"></div><h2 style="color:#686868;font-size:18px;margin:7px;padding-top:14px;">Community tab3</h2>
	<?php 
	if($message=="save") echo '<div class="updated setting-error"><p>settings saved</p></div>';
	if($message=="reset")echo '<div class="updated setting-error"><p>settings reset</p></div>';

	?>

	<div class="content_options">
		<form method="post">
<table>
		<?php foreach($theme_options_cd as $value): ?>		
		<?php 
			switch($value['type'])
			{
				case "open":
				break;
				case "close":
				break;
				case "table":
				echo"<table>";
				break;
				case "closetable":
				echo "</table>";
				break;
		?>



		<?php
				case "textarea":
		?>


<textarea style="width:500px;height:300px" type="number" min="2014" max="2050" name="<?php echo $value['id'] ?>"><?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?></textarea>

<!--  				<input style="width:300px" type="text" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>">
 -->
				<?php break; ?>
			
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>


			<?php }


		?>	

		<?php endforeach; ?>
</table>

		<span class="submit" style="display:inline-block;width:100%;"><input type="submit" class="button button-primary button-large" style="margin-bottom:10px;" value="Save"><br>
		<input type="hidden" name="action" value="save">
		</form>
		<form  method="post">
			<input type="submit" name="reset" value="reset" class="button button-primary button-large" style="display:none">
			<input type="hidden" name="action" value="reset">
		</form>
		
	</div>
</div>



<?php } 
add_action('admin_init','theme_setting_init_tab3');
add_action('admin_menu','add_setting_page_tab3');

?>