<?php
$short_name_sp="social";
$theme_options_sp=array(
				array(
					'name'=>'',
					'type'=>'section',
					),
				array('type'=>'table'),
				array('type'=>'open'),

				array('name'=>'Facebook','type'=>'text','id'=>$short_name_sp.'_fb','desc'=>'','std'=>''),
				array('name'=>'Twitter','type'=>'text','id'=>$short_name_sp.'_tw','desc'=>'','std'=>''),
				array('name'=>'Linkedin','type'=>'text','id'=>$short_name_sp.'_linke','desc'=>'','std'=>''),
				array('name'=>'Youtube','type'=>'text','id'=>$short_name_sp.'_yt','desc'=>'','std'=>''),
				array('name'=>'Instar','type'=>'text','id'=>$short_name_sp.'_instar','desc'=>'','std'=>''),
				array('name'=>'Slideshare','type'=>'text','id'=>$short_name_sp.'_slide','desc'=>'','std'=>''),

				array('type'=>'close'),
				array('type'=>'closetable'),

				///////////////////////////////////////

		
);


function add_setting_page_social()
{
	add_menu_page(__('Social Setting'.''),__('Social Setting'.''),'manage_options','socialsetting','theme_settings_page_social' );
	//add_menu_page(__('Dia chi'.''),__('Dia chi'.''),'manage_options','settings','dia_chi_page' );
}



function theme_setting_init_social()
{
	register_setting('theme_setting','theme_setting');
}





function theme_settings_page_social(){

	global $theme_options_sp;
	$i=0;
	$message='';
	if($_REQUEST['action']=='save')
	{
		foreach($theme_options_sp as $value)
		{
			update_option( $value['id'], $_REQUEST[$value['id']] );
		}
		foreach($theme_options_sp as $value)
		{
			if(isset($_REQUEST[$value['id']]))
			{
				update_option($value['id'],$_REQUEST[$value['id']] );
			}
			else{delete_option($value['id'] );}
		}
		$message='save';
	}
	else if('reset'==$_REQUEST['action'])
	{
		foreach($theme_options_sp as $value)
		{
			delete_option($value['id'] );
		}
		$message='reset';
	}
	

?>
<div class="wrapper_option">
	<div id="icon-options-general" class="icon32" style="color:#686868;font-size:18px;"></div><h2 style="color:#686868;font-size:18px;margin:7px;padding-top:14px;">Social setting</h2>
	<p>(Type link for each social input)</p>
	<?php 
	if($message=="save") echo '<div class="updated setting-error"><p>settings saved</p></div>';
	if($message=="reset")echo '<div class="updated setting-error"><p>settings reset</p></div>';

	?>

	<div class="content_options">
		<form method="post">
<table>
		<?php foreach($theme_options_sp as $value): ?>		
		<?php 
			switch($value['type'])
			{
				case "open":
				break;
				case "close":
				break;
				case "table":
				echo"<table>";
				break;
				case "closetable":
				echo "</table>";
				break;
		?>

	
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>

				<?php
				case "text":
		?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td><input style="width:300px" type="text" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>">
				<small><?php echo $value['desc']; ?></small><p></p></td>
				</tr>

				<?php break; ?>
			
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>


			<?php }


		?>	

		<?php endforeach; ?>
</table>

		<span class="submit" style="display:inline-block;width:100%;"><input type="submit" class="button button-primary button-large" style="margin-bottom:10px;" value="Save"><br>
		<input type="hidden" name="action" value="save">
		</form>
		<form  method="post">
			<input type="submit" name="reset" value="reset" class="button button-primary button-large" style="display:none">
			<input type="hidden" name="action" value="reset">
		</form>
		
	</div>
</div>

<?php } 
add_action('admin_init','theme_setting_init_social');
add_action('admin_menu','add_setting_page_social');

?>