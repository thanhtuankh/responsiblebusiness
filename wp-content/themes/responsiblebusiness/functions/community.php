<?php
$short_name_cd="community";
$theme_options_commu=array(
				array(
					'name'=>'',
					'type'=>'section',
					),
				array('type'=>'table'),
				array('type'=>'open'),

				array('name'=>'Webinar text','type'=>'textarea','id'=>$short_name_cd.'_webinar','desc'=>'','std'=>''),
				array('name'=>'Young leader page text','type'=>'textarea','id'=>$short_name_cd.'_youngleader','desc'=>'','std'=>''),
				array('name'=>'Podcasts text','type'=>'textarea','id'=>$short_name_cd.'_podcasts','desc'=>'','std'=>''),



				array('type'=>'close'),
				array('type'=>'closetable'),

				///////////////////////////////////////

		
);


function add_setting_page_tab3()
{
	add_menu_page(__('Community text'.''),__('Community text'.''),'manage_options','settingtab3','theme_settings_page_tab3' );
	//add_menu_page(__('Dia chi'.''),__('Dia chi'.''),'manage_options','settings','dia_chi_page' );
}



function theme_setting_init_tab3()
{
	register_setting('theme_setting','theme_setting');
}





function theme_settings_page_tab3(){

	global $theme_options_commu;
	$i=0;
	$message='';
	if($_REQUEST['action']=='save')
	{
		foreach($theme_options_commu as $value)
		{
			update_option( $value['id'], $_REQUEST[$value['id']] );
		}
		foreach($theme_options_commu as $value)
		{
			if(isset($_REQUEST[$value['id']]))
			{
				update_option($value['id'],$_REQUEST[$value['id']] );
			}
			else{delete_option($value['id'] );}
		}
		$message='save';
	}
	else if('reset'==$_REQUEST['action'])
	{
		foreach($theme_options_commu as $value)
		{
			delete_option($value['id'] );
		}
		$message='reset';
	}
	

?>
<div class="wrapper_option">
	<div id="icon-options-general" class="icon32" style="color:#686868;font-size:18px;"></div><h2 style="color:#686868;font-size:18px;margin:7px;padding-top:14px;">Community text</h2>
	<?php 
	if($message=="save") echo '<div class="updated setting-error"><p>settings saved</p></div>';
	if($message=="reset")echo '<div class="updated setting-error"><p>settings reset</p></div>';

	?>
	
<style type="text/css">
	#wp-community_webinar-wrap, #wp-community_youngleader-wrap, #wp-community_podcasts-wrap{
		width: 400px;
		margin-bottom: 50px;
	}


</style>
	<div class="content_options">
		<form method="post">
<table>
		<?php foreach($theme_options_commu as $value): ?>		
		<?php 
			switch($value['type'])
			{
				case "open":
				break;
				case "close":
				break;
				case "table":
				echo"<table>";
				break;
				case "closetable":
				echo "</table>";
				break;
		?>



		<?php
				case "textarea":
		?>

<label><?php echo $value['name'] ?></label><br>
<!--<textarea id="<?php //echo $value['id'] ?>" style="width:500px;height:300px" type="number" min="2014" max="2050" name="<?php //echo $value['id'] ?>"><?php //if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?></textarea>
 -->

 <?php 
if(get_settings($value['id'])!=""):$contentt = stripcslashes(get_settings($value['id']));else:$contentt = $value['std'];endif;
 wp_editor($contentt,$value['id']);

?>

				<?php break; ?>
			
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>


			<?php }


		?>	

		<?php endforeach; ?>
</table>

		<span class="submit" style="display:inline-block;width:100%;"><input type="submit" class="button button-primary button-large" style="margin-bottom:10px;" value="Save"><br>
		<input type="hidden" name="action" value="save">
		</form>
		<form  method="post">
			<input type="submit" name="reset" value="reset" class="button button-primary button-large" style="display:none">
			<input type="hidden" name="action" value="reset">
		</form>
		
	</div>
</div>



<?php } 
add_action('admin_init','theme_setting_init_tab3');
add_action('admin_menu','add_setting_page_tab3');

?>