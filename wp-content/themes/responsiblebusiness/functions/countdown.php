<?php
$short_name_cd="countdown";
$theme_options_cd=array(
				array(
					'name'=>'',
					'type'=>'section',
					),
				array('type'=>'table'),
				array('type'=>'open'),

				array('name'=>'Year','type'=>'numberyear','id'=>$short_name_cd.'_year','desc'=>'','std'=>''),
				array('name'=>'Month','type'=>'numbermonth','id'=>$short_name_cd.'_month','desc'=>'','std'=>''),
				array('name'=>'Day','type'=>'numberday','id'=>$short_name_cd.'_day','desc'=>'','std'=>''),
				array('name'=>'Hour','type'=>'numberhour','id'=>$short_name_cd.'_hour','desc'=>'','std'=>''),
				array('name'=>'Minute','type'=>'numberms','id'=>$short_name_cd.'_minute','desc'=>'','std'=>''),
				array('name'=>'Seconds','type'=>'numberms','id'=>$short_name_cd.'_second','desc'=>'','std'=>''),

				array('name'=>'Name','type'=>'text','id'=>$short_name_cd.'_name','desc'=>'','std'=>''),
				array('name'=>'Address','type'=>'text','id'=>$short_name_cd.'_desc','desc'=>'','std'=>''),
				array('name'=>'Link redirect','type'=>'text','id'=>$short_name_cd.'_link_re','desc'=>'','std'=>''),

				array('name'=>'Status','type'=>'select','id'=>$short_name_cd.'_status','value'=>array('Show',"Hide")),

				array('name'=>'Background IMG 270x490','id'=>'countdown_image','desc'=>'bt', 'std'=>'','link'=>'countdown_link_image','type'=>'image'),

				array('type'=>'close'),
				array('type'=>'closetable'),

				///////////////////////////////////////

		
);


function add_setting_page_sp()
{
	add_menu_page(__('Count down setting'.''),__('Count down setting'.''),'manage_options','settingSP','theme_settings_page_sp' );
	//add_menu_page(__('Dia chi'.''),__('Dia chi'.''),'manage_options','settings','dia_chi_page' );
}



function theme_setting_init_sp()
{
	register_setting('theme_setting','theme_setting');
}





function theme_settings_page_sp(){

	global $theme_options_cd;
	$i=0;
	$message='';
	if($_REQUEST['action']=='save')
	{
		foreach($theme_options_cd as $value)
		{
			update_option( $value['id'], $_REQUEST[$value['id']] );
		}
		foreach($theme_options_cd as $value)
		{
			if(isset($_REQUEST[$value['id']]))
			{
				update_option($value['id'],$_REQUEST[$value['id']] );
			}
			else{delete_option($value['id'] );}
		}
		$message='save';
	}
	else if('reset'==$_REQUEST['action'])
	{
		foreach($theme_options_cd as $value)
		{
			delete_option($value['id'] );
		}
		$message='reset';
	}
	

?>
<div class="wrapper_option">
	<div id="icon-options-general" class="icon32" style="color:#686868;font-size:18px;"></div><h2 style="color:#686868;font-size:18px;margin:7px;padding-top:14px;">Count down setting</h2>
	<?php 
	if($message=="save") echo '<div class="updated setting-error"><p>settings saved</p></div>';
	if($message=="reset")echo '<div class="updated setting-error"><p>settings reset</p></div>';

	?>

	<div class="content_options">
		<form method="post">
<table>
		<?php foreach($theme_options_cd as $value): ?>		
		<?php 
			switch($value['type'])
			{
				case "open":
				break;
				case "close":
				break;
				case "table":
				echo"<table>";
				break;
				case "closetable":
				echo "</table>";
				break;
		?>

		<?php
				case "numberyear":
		?>
				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td><input style="width:300px" type="number" min="2014" max="2050" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>">
				<small><?php echo $value['desc']; ?></small><p></p></td>
				</tr>

				<?php break; ?>
			
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>


				<?php
				case "number":
		?>

		<?php
				case "numbermonth":
		?>
				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td><input style="width:300px" type="number" min="1" max="12" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>">
				<small><?php echo $value['desc']; ?></small><p></p></td>
				</tr>

				<?php break; ?>
			
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>


				<?php
				case "numberday":
		?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td><input style="width:300px" type="number" min="0" max="31" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>">
				<small><?php echo $value['desc']; ?></small><p></p></td>
				</tr>

				<?php break; ?>
			
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>

					<?php
				case "numberhour":
		?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td><input style="width:300px" type="number" min="0" max="24" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>">
				<small><?php echo $value['desc']; ?></small><p></p></td>
				</tr>

				<?php break; ?>
			
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>


					<?php
				case "numberms":
		?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td><input style="width:300px" type="number" min="0" max="60" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>">
				<small><?php echo $value['desc']; ?></small><p></p></td>
				</tr>

				<?php break; ?>
			
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>



				<?php
				case "text":
		?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td><input style="width:300px" type="text" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>">
				<small><?php echo $value['desc']; ?></small><p></p></td>
				</tr>

				<?php break; ?>

				
		<?php
				case "select":
		?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td>
				
				<select name="<?php echo $value['id'] ?>" >
				<?php foreach($value['value'] as $val): 
					if($val==get_settings($value['id'])){
						$selected = 'selected';
					}else{
						$selected='';
					}
				?>
				<option value="<?php echo $val; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
				<?php endforeach; ?>
				</select>

				</td>
				</tr>

				<?php break; ?>
			

				<?php case "image": ?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td>
				<input id="<?php echo $value['id']?>" type="text" size="70" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>" />
				<input id='<?php echo $value['desc'] ?>' type="button" value="Upload Image" />

				<script type="text/javascript" src="<?php bloginfo('template_directory' ); ?>/js/jquery-1.8.2.min.js"></script>

				<script type="text/javascript">
				$(document).ready(function() {
				$("#"+"<?php echo $value['desc'] ?>").click(function() {
				 formfield = $(this).prev('input');
				 tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
				 $('#TB_iframeContent').css("width","670px");
				 return false;
				});
				window.send_to_editor = function(html) {
				 imgurl = $('img',html).attr('src');
				 $(formfield).val(imgurl);
				 tb_remove();
				}
				});
				</script>
				<br>

				</td>
				</tr>

				<tr>
				<td></td>
				<td><img width='120' src="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo ' ';endif; ?>"><br><br></td>
				</tr>

				<?php break; ?>

				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>


			<?php }


		?>	

		<?php endforeach; ?>
</table>

		<span class="submit" style="display:inline-block;width:100%;"><input type="submit" class="button button-primary button-large" style="margin-bottom:10px;" value="Save"><br>
		<input type="hidden" name="action" value="save">
		</form>
		<form  method="post">
			<input type="submit" name="reset" value="reset" class="button button-primary button-large" style="display:none">
			<input type="hidden" name="action" value="reset">
		</form>
		
	</div>
</div>

<?php } 
add_action('admin_init','theme_setting_init_sp');
add_action('admin_menu','add_setting_page_sp');

?>