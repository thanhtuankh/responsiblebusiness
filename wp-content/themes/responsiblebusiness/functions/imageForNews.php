<?php
$short_name_img_news="image_news";
$theme_options_cd=array(
				array(
					'name'=>'',
					'type'=>'section',
					),
				array('type'=>'table'),
				array('type'=>'open'),


				array('name'=>'Status','type'=>'select','id'=>$short_name_img_news.'_status','value'=>array('Show',"Hide")),
				array('name'=>'Location','type'=>'select','id'=>$short_name_img_news.'_location','value'=>array('Top',"Footer","Top-Footer")),

				array('name'=>'Image (615x262)','id'=>$short_name_img_news.'_image','desc'=>'bt', 'std'=>'','link'=>'image_news_link','type'=>'image'),

				array('name'=>'Url','id'=>$short_name_img_news.'_url','desc'=>'bt', 'std'=>'','type'=>'text'),


				array('type'=>'close'),
				array('type'=>'closetable'),

				///////////////////////////////////////

		
);


function add_setting_page_img_for_news()
{
	add_menu_page(__('Banner for news'.''),__('Banner for news'.''),'manage_options','BannerForNews','theme_settings_page_img_for_news' );
	//add_menu_page(__('Dia chi'.''),__('Dia chi'.''),'manage_options','settings','dia_chi_page' );
}



function theme_setting_init_img_for_news()
{
	register_setting('theme_setting','theme_setting');
}





function theme_settings_page_img_for_news(){

	global $theme_options_cd;
	$i=0;
	$message='';
	if($_REQUEST['action']=='save')
	{
		foreach($theme_options_cd as $value)
		{
			update_option( $value['id'], $_REQUEST[$value['id']] );
		}
		foreach($theme_options_cd as $value)
		{
			if(isset($_REQUEST[$value['id']]))
			{
				update_option($value['id'],$_REQUEST[$value['id']] );
			}
			else{delete_option($value['id'] );}
		}
		$message='save';
	}
	else if('reset'==$_REQUEST['action'])
	{
		foreach($theme_options_cd as $value)
		{
			delete_option($value['id'] );
		}
		$message='reset';
	}
	

?>
<div class="wrapper_option">
	<div id="icon-options-general" class="icon32" style="color:#686868;font-size:18px;"></div><h2 style="color:#686868;font-size:18px;margin:7px;padding-top:14px;">Image banner for news</h2>
	<?php 
	if($message=="save") echo '<div class="updated setting-error"><p>settings saved</p></div>';
	if($message=="reset")echo '<div class="updated setting-error"><p>settings reset</p></div>';

	?>

	<div class="content_options">
	<p>Gallery shortcode: [image_news] (copy/paste to add it inner content body)</p>
		<form method="post">
<table>
		<?php foreach($theme_options_cd as $value): ?>		
		<?php 
			switch($value['type'])
			{
				case "open":
				break;
				case "close":
				break;
				case "table":
				echo"<table>";
				break;
				case "closetable":
				echo "</table>";
				break;
		?>

	


		<?php
				case "select":
		?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td>
				
				<select name="<?php echo $value['id'] ?>" >
				<?php foreach($value['value'] as $val): 
					if($val==get_settings($value['id'])){
						$selected = 'selected';
					}else{
						$selected='';
					}
				?>
				<option value="<?php echo $val; ?>" <?php echo $selected; ?>><?php echo $val; ?></option>
				<?php endforeach; ?>
				</select>

				</td>
				</tr>

				<?php break; ?>

				<?php case "image": ?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td>
				<input id="<?php echo $value['id']?>" type="text" size="70" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>" />
				<input id='<?php echo $value['desc'] ?>' type="button" value="Upload Image" />

				<script type="text/javascript" src="<?php bloginfo('template_directory' ); ?>/js/jquery-1.8.2.min.js"></script>

				<script type="text/javascript">
				$(document).ready(function() {
				$("#"+"<?php echo $value['desc'] ?>").click(function() {
				 formfield = $(this).prev('input');
				 tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
				 $('#TB_iframeContent').css("width","670px");
				 return false;
				});
				window.send_to_editor = function(html) {
				 imgurl = $('img',html).attr('src');
				 $(formfield).val(imgurl);
				 tb_remove();
				}
				});
				</script>
				<br>

				</td>
				</tr>

				<tr>
				<td></td>
				<td><img width='120' src="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo ' ';endif; ?>"><br><br></td>
				</tr>

				<?php break; ?>

				<?php
				case "text":
		?>


				<tr>
				<td><label style="font-weight:bold;"><?php echo $value['name'] ?></label></td>
				<td><input style="width:300px" type="text" name="<?php echo $value['id'] ?>" value="<?php if(get_settings($value['id'])!=""):echo stripcslashes(get_settings($value['id']));else:echo $value['std'];endif; ?>">
				</tr>

				<?php break; ?>


			
				<?php
				case "section":

				$i++;
				?>
				<h3 style="margin:30px 0px 10px 0px;"><?php echo $value['name']?></h3>
				
				<?php 

				break; ?>




			<?php }


		?>	

		<?php endforeach; ?>
</table>

		<span class="submit" style="display:inline-block;width:100%;"><input type="submit" class="button button-primary button-large" style="margin-bottom:10px;" value="Save"><br>
		<input type="hidden" name="action" value="save">
		</form>
		<form  method="post">
			<input type="submit" name="reset" value="reset" class="button button-primary button-large" style="display:none">
			<input type="hidden" name="action" value="reset">
		</form>
		
	</div>
</div>

<?php } 
add_action('admin_init','theme_setting_init_img_for_news');
add_action('admin_menu','add_setting_page_img_for_news');

function add_image_shortcode() {
      if( (get_option('image_news_status') == 'Show') && (get_option('image_news_image'))  ){
      	 $image = "<img src='".get_option('image_news_image')."'>";
      }
      return $image;
}
add_shortcode( 'image_news', 'add_image_shortcode' );
?>