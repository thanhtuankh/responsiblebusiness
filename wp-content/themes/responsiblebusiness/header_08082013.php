<!DOCTYPE html>

<html>

    <head>    

    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta property="og:url" content="<?php echo get_home_url(); ?>" />

        <meta property="og:title" content="<?php bloginfo('name'); ?>" />   

        <meta property="og:image" content="http://responsiblebusiness.com/wp-content/themes/responsiblebusiness/images/logo_200.png" />     

        <meta property="og:description" content="<?php echo get_bloginfo ( 'description' ); ?>" />

        <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery-1.8.2.min.js'></script>    

        <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jQuery.BlackAndWhite.min.js'></script>        

        <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/easySlider1.7.js?v=5'></script>

        <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/functions.js?v=18'></script>        

        <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery.mCustomScrollbar.concat.min.js'></script>

        <script type='text/javascript' src='<?php echo bloginfo('template_directory');?>/js/jquery.simplemodal.js'></script>

        <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/jquery.mCustomScrollbar.css' rel='stylesheet' media='screen' /> 

        <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/basic.css' rel='stylesheet' media='screen' />
        <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/forumstyle' rel='stylesheet' media='screen' />

        <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/screen.css?v=2' rel='stylesheet' media='screen' />

        <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>

		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-517642bf5ed47108"></script>        

        

        <script src="http://jwpsrv.com/library/5UsIaq8FEeKsVSIACpYGxA.js"></script>

               

        <!-- IE6 "fix" for the close png image -->

        <!--[if lt IE 7]>

        <link type='text/css' href='<?php echo bloginfo('template_directory');?>/css/basic_ie.css' rel='stylesheet' media='screen' />

        <![endif]-->     

    	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>?v=30" />

    	<title>

            <?php if(is_front_page()): ?>

                <?php bloginfo('name'); ?>

            <?php else: ?>

                <?php wp_title( '', true);?>

            <?php endif; ?>            

        </title>

        <?php wp_head(); ?>



<script type="text/javascript">



  var _gaq = _gaq || [];

  _gaq.push(['_setAccount', 'UA-5927070-1']);

  _gaq.push(['_trackPageview']);



  (function() {

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();



</script>

 <!-- POPUP STRTAS HERE -->
 <script src="<?php bloginfo('template_directory')?>/Scripts/jquery-1.4.1.js" type="text/javascript"></script>
<!--    <script src="Scripts/jquery.msgBox.min.js" type="text/javascript"></script>-->
    <script src="<?php bloginfo('template_directory')?>/Scripts/jquery.msgBox.js" type="text/javascript"></script>
    <link href="<?php bloginfo('template_directory')?>/Scripts/msgBoxLight.css" rel="stylesheet" type="text/css" />
    <!--<link href="Scripts/styles.css" rel="stylesheet" type="text/css" />-->

   

 

    <script type="text/javascript">


      
    </script>

<!-- pop up ends here -->



    </head>

    <body>   

    

   

    	<div id="loader" style="display:none;"><img src="<?php echo bloginfo('template_directory');?>/images/ajax-loader.gif" /></div>  

    	<div id="basic-modal-content"></div>

    	<div id="wrapper">   

            <div id="header">

               <div class="line-separator"></div>	

               <div id="header-left">

               		<a href="<?php echo get_home_url(); ?>"><img src="<?php echo bloginfo('template_directory');?>/images/logo.png" width="270" height="88" alt="" /></a>               </div>               

               <div id="header-right">               

                    <div id="top">

                        <div id="search-form">

                            <form role="search" method="get" id="searchform" action="<?php echo get_home_url(); ?>">

                            <label class="upper"> <input class="search" type="submit" value="SEARCH" /></label>          

                            <input type="text" value="" name="s" id="s" />                                              

                            </form>  

                        </div> 

                        <ul id="top-menu">   
                        <li style="padding-right:8px;"><a href="https://twitter.com/@Responsible_Biz" class="upper" target="_blank"><img src="<?php echo bloginfo('template_directory');?>/forum-images/twitter-ico.png"></a></li> 
                        <li style="padding-right:8px;"><a href="http://www.youtube.com/globalinitiativestv" class="upper" target="_blank"><img src="<?php echo bloginfo('template_directory');?>/forum-images/youtube-ico.png" ></a></li>    
                        <li ><a href="http://www.linkedin.com/groups/Responsible-Business-4975780" class="upper" target="_blank"><img src="<?php echo bloginfo('template_directory');?>/forum-images/linkedin-ico.png" ></a></li>                 	

                          

                            

                           

                            <li class="menu-items-separator"></li>

                        </ul>                

                	</div>

                    <div class="clear"></div>                    

                    <?php generate_primary_menu(); ?>   
                      
                    <?php if(is_page(8) || is_page(1017)|| is_page(1019) || is_page(1021) || is_page(1023) || is_page(1025) || is_page(1028)  ){ ?> 
                    <div class="line-separator" style="margin-left:15px; width:480px;"></div>              
					<div class="blue-menu">
                    <ul id="top_menu_sub">
                    
                    <li> <a href="<?php echo get_permalink(1017) ?>"> ABOUT </a>
                    
                    <?php /*?><ul>
                    <li> <a href="<?php echo get_permalink(1017) ?>"> VENUE  </a></li>  
                     
                    <li> <a href="<?php echo get_permalink(1021) ?>"> TRAVEL & ACCOMMODATION </a></li>          
                    <li> <a href="<?php echo get_permalink(1024) ?>"> SUSTAINABILITY  </a></li>  
                    </ul><?php */?>
                    
                    </li>  
                     
                    <li> <a href="<?php echo get_permalink(1019) ?>">SPEAKERS </a></li>          
                    <li> <a href="<?php echo get_permalink(1028) ?>">AGENDA  </a></li>       
                    <li> <a href="<?php echo get_permalink(1021) ?>"> REGISTER </a></li>        
                    <li> <a href="<?php echo get_permalink(1023) ?>"> PARTNERS </a></li>        
                    <li> <a href="<?php echo get_permalink(1025) ?>"> MEDIA</a></li>  
                    
                   
                    </ul>
                    
                    </div>
                    <?php } ?>
               </div>

                <div class="clear"></div>

                <div class="line-separator" style="margin-top:46px;"></div>

            </div>                  