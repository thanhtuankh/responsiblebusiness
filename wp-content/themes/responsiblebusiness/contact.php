<?php

/*

Template Name: Contact Us

*/

?>
<?php get_header(); ?>
<script>
$(function () {

    var counter = 0,
        divs = $('#one,#two');

    function showDiv () {
        divs.fadeOut('slow') // hide all divs
            .filter(function (index) { return index == counter % 2; }) // figure out correct div to show
            .fadeIn('slow'); // and show it

        counter++;
    }; // function to loop through divs and show correct div

    showDiv(); // show first div    

    setInterval(function () {
        showDiv(); // show next div
    }, 5 * 1000); // do this every 10 seconds    

});
</script>
<div class="top_smnu"></div>
<div class="clear"></div>
<div class="con_lef">
<?php include("side_new_menu.php"); ?>
</div>
<div class="con_rig">
<div class="contact_banner">
 <?php the_post_thumbnail();?> 

</div>

   <div id="content" class="home" style="color:#7d7d7d">

    Interested in joining the movement? If you want to become an agent for change or have any questions regarding our initiatives or future possibilities, feel free to contact us now in order to: 
<ul>
    <li> Discuss event sponsorship and stakeholder engagement opportunities.</li>
    <li> Organise event speaking opportunities or partnering.</li>
    <li> Discuss video production and sustainability content distribution.</li>
</ul>

    </div>   


<div id="container" class="ccontent">	
<div class="hlt" style="width:100%">
    <h3 class="section-title"><?php echo get_the_title(); ?></h3>

    <div class="line-separator" style="margin-bottom:18px;"></div>

	<?php

	 	$content = apply_filters('the_content', $post->post_content); 

	  	echo $content; 

	?>
</div>

<div class="clear"></div>
<?php get_footer(); ?>
</div>
</div>
