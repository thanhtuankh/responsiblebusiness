<?php
$img_data = array();
for($i=1; $i<=4;$i++)
{
	$image_name = 'slide-image-'.$i;	
	if (MultiPostThumbnails::has_post_thumbnail(get_post_type(), $image_name)) {
		$image_id = MultiPostThumbnails::get_post_thumbnail_id( get_post_type(), $image_name, $post->ID );		
		$image_thumb_url[] = wp_get_attachment_image_src( $image_id,'medium-thumb');
		$image_full_url[] = wp_get_attachment_image_src( $image_id,'full-image' ); 
		$img_data[]  = wp_get_attachment($image_id);		
	}
}
if(count($img_data)>0){
?>
<div id="slider2" data-slider-count="<?php echo count($img_data); ?>">
    <ul>	
        <?php foreach($image_full_url as $key => $img): ?>	               
        <li style="background:url(<?php echo $img[0]; ?>) no-repeat #01AEF0;"></li>		
        <?php endforeach; ?>
    </ul>
</div>


<?php	
}
if(count($img_data)==1){
?>
<div style="margin-top:20px;"></div>
<?php
}
?>