<?php if(count($lastpost)>0): ?>



<?php



$img_data = get_post_thumbnails($lastpost->ID);



?>

<style>

.nav li:nth-child(4) a{color:#00BFF3 !important;}



</style>

<?php  ?>

<div id="header-wrap">

 <div id="header-wrap-img" class="b_wr_img_news_page">  
	<a href="<?php echo get_permalink($lastpost->ID); ?>" class="bl_hd_d">


<?php 
 $thum_url=wp_get_attachment_url( get_post_thumbnail_id($lastpost->ID) );
?>

<img src="<?php echo $thum_url; ?>" class="lasted_img" />


        </a>                		   

</div>



 <div class="b-wr-text-news-page">

        <div class="img-text imgnws">



            <div class="img-text-wrap">



            	<div class="upper t10 b_date_news"><?php echo date('j F Y', strtotime($lastpost->post_date));?></div>



                <div class="l_black t18"><strong><?php 
				
				if(get_post_meta($lastpost->ID, 'banner_title', true)!="")
				{
					
					echo get_post_meta($lastpost->ID, 'banner_title', true);
				}
				else
				{
					echo truncate_text($lastpost->post_title, 80);
				}
				
				
				//echo truncate_text($lastpost->post_title, 80);
                
                ?>
                </strong></div>



                <div class="t10 t_white"><?php echo get_post_meta($lastpost->ID, 'alternative_title', true); ?></div>



                <div class="t11 l_black"><?php  
				
				if(get_post_meta($lastpost->ID, 'banner_description', true)!="")
				{
					
					echo get_post_meta($lastpost->ID, 'banner_description', true);
				}
				else
				{
					echo truncate_text($lastpost->post_content, 310);
				}
				
				//echo truncate_text($lastpost->post_content, 310);
				 //echo get_post_meta($post->ID, 'featured_description', true);
				 ?></div> 



                <div class="read_more mrgtp"><a href="<?php echo get_permalink($lastpost->ID); ?>" style="color:#fff !important;">READ MORE</a></div>    



            </div>



        </div>

        </div>




    </div>     


    <div id="content" class="home" style="color:#7d7d7d">

We curate articles from our global network, including thought-leaders from reputed sustainability organisations. Our published articles are written by sustainability experts and members of our community. We aim to provide updates on the upcoming RBF events, showcase outcome reports and recommendations  from past events. 
    </div>   





<?php endif; ?>