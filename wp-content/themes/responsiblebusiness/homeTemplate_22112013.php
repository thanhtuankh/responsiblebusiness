<?php

/*

Template Name: Home Template 1

*/

?>

<?php get_header(); ?>

<script>

jQuery(function ($) {

	loadVideos('videos-tv', 20, 1, 2);

	loadVideos('videos-forum', 16, 1, 2);

});	

</script>

<?php

$vid = "";

if(isset($_GET['vid'])){

	$vid = $_GET['vid'];

	?>

    <script>

	video_popup("<?php echo $vid;?>");

	</script>

    <?php

}

$featuredposts = get_home_slide_posts();

$images = array();

if(count($featuredposts)>0){

	foreach($featuredposts as $key => $p){

		$images[] = get_featured_post_thumbnails($p->ID);	

		$post_data[]  = array('ID' => $p->ID, 'title' => $p->post_title, 'post_excerpt' => $p->post_excerpt, 'post_content' => $p->post_content, 'post_type' => $p->post_type);			

	}

}

?>

<?php if(count($post_data)==4): ?>

<div id="header-wrap">

    <?php foreach($post_data as $key => $p): ?>	   

    <?php

  //  var_dump($p);

	?> 

    <div id="c<?php echo ($key+1);?>" style="display:none;">

    	

        <div class="s<?php echo ($key+1);?>">

        	<?php

            if($images[$key]['small-thumbnail'] == ""){

				$images[$key]['small-thumbnail'] = $images[$key]['medium-thumb'];

			}

			?>           

            <div class="slide-small" style="background:url(<?php echo $images[$key]['small-thumbnail']; ?>) no-repeat #01AEF0;"> 

            	<div class="slide-small-wrap">

               		<a href="<?php echo get_permalink($p['ID']); ?>" style="color:#FFFFFF;">

                        <div class="t_10 t_white"><strong><?php echo $p['title']; ?></a></strong></div>

                        <?php if($p['post_type'] != "video"): ?>   

                        <div class="read_more"><a href="<?php echo get_permalink($p['ID']); ?>" class="t_10"><strong>Read More</strong></div> 

                        <?php endif; ?> 

                    </a>                    	

                </div>   

            </div>            

        </div>       

    </div>        

    <?php endforeach; ?>

    <div id="slider">   	 	

        <ul>	

            <?php foreach($post_data as $key => $p): ?>	            	      

            <li style="background:#01AEF0; position:relative;">	

            	<?php if($images[$key]['feature-image']): ?>	

                <?php if($p['post_type'] == "video"): ?>

           			<a href="<?php echo get_permalink($p['ID']); ?>"><img src="<?php echo bloginfo('template_directory');?>/images/play_button.png" class="video-icon" /></a>   

        		<?php endif; ?>   		   

            	<a href="<?php echo get_permalink($p['ID']); ?>"><img src="<?php echo $images[$key]['feature-image']; ?>" style="width:647px; height:280px; float:left;" width="647px" height="280px" /></a>         

                <?php endif; ?>

                <div class="img-text">                	

                	<a href="<?php echo get_permalink($p['ID']); ?>">

               	 	<div class="img-text-wrap">

                    	<div class="t_26">

                        	<?php 

								if($p['ID']==1032){?>

                                

                                

                                <div id="event-text">

                                    <div id="event-text-wrap">

                                        <div class="event-cat"> <strong><?php echo get_post_meta(8, 'banner_head_1', true);?><br>

                                            <span class="t_white"><?php echo get_post_meta(8, 'banner_head_2', true);?></span> </strong> </div>

                                        <div class="event-details" style="margin-bottom: 35px !important;">

                                            <div class="t14"><strong> <?php echo get_post_meta(8, 'banner_text_1', true);?> </strong></div>

                                            <div class="t10"> <strong> <span class="t_white"> <?php echo get_post_meta(8, 'banner_text_2', true);?> </span> </strong> </div>

                                        </div>

                                    </div>

                                </div>

									

                                        <?php /*?><div id="event-text">

                                              <div id="event-text-wrap">

                                                 <div class="event-cat">

                                                     <strong>RESPONSIBLE BUSINESS FORUM<br>

                                                     <span class="t_white">ON SUSTAINABLE DEVELOPMENT</span> </strong>

                                                 </div>

                                                    <div class="event-details" style="margin-top:20px; position:relative;">

                                                    

                                                     <div class="t14" ><strong>

                                                         Transformation, Growth and the Green Economy 

                                                         </strong></div>

                                                         

                                                        <div class="t10">

                                                            <strong>    

                                                                                    

                                                                <span class="t_white">

                                                                 Singapore<br> 

                                            25 - 26 November 2013                            </span> 

                                                            </strong>

                                                        </div>     

                                                    </div> 

                                                </div>

                                             </div><?php */?>

                                             

									

									

									

								<?php } else {

							

							

							echo truncate_text($p['title'], 70, true);} ?>                         

                        </div>

                		<div class="t_16">                        	

                        	<?php  if($p['post_tpye'] == "post"): ?>                        	

                        		<?php echo truncate_text($p['post_excerpt'], 130); ?>	

                             <?php else: ?>

                             	<?php echo truncate_text(strip_tags($p['post_content']), 130); ?>	

                             <?php endif; ?>   						 

                        </div>  

                        <?php if($p['post_type'] != "video"): ?>                  

                    		<div class="read_more"><a href="<?php echo get_permalink($p['ID']); ?>"><strong>READ MORE</strong></a></div>

                        <?php endif; ?>       

                    </div>

                    </a>

               </div>               

            </li>		

            <?php endforeach; ?>

        </ul>

    </div>

    <div class="clear"></div>

    <div id="slider-thumb">

    	<div id="col3" style="float:left;"></div>

        <div id="col2" style="float:left;"></div>

        <div id="col1" style="float:left;"></div>

    </div>       	

    <div class="clear"></div>

</div>

<?php endif; ?>

<div id="content" class="home">

    <?php

        $content = apply_filters('the_content', $post->post_content); 

        echo $content;  

    ?>

</div>



<div id="videos"></div>



<div id="home-videos" style="border:0px solid black">

	<div class="line-separator"></div>

    <h3 class="section-title upper">Featured Videos</h3>

    <div class="line-separator"></div>

    

    <div id="i551" class="actions">           		

        <div class="row">

            <div class="cell1">

               <div class="home-videos-wrap t10 upper" style="margin-left:4px;">

                    <strong> 

                    	RESPONSIBLE BUSINESS<br>

                    	<span class="t_gray">TELEVISION</span>

                    </strong>

              	</div>

                

                <div id="videos-tv" class="videos"></div>

                

            </div>  

            <div class="cell2"></div>              

            <div class="cell3">

                <div class="home-videos-wrap t10 upper">

                	<strong> 

                    	RESPONSIBLE BUSINESS<br>

                    	<span class="t_gray">FORUM</span>

                    </strong>

              	</div>  

                

                <div id="videos-forum" class="videos"></div>

                

            </div>          

        </div>                

    </div>    

    

</div>



<div class="clear"></div>



<div class="line-separator"></div>   

<?php get_footer(); ?>