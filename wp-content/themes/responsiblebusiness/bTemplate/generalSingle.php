<?php
if($hierarchy[0] == '108'): // Community category
 ?>

 <style type="text/css">
  #primary-menu-wrap .nav li:nth-child(4) a{
  color: #000!important;
}
.nav li:nth-child(5) a{
  color: #00bff3!important;
}
 </style>

<?php endif; ?>
<style>

.nav li:nth-child(4) a{ color:#00BFF3 !important;}
.hlt{border-top:none;}
#blog-left{margin-top:0;}
#blog-left .t10{margin-top: 37px;}
#blog-left .t10 strong{font-weight:normal;color:#46a4d5;font-size:16px;}
#blog-left .t24{margin-top: 23px;font-size:22px;line-height:28px;}
#blog-left .t10 p{margin-bottom:30px !important;font-size:13px;color:#959595;line-height:20px;}
.shre_this{font-family:"open_sansbold",Arial,Helvetica,sans-serif;}
.sep_le{margin-top:0;padding-top:32px;min-height: 1356px;}

.sing_ban img {
  height: auto;
  max-height: 341px;
  width: 100%;
}

</style>
<script>
$(function () {

    var counter = 0,
        divs = $('#one,#two');

    function showDiv () {
        divs.fadeOut('slow') // hide all divs
            .filter(function (index) { return index == counter % 2; }) // figure out correct div to show
            .fadeIn('slow'); // and show it

        counter++;
    }; // function to loop through divs and show correct div

    showDiv(); // show first div    

    setInterval(function () {
        showDiv(); // show next div
    }, 10 * 1000); // do this every 10 seconds    

});
</script>
 <?php 
    $id = get_the_id();
$cat = get_the_category();



$new_arr_cat_id = array();
if(count($cat)>1){
$i=1;
foreach($cat as $key => $val){
  if($i!=1){
  $new_arr_cat_id[] = $val->cat_ID;
  }
  $i++;
}
}else{
   foreach($cat as $key => $val){
    $new_arr_cat_id[] = $val->cat_ID;
    }
}

if(in_array(67, $new_arr_cat_id) || $hierarchy[0]=='67'){
  $tit_related = 'Related News';
  $tit_shared_this = 'Share with your network';
}else{
  $tit_related ='Related Posts';
  $tit_shared_this = 'Share this post';
}


$isNewsLetter = false;
if(in_array(110, getCatesIdFromPost())){ //110: cate News letter
 $isNewsLetter = true;
}

  ?>

<div class="top_smnu"></div>

<div class="clear"></div>

<div class="con_lef">

<?php include get_template_directory()."/side_new_menu.php"; ?>


</div>

<div class="con_rig">

<div id="container" class="single-post l_black" style="min-height:100%;">

<div class="hlt blog">

<?php



if ( have_posts() ) :



?>



<div id="blog-left" class="ccontent ">



	<?php $img_data = get_post_thumbnails(get_the_ID()); ?>



    <?php if(count($img_data)>0): ?>

<div class="sing_ban">

    <img src="<?php echo $img_data['image']; ?>"  />

</div>

    <?php endif; ?>

    <br>

    <?php if($isNewsLetter==false): ?>
    <div class="addthis_toolbox addthis_default_style">
      <a class="addthis_button_facebook addthis_button_preferred_1" title="Facebook"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/facebook.png" border="0" alt="Facebook"/></a>
      <a class="addthis_button_twitter addthis_button_preferred_3"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/twitter.png" border="0" alt="Twitter"/></a>
      <div class="wr-linke-share"> <a class="addthis_button_linkedin_counter at300b b_linke_share_btn"></a><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/linkedin.png" border="0" alt="Linkeln"/></div>
      <a class="aaddthis_button_email"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/email.png" border="0" alt="Email"/></a>
      <a class="addthis_button_compact at300m" href="#"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/addthis.png" border="0" alt="Twitter"/></a>
                                           
    </div>



<div class="t10 upper"><strong><?php echo date('j F Y', strtotime($post->post_date));?></strong></div>

<?php endif; ?>



    <div class="t24 t_blue"><strong><?php echo $post->post_title; ?></strong></div>

  <?php if($isNewsLetter==false): ?>
 
     <div class="b_cate_single">
     
      <?php //the_category();                        
                                              $cates = get_the_category();
                                              $count_cates = count($cates);
                                              $i=1;
                                              foreach($cates as $k => $v){
                                                  if($i!=$count_cates){
                                                    $sepa = ', ';
                                                  }else{
                                                    $sepa = '';
                                                  }
                                                
                                                  // echo $v->name.$sepa;
                                                  
                                                $i++;
                                              }
                                             
                                            ?>
     </div>
   <?php endif; ?>



 <?php /*?>   <div class="t11 t_blue"><strong><?php echo get_post_meta($post->ID, 'alternative_title', true); ?></strong></div><?php */?>



    <?php $author = get_userdata($post->post_author); ?>    



    <?php /*?><div class="t_11">By<strong> <?php echo $author->user_firstname. ' ' .$author->user_lastname; ?></strong> <?php echo $author->user_description; ?></div><?php */?>



    <div class="t10">

    <?php 
    if(has_category(array(67,9), $id)){ // category News, feature post
      //if ( !has_shortcode( $post->post_content, 'image_news' ) ) { 
        if( (get_option('image_news_status') == 'Show') && (get_option('image_news_image')) && (get_option('image_news_location') == 'Top')  ){
          echo "<a target='_blank' href='".get_option('image_news_url')."'><img src='".get_option('image_news_image')."'></a>";
        }elseif( (get_option('image_news_status') == 'Show') && (get_option('image_news_image')) && (get_option('image_news_location') == 'Top-Footer') ){
           echo "<a target='_blank' href='".get_option('image_news_url')."'><img src='".get_option('image_news_image')."'></a>";
        }
      // }
    }

    echo do_shortcode($post->post_content); 

    if(has_category(array(67,9), $id)){ // category News, feature post
       // if ( !has_shortcode( $post->post_content, 'image_news' ) ) { 
          if( (get_option('image_news_status') == 'Show') && (get_option('image_news_image')) && (get_option('image_news_location') == 'Footer')  ){
            echo "<a target='_blank' href='".get_option('image_news_url')."'><img src='".get_option('image_news_image')."'></a>";
          }elseif( (get_option('image_news_status') == 'Show') && (get_option('image_news_image')) && (get_option('image_news_location') == 'Top-Footer') ){
           echo "<a target='_blank' href='".get_option('image_news_url')."'><img src='".get_option('image_news_image')."'></a>";
        }
       //}
    }

    ?>

    <?php //echo do_shortcode('[gview file="http://localhost/resbusness/wp-content/uploads/2013/11/RBF_OutcomeStatement1.pdf"]') ?>

    

    </div>
  <?php if($isNewsLetter==false): ?>
    <span class="shre_this"><?php echo $tit_shared_this; ?></span>

    <div class="addthis_toolbox addthis_default_style">
      <a class="addthis_button_facebook addthis_button_preferred_1" title="Facebook"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/facebook.png" border="0" alt="Facebook"/></a>
      <a class="addthis_button_twitter addthis_button_preferred_3"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/twitter.png" border="0" alt="Twitter"/></a>
      <div class="wr-linke-share"> <a class="addthis_button_linkedin_counter at300b b_linke_share_btn"></a><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/linkedin.png" border="0" alt="Linkeln"/></div>
      <a class="aaddthis_button_email"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/email.png" border="0" alt="Email"/></a>
      <a class="addthis_button_compact at300m" href="#"><img src="https://cache.addthiscdn.com/icons/v2/thumbs/32x32/addthis.png" border="0" alt="Twitter"/></a>                                    
    </div>

<?php endif; ?>




<p>&nbsp;</p>

</div>

<?php if($isNewsLetter==false): ?>
<div class="blockCate">


  <h4 class="related-title"><?php echo $tit_related; ?></h4>
  <?php 


  $args = array(
    'category__in'    => $new_arr_cat_id,
    //Type & Status Parameters
    'post_type'   => 'post',
    //Pagination Parameters
    'posts_per_page'    => 2,

    'post__not_in' =>array($id),

    
  );

$query = new WP_Query( $args );
if($query->have_posts()):while($query->have_posts()):$query->the_post();
$post_id = get_the_id();
 $thum_url=wp_get_attachment_url( get_post_thumbnail_id($post_id ) );
 ?>
<div class="bcol4 b_item_related">

    <div class="blog-wrap" style="min-height:415px;">




      <div class="blog-thumb"><a href="<?php echo the_permalink(); ?>"><img src="<?=$thum_url?>" class="b-img-thum-related"></a> </div><!---->

      <div class="blog-text l_black">

        <div class="t8"><?php echo date('j F Y', strtotime(get_the_date()));?> </div>

        <div class="t11"><strong><a href="<?php echo the_permalink(); ?>">

         <?php echo the_title(); ?>                       </a></strong></div>

          <div class="t9">

            <?php

              $c =get_the_excerpt();

              if($c==""){

                $c = get_the_content();;

                $c = strip_tags(get_the_content());               

              }
              
              
              
              if(get_post_meta($post_id, 'featured_description', true)!="")
              {
                
                echo get_post_meta($post_id, 'featured_description', true);
              }
              else
              {

               echo limit_words($c,15);//echo truncate_text($c, 110); 
                            }
                           ?>

          </div>

          <div class="b_cate_home b_cate_news">
           
            <?php //the_category();                        
                                              $cates = get_the_category();
                                              $count_cates = count($cates);
                                              $i=1;
                                              foreach($cates as $k => $v){
                                                  if($i!=$count_cates){
                                                    $sepa = ', ';
                                                  }else{
                                                    $sepa = '';
                                                  }
                                                
                                                    echo $v->name.$sepa;
                                                  
                                                $i++;
                                              }
                                             
                                            ?>

           </div>

          <div class="read-more"> <a href="<?php the_permalink(); ?>" class="readmore_featured">Read More</a></div>

        </div>                   

      </div>

    </div>


<?php endwhile; endif; ?>

  
    
      
</div>
<?php endif; ?>

</div>



<?php /*?><div id="blog-right">



	<?php



    $lastposts = get_latest_post(100);



	?>



    <?php if(count($lastposts)>0): ?>



    <div id="blog-right-wrap">



    	<div class="line-separator"></div>



        <h3 class="section-title">LATEST POSTS</h3>



        <div class="line-separator"></div>



        <div id="content_1" style="margin-bottom:20px; width:100%; ">



            <ul>



                <?php foreach($lastposts as $data): ?>



                <li>



                    <div>



                        <div class="t_blue t8 upper pdate"><?php echo date('j F Y', strtotime($data->post_date));?></div>



                        <a href="<?php echo get_permalink($data->ID); ?>"><?php echo $data->post_excerpt; ?></a>



                    </div>



                </li>



                <?php endforeach; ?>



            </ul>            



        </div>



    </div>



    <?php endif; ?>



</div><?php */?>





<?php



else :



	echo wpautop( 'Sorry, no posts were found' );



endif;



?>

<div class="hlrs">


<div class="sep_le">

<!-- <div class="glsm">

<div style="height:227px;width:270px;overflow:hidden;">
  <?php
//   	$id=5044; 
// $post = get_post($id); 
// $url = wp_get_attachment_url( get_post_thumbnail_id());
?>
<div id="one">
<a href="<?php //echo $post->post_excerpt;?>" target="_blank">
<img src="http://www.responsiblebusiness.com/wp-content/uploads/2015/05/image2.jpg" />
<?php /*?><img src="<?php  echo $url; ?>"   /><?php */?>
</a>
</div>
<div id="two">
<a href="<?php //echo $post->post_excerpt;?>" target="_blank">
<img src="http://www.responsiblebusiness.com/wp-content/uploads/2015/05/image1.jpg" />
<?php /*?><img src="<?php  echo $url; ?>"   /><?php */?></a>
</div>
</div>

</div> -->

 <?php echo b_get_count_down(); ?>

<div class="tweets-feed twt b_twt" id="b_tw_border">



            <h3>Recent Tweets</h3>



            <style>







/*.dummy_div {



  background:#FFF;



  bottom: 11px;



  height: 13px;



  left: 5px;



  position: absolute;



  width: 216px;



}*/



.tweets-feed iframe{width:100% !important}

#main{width:100% !important}

.tweets-feed iframe #header{width:100% !important}

.blockItem{width:100% !important}



.dummy_div {



  background:#FFF;



  bottom: 6px;



  height: 30px;



  left: 5px;



  position: absolute;



  width: 216px;



}







.slides0 ul li {



padding-top:0px !important;

}

.slides0 {

  height: 180px !important;

}





.single_logo {

  float: left;

  height: 150px;

  margin: 10px 0 0;

  text-align: center;

  width: 250px;

}

</style>



            



            <!-- BEGIN: Twitter website widget (http://twitterforweb.com) -->



            



            <div style="" >



              <!--  <div class="dummy_div"></div>-->



            <?php /*?>    <script type="text/javascript">







document.write(unescape("%3Cscript src='http://twitterforweb.com/twitterbox.js?username=@Responsible_Biz&settings=1,0,3,225,350,ffffff,0,ffffff,101010,1,1,336699' type='text/javascript'%3E%3C/script%3E"));</script><?php */?>



<?php /*?><a class="twitter-timeline"  href="https://twitter.com/Responsible_Biz"  data-widget-id="454328311032713216">Tweets by @Responsible_Biz</a>

    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script><?php */?>
    <a class="twitter-timeline" href="https://twitter.com/Responsible_Biz" data-widget-id="454328311032713216" height="500">Tweets by @Responsible_Biz</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>







</div>



            



            <!-- END: Twitter website widget (http://twitterforweb.com) -->



            



            <?php /*?> </div>







  <div class="clear"></div>







  </div><?php */?>



        </div>

 <div class="col4 jntwr" >

                <h3>Join Our Network</h3>

                 <a href="#" data-reveal-id="join_our_network" class="sgnp b_form_popup" data-animation="fade"><img src="<?php echo get_field('src_banner_newsletter',2) ?>"></a>

                <p>Receive our newsletters, exclusive content previews, event invitations and much more<!--Receive our newletters, exclusive content previews, VIP event invitation and much more--></p>

                <?php //dynamic_sidebar( 'newsletter-sidebar' ); ?>

                <a href="#" data-reveal-id="join_our_network" class="sgnp" data-animation="fade">Sign up</a>

                <?php //generate_social_media_menu(); ?>

            </div>



<div class="clear"></div>

<?php /*?> <div class="col3_app appfrm">

<h3>GET RB FORUM APP</h3>


        <a href="http://play.google.com/store/apps/details?id=com.rbforum" target="_blank"><img src="<?php bloginfo('template_directory')?>/images/google.png"   /></a>
        <a href="http:///itunes.apple.com/us/app/rb-forum/id741520067?mt=8" target="_blank"><img src="<?php bloginfo('template_directory')?>/images/app.png" /></a>

    </div><?php */?>

</div> 

</div>

</div>



<?php get_footer(); ?>

</div>

</div>
