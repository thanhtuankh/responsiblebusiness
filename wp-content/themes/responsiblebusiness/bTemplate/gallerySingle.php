
<?php get_header(); ?>
<div class="top_smnu"></div>
<div class="clear"></div>
<div id="container" class="blog">
<div class="con_lef">
<?php include get_template_directory().'/side_new_menu.php'; ?>
</div>
<style>
.wp-pagenavi{margin:auto !important;}
@media only screen and (max-width: 1170px){
.blog-text .t11{font-size: 13px !important;line-height: 16px;}
}
@media only screen and (max-width: 1024px){
 .blog-text .t11{font-size: 12px !important;}
}
@media only screen and (max-width:995px){
 .blog-text .t11{font-size: 11px !important;}
}
@media only screen and (max-width:936px){
	.blog-text .t11{line-height: 14px;}
 .blog-text .t9{font-size:8pt;line-height: 14px;}
}
@media only screen and (max-width:600px){
 .blog .bcol1, .blog .bcol2, .blog .bcol3, .blog .bcol4 {width: 47%;}
}
</style>
<div class="con_rig">


  <?php echo b_get_slide_by_cat(149);//gallery cat slide ?>

     <div id="content" class="home" style="color:#7d7d7d">
Our community and any interested parties can look at exclusive images from our events, panels and discussions through accessing our Gallery. The collection includes behind the scenes images of our reputable speakers and delegates delivering presentations, while you can also view various strategic networking sessions in action.     

    </div>






<!--========================================== Category News Section============================================-->

	<style type="text/css">
.nav li:nth-child(5) a{
	color: #000!important;
}
.nav li:nth-child(6) a{
	color: #00bff3!important;
}
</style>
<div id="content" class="contentCommnunity">

    <div class="line-separator nws_bdr"></div>



    <h3 class="section-title">Gallery</h3>



    <div class="line-separator" style="margin-bottom:3px;"></div>



     <div class="">



            <div class="contentGallery">
        
              <!--===== Loop =========-->
              <?php $cat_id = $hierarchy[0]; 
              
              	$arr_other = array(         		
              		//Category Parameters
              		'cat'              => $cat_id,
   		
              		//Type & Status Parameters
              		'post_type'   => 'post',
              	
              		//Pagination Parameters
              		'posts_per_page'         => 10,
              		);
              
              $query = new WP_Query( $arr_other );
              
    ?>

<?php $title_cur_post = strtolower(str_replace(' ', '', get_the_title())); ?>
 <input type="hidden" class="b_title_cur_post" value="<?php echo $title_cur_post ?>">		
    	
    </script>
 <div class="menuGallery">
                <ul>
                    <?php 
                    $i=1;
                      while($query->have_posts()):$query->the_post();
                      $title = strtolower(str_replace(' ', '', get_the_title()));
                     ?>
                      <li class="<?php echo $title; ?>">
                        <a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a>
                        <i></i>
                      </li>    

                   <?php $i++; endwhile; wp_reset_postdata();?>
                    <div class="clear"></div>
                 </ul>
           </div> <!--menu gallery-->

              


               <?php 
               while(have_posts()):the_post();
             
                 // $first_item = get_post($first_gal_id);

                  $content = get_the_content();
                  $content = apply_filters('the_content',$content);
                  echo $content;
               ?>
                  
                   
                  <?php endwhile; ?>

            </div>
          </div>
                 
 




</div>	

<!--================================= End Category News Section =================================-->



<div class="line-separator" style="margin:20px 0 23px;"></div>






<?php get_footer(); ?>

<style>
.blog .blog-wrap{height:404px;}
.blog .t8{font-size:12px;}
.blog-text{height:130px;position: relative;}
a.readmore_featured{position: absolute;left:0;bottom:-67px;}
@media only screen and (max-width: 479px){
	.blog .bcol1, .blog .bcol2, .blog .bcol3, .blog .bcol4{width:100% !important;}
}
</style>
</div> 
</div> 


