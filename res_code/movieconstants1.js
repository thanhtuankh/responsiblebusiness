/***************************************************************************************************
* Date: 30/08/08
* Author: Eric Brillet (Ekkko Systems)
* Description: Custom logic for this event.
* 
*/

/*******************************************************************
* Array to store the mapping between the episode number and the starting movie number.
* Used when we load an episode into the iFrame, it automatically loads the movie into
* the flash player, as well as the movie title and description.
*/
EPISODE_STARTINGMOVIE = new Array();
EPISODE_STARTINGMOVIE[0] = 0;
EPISODE_STARTINGMOVIE[1] = 4;
EPISODE_STARTINGMOVIE[2] = 8;
EPISODE_STARTINGMOVIE[3] = 12;
EPISODE_STARTINGMOVIE[4] = 16;
EPISODE_STARTINGMOVIE[5] = 20;
EPISODE_STARTINGMOVIE[6] = 24;
EPISODE_STARTINGMOVIE[7] = 29;
EPISODE_STARTINGMOVIE[8] = 33;
EPISODE_STARTINGMOVIE[9] = 37;
EPISODE_STARTINGMOVIE[10] = 41;
EPISODE_STARTINGMOVIE[11] = 45;



/*******************************************************************
* Array to store the movie info.
* Note that this is a 2 dimension array, the first index number is the episode index number
* and the second one is the movie index number.
*/
MOVIE = new Array();

/***************************************
* EPISODE 1 (Index 0) with 3 stories.
***************************************/
MOVIE[0] = {
    title: "Elopak",
    file: "Series2/Elopak.flv", 
	description: " "};

MOVIE[1] = {
    title: "Biogas - Vietnam",
    file: "Series2/BiogasVietnam.flv", 
	description: " "};

MOVIE[2] = {
    title: "Grundfos",
    file: "Series2/Grundfos.flv",
    description: " "
};

MOVIE[3] = {
    title: "Tree Water People",
    file: "Series2/TreeWaterPeople.flv",
    description: " "
};


/***************************************
* EPISODE 2 (Index 0) with 4 stories.
***************************************/

MOVIE[4] = {
    title: "Grupo Bimbo",
    file: "Series2/GrupoBimbo.flv", 
	description: " "};

MOVIE[5] = {
    title: "Creluz",
    file: "Series2/Creluz.flv", 
	description: " "};

MOVIE[6] = {
    title: "Infosys",
    file: "Series2/Infosys.flv",
    description: " "
};
MOVIE[7] = {
    title: "Nuru Light",
    file: "Series2/NuruLight.flv",
    description: " "
};

	
/***************************************
* EPISODE 3 (Index 0) with 4 stories.
***************************************/

MOVIE[8] = {
    title: "Air Liquide",
    file: "Series2/AirLiquide.flv", 
	description: " "};

MOVIE[9] = {
    title: "One Planet",
    file: "Series2/OnePlanet.flv", 
	description: " "};

MOVIE[10] = {
    title: "Tecnosol",
    file: "Series2/Tecnosol.flv", 
	description: " "};

MOVIE[11] = {
    title: "Vale - Brazil",
    file: "Series2/ValeBrazil.flv",
    description: " "
};

/***************************************
* EPISODE 4 (Index 0) with 4 stories.
***************************************/

MOVIE[12] = {
    title: "Tata Chemicals",
    file: "Series2/TataChemicals.flv", 
	description: " "};

MOVIE[13] = {
    title: "Suffolk County Council",
    file: "Series2/SuffolkCountyCouncil.flv", 
	description: " "};

MOVIE[14] = {
    title: "Tech Resources",
    file: "Series2/TechResources.flv",
    description: " "
};
MOVIE[15] = {
    title: "Watershed Project - Srilanka",
    file: "Series2/WatershedProjectSrilanka.flv",
    description: " "
};

	
/***************************************
* EPISODE 5 (Index 0) with 4 stories.
***************************************/

MOVIE[16] = {
    title: "Unilever",
    file: "Series2/Unilever.flv", 
	description: " "};

MOVIE[17] = {
    title: "Rural Energy Foundation",
    file: "Series2/RuralEnergyFoundation.flv", 
	description: " "};

MOVIE[18] = {
    title: "EPIA",
    file: "Series2/EPIA.flv", 
	description: " "};

/***************************************
* EPISODE 6 (Index 0) with 4 stories.
***************************************/
MOVIE[19] = {
    title: "Barefoot College",
    file: "Series2/BarefootCollege.flv", 
	description: " "};

MOVIE[20] = {
    title: "Hitachi",
    file: "Series2/Hitachi.flv", 
	description: " "};

MOVIE[21] = {
    title: "Halting the loss of Europe's Bio Diversity",
    file: "Series2/HaltingthelossofEuropesBioDiversity.flv", 
	description: " "};

MOVIE[22] = {
    title: "Vale Canada",
    file: "Series2/ValeCanada.flv", 
	description: " "};

/***************************************
* EPISODE 7 (Index 0) with 4 stories.
***************************************/
MOVIE[23] = {
    title: "HERproject",
    file: "Series2/HERproject.flv", 
	description: " "};

MOVIE[24] = {
    title: "Fuji Xerox	",
    file: "Series2/FujiXerox.flv", /* Title and File doesn't match */
	description: " "};
	
MOVIE[25] = {
    title: "Wilmar",
    file: "Series2/Wilmar.flv", 
	description: " "};

MOVIE[26] = {
    title: "Ecolabel",
    file: "Series2/Ecolabel.flv", 
	description: " "};

/***************************************
* EPISODE 8 (Index 0) with 4 stories.
***************************************/
MOVIE[27] = {
    title: "D Light",
    file: "Series2/DLight.flv", 
	description: " "
	};

MOVIE[29] = {
    title: "YARA",
    file: "Series2/YARA.flv", 
	description: " "
	};
MOVIE[30] = {
    title: "Green School",
    file: "Series2/GreenSchool.flv", 
	description: " "
	};
MOVIE[31] = {
    title: "Gender Pay Gap",
    file: "Series2/GenderPayGap.flv", 
	description: " "
	};
MOVIE[32] = {
    title: "Isle of Eigg",
    file: "Series2/IsleofEigg.flv", 
	description: " "
	};
MOVIE[33] = {
    title: "Barrick Gold",
    file: "Series2/BarrickGold.flv", 
	description: " "
	};
MOVIE[34] = {
    title: "Biogas -Vietnam",
    file: "Series2/BiogasVietnam.flv", 
	description: " "
	};
MOVIE[35] = {
    title: "Marco Polo programme",
    file: "Series2/MarcoPoloprogramme.flv", 
	description: " "
	};
MOVIE[36] = {
    title: "One Planet",
    file: "Series2/OnePlanet.flv", 
	description: " "
	};
MOVIE[37] = {
    title: "Syngenta",
    file: "Series2/Syngenta.flv", 
	description: " "
	};
MOVIE[38] = {
    title: "EU Food Facility Programme",
    file: "Series2/EUFoodFacilityProgramme.flv", 
	description: " "
	};
MOVIE[39] = {
    title: "SkyLinkInnovators",
    file: "Series2/SkyLinkInnovators.flv", 
	description: " "
	};
MOVIE[40] = {
    title: "HERproject",
    file: "Series2/HERproject.flv", 
	description: " "
	};
MOVIE[41] = {
    title: "Elopak",
    file: "Series2/Elopak.flv", 
	description: " "
	};
MOVIE[42] = {
    title: "Ecolabel",
    file: "Series2/Ecolabel.flv", 
	description: " "
	};
MOVIE[43] = {
    title: "Barefoot College",
    file: "Series2/BarefootCollege.flv", 
	description: " "
	};
MOVIE[44] = {
    title: "Vale Brazil",
    file: "Series2/ValeBrazil.flv", 
	description: " "
	};
MOVIE[45] = {
    title: "Artha Graha",
    file: "Series2/ArthaGraha.flv", 
	description: " "
	};
MOVIE[46] = {
    title: "Enterprise Europe Network",
    file: "Series2/EnterpriseEuropeNetwork.flv", 
	description: " "
	};
MOVIE[47] = {
    title: "Creluz",
    file: "Series2/Creluz.flv", 
	description: " "
	};
MOVIE[48] = {
    title: "Watershed Project - Srilanka",
    file: "Series2/WatershedProjectSrilanka.flv", 
	description: " "
	};