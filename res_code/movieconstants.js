/***************************************************************************************************
* Date: 30/08/08
* Author: Eric Brillet (Ekkko Systems)
* Description: Custom logic for this event.
* 
*/

/*******************************************************************
* Array to store the mapping between the episode number and the starting movie number.
* Used when we load an episode into the iFrame, it automatically loads the movie into
* the flash player, as well as the movie title and description.
*/
EPISODE_STARTINGMOVIE = new Array();
EPISODE_STARTINGMOVIE[0] = 0;
EPISODE_STARTINGMOVIE[1] = 3;
EPISODE_STARTINGMOVIE[2] = 7;
EPISODE_STARTINGMOVIE[3] = 11;
EPISODE_STARTINGMOVIE[4] = 15;
EPISODE_STARTINGMOVIE[5] = 19;
EPISODE_STARTINGMOVIE[6] = 23;
EPISODE_STARTINGMOVIE[7] = 27;
EPISODE_STARTINGMOVIE[8] = 31;
EPISODE_STARTINGMOVIE[9] = 35;
EPISODE_STARTINGMOVIE[10] = 39;
EPISODE_STARTINGMOVIE[11] = 43;
EPISODE_STARTINGMOVIE[12] = 47;


/*******************************************************************
* Array to store the movie info.
* Note that this is a 2 dimension array, the first index number is the episode index number
* and the second one is the movie index number.
*/
MOVIE = new Array();

/***************************************
* EPISODE 1 (Index 0) with 3 stories.
***************************************/
MOVIE[0] = {
	title: "British Telecom", 
	file: "British_Telecom.flv", 
	description: "BT has long recognised that corporate responsibility goes hand in hand with growing a sustainable business model. From the top to the very bottom, the company has actively set out to transform the way it is run &ndash; and as a result has reduced its carbon footprint, saved millions in annual expenditure, and increased worker productivity by 20%. In India, BT, Cisco and the NGO One World South Asia have launched a telephone-based information service that has seen farmers' crop quality and efficiency improve, and their profits increase by up to 150%."};

MOVIE[1] = {
	title: "American Airlines", 
	file: "American_Airlines.flv", 
	description: "American Airlines is one company that is working to become a better corporate citizen in all aspects of its business. From its environmental strategies and supply chain to its charitable work. It has raised millions of dollars for Cystic Fibrosis through its American Airlines Celebrity Ski event, contributing to research that has almost doubled the life expectancy of CF patients."};

MOVIE[2] = {
	title: "BSR - Corporate Citizenship", 
	file: "BSR_Corporate_Citizenship.flv", 
	description: "Turning a business into a better corporate citizen doesn't always go swimmingly, but fortunately there are a number of enterprises that specialise in corporate social responsibility. One such organisation is BSR or Business for Social Responsibility."};

/***************************************
* EPISODE 2 (Index 0) with 4 stories.
***************************************/
MOVIE[3] = {
	title: "Hydro Building Systems", 
	file: "Hydro_Building_Systems.flv", 
	description: "Buildings are often overlooked when people think of the impact on the environment. However, buildings directly account for over 30% of all CO2 emissions in the world. Norwegian company Hydro has come up with a concept for designing sustainable, low-carbon footprint buildings."};

MOVIE[4] = {
	title: "Jamtland Biomass", 
	file: "Jamtland_Biomass.flv", 
	description: "In the face of increasing energy prices, the European Union plans to reduce its use of fossil fuels by 20% before 2020, saving an estimated &euro;60 billion on the European Union's energy bill. One county in Sweden that has switched to a more sustainable source of energy is Jamtland. Today, more than 60% of its heat and power is derived from biomass. The county's aim is to become a 100% non-fossil fuel region by 2015."};

MOVIE[5] = {
	title: "Endessa", 
	file: "Endessa.flv", 
	description: "Consumers in North America throw away 2.5 million plastic bottles every hour - plastic bottles that can be recycled, along with other materials we dispose of daily such as paper, metals and glass. In Brazil, energy company Endessa has come up with an innovative solution to encourage people to recycle - free energy in exchange for garbage! The programme is having remarkable results."};

MOVIE[6] = {
	title: "Novozymes", 
	file: "Novozymes.flv", 
	description: "The textile industry traditionally uses large amounts of water, vast reserves of energy and a wide variety of chemicals to clean and prepare its fabrics, yarn and fibre. An environmental polluter on all counts. Novozymes is a biotech company that is proving to be a knight in shining armour, not just for the textile industry but many other industries as well. The company is leading the way in replacing toxic chemicals with naturally occurring enzymes, whilst still saving the companies money. "};
	
/***************************************
* EPISODE 3 (Index 0) with 4 stories.
***************************************/
MOVIE[7] = {
	title: "Nokia Siemens", 
	file: "Nokia_Siemens_Networks.flv", 
	description: "Information Technology has become the single most important source for the availability and the transfer of knowledge, which are the main drivers for economic and social progress. Often the cost of connecting rural communities to the information superhighway is prohibitive. Nokia Siemens Networks' Village Connection is a unique solution that has been designed to extend the mobile voice and data coverage to rural villages where people often have less than USD 3 per month to spend on communications."};

MOVIE[8] = {
	title: "One Laptop Per Child", 
	file: "One_Laptop_Per_Child.flv", 
	description: "Back in January of 2005, co-founder and director of the MIT Media Laboratory Nicholas Negroponte sent an email to his friend Hector Ruiz, CEO of AMD, sketching out his idea for a laptop for poor children throughout the world. Six hours later, Ruiz had signed up. Within weeks, News Corp. and Google also joined as founding members of the newly formed program - One Laptop per Child."};

MOVIE[9] = {
	title: "Deloitte", 
	file: "Deloitte.flv", 
	description: "Currently there are 500 million children without access to quality education in the world. And it is forecasted that by 2020 there will be a shortage of 50 million teachers. Supported by organisations like Deloitte, Close the Gap and E Learning for Kids are offering a solution for the world's children."};

MOVIE[10] = {
	title: "Telenor", 
	file: "Telenor.flv", 
	description: "In Bangladesh, 76% of the country's population live in villages, with no internet access. In 1996 Telenor formed a joint venture with Grameen Telecom and set up Grameenphone. Grameenphone started connecting rural villages in Bangladesh to the wider communication network via the Village Phone initiative, hoping to alleviate poverty and isolation by providing access to real time information - despite the remote locations of these villages."};
	
/***************************************
* EPISODE 4 (Index 0) with 4 stories.
***************************************/
MOVIE[11] = {
	title: "APRIL", 
	file: "APRIL.flv", 
	description: "In Sumatra, Indonesia, the majority of the population are farmers. Because they earn so little from the land that they cultivate, their children are not able to get a good education. And as a result they get stuck in a cycle of poverty. RGMI and April however, has recognised the fact that education is the single most empowering tool and is hoping to change things."};

MOVIE[12] = {
	title: "India Vision Foundation", 
	file: "India_Vision_Foundation.flv", 
	description: "India is one country that has invested heavily in the education of its people. More Indian children are in school today than ever before. Today, India produces more IT graduates than any other country in the world. However, children at the very bottom of India's social ladder are being left behind. Kiran Bedi, Head of the India Vision Foundation is determined to change that."};

MOVIE[13] = {
	title: "Hydro Brazil", 
	file: "Hydro_Brazil.flv", 
	description: "Soccer is the most popular sport in Brazil and more than 10,000 Brazilians play soccer professionally at home and all over the world. Brazil also has one of the world's largest numbers of child workers and children who do not attend school. One Norwegian company, Hydro Brasil, has come up with a programme to encourage children in Brazil to stay in school, using soccer as an incentive."};

MOVIE[14] = {
	title: "Interros", 
	file: "Interros.flv", 
	description: "The education system in Russia hasn't changed much since the Soviet Union collapsed in 1989. Under the Soviet system, education at all levels was free. In the new Russia, many schools have introduced fees, making education unaffordable for most.Some lucky students however got a helping hand - from The Vladimir Potanin Foundation, the first private charity in Russia."};
	
/***************************************
* EPISODE 5 (Index 0) with 4 stories.
***************************************/
MOVIE[15] = {
	title: "Borealis and Borouge", 
	file: "Borealis_and_Borouge.flv", 
	description: "Everyone from commodity traders to governments are calling water the 'new oil'. As water becomes a rarer commodity, reducing wastage during transport becomes a greater priority. In some countries leakages and poor maintenance of pipes is blamed for the loss of almost 50% of clean drinking water. The problem is most acute in larger established cities where ageing pipe networks are sometimes over a hundred years old. European multi-nationals Borealis and Borouge are offering a solution."};

MOVIE[16] = {
	title: "The Water Facility - Kenya", 
	file: "The_Water_Facility_Kenya.flv", 
	description: "The Water Facility was set by the European Union in 2002, to provide safe drinking water to countries in Africa, the Caribbean and the Pacific regions. The Water Facility does not just donate money or equipment, but ensures that local communities take ownership of the project and also trains them to maintain the equipment and networks - the reason for its success."};

MOVIE[17] = {
	title: "Keppel Seghers", 
	file: "Keppel_Seghers.flv", 
	description: "With the world population increasing by nearly 80 million every year, it has become critical that new sources of water are found to quench the thirst of the growing millions. In the developed world 20% of water people use gets flushed down the toilet. Compare that to the less than 1% used for drinking. What if all the water that gets flushed down the toilet can be recycled for drinking? If Keppel, a Singaporean company has its way, that's exactly what we will all be drinking."};

MOVIE[18] = {
	title: "Nestle - Water in Rwanda", 
	file: "Nestle_Water_in_Rwanda.flv", 
	description: "Having abundant water resources by itself does not solve a nation's water problem. It is equally important to have the necessary infrastructure to transport that water to where it is most needed. With the help of Nestle, some communities in Rwanda are getting exactly that - infrastructure to bring piped drinking water closer to villages and into schools, improving living conditions for entire communities."};

/***************************************
* EPISODE 6 (Index 0) with 4 stories.
***************************************/
MOVIE[19] = {
	title: "Sprint and KCP&L", 
	file: "Sprint_and_KCPL.flv", 
	description: "Wind power is today widely recognised as a major global energy producer. A total of 14 billion dollars was spent on wind energy in 2007 and capacity grew by 24% to 59 Gigawatts. This is equal to 59 large nuclear plants which typically have an annual output of 1GW each. Sprint and KCP&L demonstrate the business case for adopting green power."};

MOVIE[20] = {
	title: "HER Women's Health Program", 
	file: "BSR_HER_Womens_Health_Program.flv", 
	description: "Business for Social Responsibility or BSR is one organisation that is at the forefront of encouraging different stakeholders in the business world to work together to address common issues. One such BSR initiative is looking at improving women's health among factory workers; an initiative that is benefiting not just the women but also their employers."};

MOVIE[21] = {
	title: "HP - Managing the Responsible Supply Chain", 
	file: "HP_Managing_the_Responsible_Supply_Chain.flv", 
	description: "Managing a responsible supply chain is often a challenge to many companies. The problem is even more severe when you are a large multinational using many contract manufacturers that are spread all over the world. HP is one such company and it is an industry leader in establishing practices and partnerships to manage a responsible supply chain."};

MOVIE[22] = {
	title: "The Esquel Group", 
	file: "The_Esquel_Group.flv", 
	description: "Set up as a family owned business in 1978, The Esquel Group is today the largest exporter of high end shirts in China. The company is vertically integrated through the entire manufacturing process. Esquel grows the cotton, weaves the fabric and designs and produces the shirts. As a result Esquel has built partnerships with its different stakeholders to address issues ranging from land usage and improving farmer incomes to saving water and energy."};

/***************************************
* EPISODE 7 (Index 0) with 4 stories.
***************************************/
MOVIE[23] = {
	title: "Sveaskog", 
	file: "Sveaskog.flv", 
	description: "International demand-driven intensive logging and ranching have proved an unstoppable force for deforestation and loss of habitats. In the next 24 hours, deforestation will release as much CO2 into the atmosphere as 8 million people flying from London to New York; conservation has been no match for commerce. Sveaskog is one Swedish company that is leading the way in sustainable forestry practices."};

MOVIE[24] = {
	title: "Marine Stewardship Council", 
	file: "BSR-HER_Project.flv", /* Title and File doesn't match */
	description: "The modern fishing industry is dominated by vessels that far out-match nature's ability to replenish fish stocks. Sonar can pinpoint schools of fish quickly and accurately. The ships are fitted out like giant floating factories with processing and packing plants, huge freezers, and powerful engines to drag enormous fishing gear through the ocean. Put simply: the fish don't stand a chance! The Marine Stewardship Council is at the forefront of the battle to protect global fish stocks."};
	
MOVIE[25] = {
	title: "Vranken Pommery", 
	file: "Vranken_Pommery.flv", 
	description: "Wine making is one of the oldest industries in the world. Did you know that the ancient Mesopotamians were the first people to cultivate grapes and enjoy wine? The way in which the wine is made has not changed much in 8000 years. What HAS changed is the way in which the wines are grown. One French company, Vranken Pommery, is now turning its back on modern chemicals and pesticides and looking back to nature, to manage the most important resource to the wine-maker; the land. "};

MOVIE[26] = {
	title: "Intel", 
	file: "Intel.flv", 
	description: "Industries have a significant impact on the natural resources and eco-systems in the environment around where they are based. In the past this impact has often been negative. But more and more companies such Intel, are realising that protecting the natural resources around them is not just the right thing to do, but that it also makes good business sense."};

/***************************************
* EPISODE 8 (Index 0) with 4 stories.
***************************************/
MOVIE[27] = {
	title: "Voestalpine", 
	file: "Voestalpine.flv", 
	description: "Although the amount of energy needed to produce a ton of steel has been reduced by a third since 1972, the steel industry is today the single largest energy-consuming industry in the world. It accounts for almost 4% of global greenhouse gas emissions. The biggest challenge for the steel industry has been to come up with new and innovative technologies that help reduce climate emissions and Austrian steelmaker Voestalpine have established themselves as a world leader in developing and using advanced technologies to reduce emissions in the steel industry."};

MOVIE[28] = {
	title: "European Carbon Exchange", 
	file: "Carbon_Exchange.flv", 
	description: "When Europe signed the Kyoto protocol, it pledged to cut green house gases by 8% from its 1990 levels, by 2012. As one way of reaching this goal, at the beginning of 2005, Europe launched the first international trading scheme for CO2 emission quotas - a carbon exchange."};
	
MOVIE[29] = {
	title: "Advantage West Midlands", 
	file: "Advantage_West_Midlands.flv", 
	description: "During the 18th century the Industrial Revolution spread throughout Britain. The use of steam-powered machines, led to a massive increase in the number of factories. As the number of factories grew so did the pollution. Guided by the regions' development agency Advantage West Midlands, the birth place of this high carbon economy is today spearheading a different kind of revolution."};

MOVIE[30] = {
	title: "Navarre", 
	file: "Navarre.flv", 
	description: "When it comes to wind energy, Spain is one of the world leaders. On very windy days, wind energy production can supply as much as 40 percent of Spain's electricity demand, although the yearly average is around 10 percent. Navarre is one region in Northern Spain that is leading the way in renewable energy generation and hoping that all of the region's energy would soon come from renewable sources."};

/***************************************
* EPISODE 9 (Index 0) with 4 stories.
***************************************/
MOVIE[31] = {
	title: "Covanta Energy", 
	file: "Covanta_Energy.flv", 
	description: "Did you know that each tone solid waste or garbage that we throw out has the same energy value of about one barrel of oil? An increasing number of communities across the world are today looking to energy-from-waste as a viable alternative to fossil fuels, while at the same time tackling the growing problem of landfill and the world leader in this waste to energy revolution is American energy producer Covanta Energy."};

MOVIE[32] = {
	title: "Island of Fohr", 
	file: "Fohr.flv", 
	description: "When you live on a small island that is two thirds below sea level, reducing climate emissions and trying to do your part to slow the rising tide becomes very important. For residents of a small German island called Fohr, that is just over 7 miles long and 4 miles wide, renewable energy is proving to be an environmentally safe solution while at the same time making good financial sense."};
	
MOVIE[33] = {
	title: "HP - Reducing Climate Emissions", 
	file: "HP.flv", 
	description: "Videoconferencing has been around for many years now. However due to slow data transfer speeds, low video quality and the inability to do multiple functions at the same time, such as share presentations, businesses did not see videoconferencing as a replacement for face-to-face meetings. Hewlett Packard's Halo Telepresence is a revolutionary teleconferencing solution that is helping companies all over the world reduce their climate footprint."};

MOVIE[34] = {
	title: "Rezidor Hotel Group", 
	file: "Rezidor_ Hotel_ Group.flv", 
	description: "The global hotel industry uses large amounts of energy and plenty of other natural resources in the day-to-day running of its properties. 'Going Green' is more than a mere fad for a lot of the players in the industry. With energy accounting for between 3 and 6% of running costs, it also makes better business sense. The Rezidor Hotel Group is an industry leader in sustainable practices and is adopting measures that are suited to each individual property."};
	
	
/***************************************
* EPISODE 10 (Index 0) with 4 stories.
***************************************/
MOVIE[35] = {
	title: "Syngenta",
	file: "Syngenta.flv", 
	description: "India was the first country in the world to produce sugar crystals almost 1700 years ago during the Gupta Dynasty. Until recently though, India's primary sugar crop was sugar cane. A new initiative by Syngenta is now encouraging farmers to switch to sugar beet, a far less restrictive crop than the cane and one that is not only helping to improve the living standards of the farmers but is also proving to be beneficial for the environment."};
	
MOVIE[36] = {
	title: "YCI",
	file: "YCI.flv", 
	description: "Almost 10% of Brazil's urban population live below the UN's absolute poverty line. And as a result it's estimated that up to 8 million young people live on the streets in this, the largest country in South America. Most of the time no one takes much notice of these children who are most commonly at risk in their teens, often resorting to a life of crime and violence. Some of these children are now getting a second chance thanks to the Youth Career Initiative."};
	
MOVIE[37] = {
	title: "IPNI",
	file: "IPNI.flv", 
	description: "For many farmers achieving even a slightly higher crop yield could mean the difference between a better standard of life and one that is caught up in a cycle of poverty. One key ingredient in the formula for increasing crop yields is the use of fertilizer. According the International Plant Nutrition Institute, if used responsibly, fertilizer could significantly increase harvests while having very little impact on the environment."};

MOVIE[38] = {
	title: "Nestle India",
	file: "Nestle_India.flv", 
	description: "Today India is the world leader in milk production and produces nearly 50% more milk than its nearest rival - the United States. However, the average dairy herd in India consists of just 2 animals and yields are low. Compare that to the USA - here an average herd consists of 88 dairy cows and each cow produces 10 times the yield. But thanks to the efforts of companies like Nestle, the Indian dairy farm is changing."};

/***************************************
* EPISODE 11 (Index 0) with 4 stories.
***************************************/
MOVIE[39] = {
	title: "Starbucks",
	file: "Starbucks.flv", 
	description: "Coffee is one of the most widely grown cash crops in the world. It is planted in over 60 countries and there are an estimated 25 million coffee farmers worldwide. On the opposite end of this supply chain are the retailers - familiar names to most of us. For many of these retailers such as the Starbucks Coffee Company, who are a world away from the plantations where the coffee is grown, it has become a challenge to ensure that farmers get a fair price for their produce and the coffee is grown sustainably."};
	
MOVIE[40] = {
	title: "Coloplast",
	file: "Coloplast.flv", 
	description: "Unlike most other sectors, the pharmaceutical industry has to follow stricter regulations on how they select suppliers and manage a responsible supply chain. Because of the risk to patients, even the simple act of replacing an ingredient used in the packaging of pharmaceutical products could be a long and difficult process. Coloplast, a Danish pharmaceutical company is now replacing some of its packaging with advanced bio-degradable plastics."};
	
MOVIE[41] = {
	title: "Fairtrade",
	file: "FairTrade.flv", 
	description: "The concept of 'fair trade' has been around for over 40 years but a formal labeling scheme wasn't launched until the late 1980s. Since those early days the Fairtrade movement has grown exponentially. Over $3 billion was spent on Fairtrade certified products in 2007. This is a 47% increase on the previous year and it directly benefitted over 7 million people - farmers, workers and their families in 58 developing countries."};

MOVIE[42] = {
	title: "BSR - Supply Chain Management",
	file: "BSR_Supply_Chain_Management_v3.flv", 
	description: "In today's cost conscious marketplace companies such as Levi Strauss and Gap, often outsource their production to a number of independent suppliers from across the world. Similarly a single contract manufacturer could supply to a large number of customers at the same time. If this complex supply chain is managed individually, it can be very complex and time consuming to both the buyers and the suppliers. One solution that is has been promoted by organizations such as BSR, is to work collectively - saving everyone money and time."};
	
/***************************************
* EPISODE 12 (Index 0) with 4 stories.
***************************************/
MOVIE[43] = {
	title: "Nexen",
	file: "Nexen.flv", 
	description: "While lowering emissions, companies also use offsetting schemes to further reduce their carbon footprint. Often, these carbons offset projects take the form of tree planting initiatives or investments in renewable energy generation. Canadian oil and gas company, Nexen's support of the Rio Bravo Carbon Sequestration Project in Belize, is unique because it not only helps offset carbon emissions, but also supports sustainable development and helps protect many endangered species of plants and animals."};
	
MOVIE[44] = {
	title: "Sweden - State Boost to Renewable Energy",
	file: "Sweden.flv", 
	description: "When it comes to finding sustainable energy solutions, Sweden is one of the world leaders. This country of 9 million has an ambitious target of weaning itself off oil completely within 15 years. With a government that is not shying away from energy issues, Sweden seems to be well on its way to achieving this goal."};
	
MOVIE[45] = {
	title: "Scania",
	file: "Scania_New.flv", 
	description: "A large portion of the energy we use goes to transporting people and goods from one place to another. For example in the United States there are almost 250 million vehicles on the road and they use about 28% of all the energy used within the country. While actively developing and promoting hybrid vehicles as one way of reducing fuel consumption, Scania, the world's third largest heavy truck and bus manufacturer is also pursuing other more simpler ways of achieving the same goal."};

MOVIE[46] = {
	title: "NSN Flexi Base Stations",
	file: "Nokia_121108.flv", 
	description: "Currently there are almost 4 billion mobile phone users in the world and the number is growing. As penetration saturates in developed countries and urban areas, mobile phone networks are looking to expand beyond these regions. One of the key challenges the operators have to overcome when doing this is how to power these base stations that are often in areas not connected to national power grids. Nokia Siemens Networks Flexi Base Stations offer a solution the problem, while at the same time helping network operators reduce costs in the long run."};
	
/***************************************
* EPISODE 13 (Index 0) with 3 stories.
***************************************/
MOVIE[47] = {
	title: "Pfizer Inc",
	file: "Pfizer.flv", 
	description: "According to the World Health Organization, the difference in life expectancy between the richest and the poorest countries now exceeds 40 years. This is not only due to the unaffordable cost of medicines, but often it's because developing countries lack the basic health infrastructure and trained staff. Pfizer is one company that has recognized this fact and is committing more than just money."};
	
MOVIE[48] = {
	title: "Impact",
	file: "Impact.flv", 
	description: "Natural disasters occur every year around the world, and in their wake communities, and sometimes entire countries are left struggling to re-build themselves. Helping those affected requires the combined efforts of governments, NGO's, corporations and individuals. The relief work, while making a life changing difference to the victims, also leaves a lasting impact on the volunteers. Impact, a company that is based in northern England, is seeing disaster relief as an important part of the development of its work-force."};
	
MOVIE[49] = {
	title: "Petronas",
	file: "Petronas.flv", 
	description: "In 1975, when Malaysian oil and gas company Petronas set up its scholarship programme, the aim was to develop the skills it needed to run its business.  What started out as a means to benefit the company, soon developed into a much larger scholarship programme that benefitted not only the wider Malaysian population, but also students from countries all over the world."};


	
