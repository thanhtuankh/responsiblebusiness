﻿/***************************************************************************************************

* Date: 30/08/08

* Author: Eric Brillet (Ekkko Systems)

* Description: Custom logic for this event.

* 

*/



/*******************************************************************

* Array to store the mapping between the episode number and the starting movie number.

* Used when we load an episode into the iFrame, it automatically loads the movie into

* the flash player, as well as the movie title and description.

*/

EPISODE_STARTINGMOVIE = new Array();

EPISODE_STARTINGMOVIE[0] = 0;

EPISODE_STARTINGMOVIE[1] = 1; 

EPISODE_STARTINGMOVIE[2] = 5;

EPISODE_STARTINGMOVIE[3] = 9;

EPISODE_STARTINGMOVIE[4] = 13;

EPISODE_STARTINGMOVIE[5] = 17;

EPISODE_STARTINGMOVIE[6] = 21;

EPISODE_STARTINGMOVIE[7] = 24;

EPISODE_STARTINGMOVIE[8] = 28;

EPISODE_STARTINGMOVIE[9] = 32;

EPISODE_STARTINGMOVIE[10] = 36;

EPISODE_STARTINGMOVIE[11] = 40;

/*******************************************************************

* Array to store the movie info.

* Note that this is a 2 dimension array, the first index number is the episode index number

* and the second one is the movie index number.

*/

MOVIE = new Array();



/***************************************

* EPISODE 1 (Index 0) with 4 stories.

***************************************/

MOVIE[0] = {

    title: "Citi Developments Limited",

    file: "series3/City_Developments_Limited.flv", 

	description: " "};



MOVIE[1] = {

    title: "Using waste as a resource",

    file: "series3/Using_waste_as_a_resource1.flv", 

	description: " "};



MOVIE[2] = {

    title: "The Mall Group",

    file: "series3/The_Mall_Group.flv",

    description: " "

};



MOVIE[3] = {

    title: "Knauf Insulation",

    file: "series3/Knauf.flv",

    description: " "

};







/***************************************

* EPISODE 2 (Index 0) with 4 stories.

***************************************/



MOVIE[4] = {

    title: 'Sonae Indústria',

    file: "series3/Sonae_Industria.flv", 

	description: " "};

	

MOVIE[5] = {

    title: 'Saving trees and warming homes',

    file: "series3/Saving_trees_and_warming_homes.flv", 

	description: " "};

	

MOVIE[6] = {

    title: 'Billerud AB',

    file: "series3/Billerud.flv", 

	description: " "};

	

MOVIE[7] = {

    title: 'Mitsui & Co., LTD.',

    file: "series3/Mitsui.flv", 

	description: " "};

	

/***************************************

* EPISODE 3 (Index 0) with 4 stories.

***************************************/



MOVIE[8] = {

    title: 'Connecting Europe at High Speed',

    file: "series3/Connecting_Europe_at_High_Speed.flv", 

	description: " "};

	

MOVIE[9] = {

    title: 'KLM',

    file: "series3/KLM.flv", 

	description: " "};

	

MOVIE[10] = {

    title: 'Maritime and Port Authority of Singapore',

    file: "series3/MPA.flv", 

	description: " "};

	

MOVIE[11] = {

    title: 'Tristar',

    file: "series3/TRISTAR.flv", 

	description: " "};	

	

/***************************************

* EPISODE 4 (Index 0) with 4 stories.

***************************************/



MOVIE[12] = {

    title: 'Afghan Health Service',

    file: "series3/AfghanHealthService.flv", 

	description: " "};

	

MOVIE[13] = {

    title: 'Bernard van Leer Foundation',

    file: "series3/BernardvanLeerFoundation.flv", 

	description: " "};

	

MOVIE[14] = {

    title: 'Shire',

    file: "series3/Shire.flv", 

	description: " "};

	

MOVIE[15] = {

    title: 'Türk Telekom',

    file: "series3/TurkTelekom.flv", 

	description: " "};	
	
	

/***************************************

* EPISODE 5 (Index 0) with 4 stories.

***************************************/



MOVIE[16] = {

    title: 'Concophilips',

    file: "series3/Ep 5 Concophilips.flv", 

	description: " "};

	

MOVIE[17] = {

    title: 'KIA',

    file: "series3/Ep 5 KIA.flv", 

	description: " "};

	

MOVIE[18] = {

    title: 'Mohawk',

    file: "series3/Ep 5 Mohawk.flv", 

	description: " "};

	

MOVIE[19] = {

    title: 'Tough Stuff',

    file: "series3/Ep 5 Tough Stuff.flv", 

	description: " "};		
	
/***************************************

* EPISODE 6 (Index 0) with 3 stories.

***************************************/



MOVIE[20] = {

    title: 'IHG',

    file: "series3/Ep 6 IHG.flv", 

	description: " "};

	

MOVIE[21] = {

    title: 'Sime Darby',

    file: "series3/EP 6 SimeDarby.flv", 

	description: " "};

	

MOVIE[22] = {

    title: 'Staedtler',

    file: "series3/Ep 6 Staedtler.flv", 

	description: " "};
	
/***************************************

* EPISODE 7 (Index 0) with 4 stories.

***************************************/



MOVIE[23] = {

    title: 'Bayer',

    file: "series3/Ep 07 Bayer.flv", 

	description: " "};

	

MOVIE[24] = {

    title: 'Novartis',

    file: "series3/Ep 07 Novartis.flv", 

	description: " "};

	

MOVIE[25] = {

    title: 'Unilever',

    file: "series3/Ep 07 Unilever.flv", 

	description: " "};
	
MOVIE[26] = {

    title: 'Verizon',

    file: "series3/Ep 07 Verizon.flv", 

	description: " "};
	
	
	
/***************************************

* EPISODE 8 (Index 0) with 4 stories.

***************************************/

MOVIE[27] = {

    title: 'Jindal Steel & Power',

    file: "series3/Ep 08 Jindal Steel n Power.flv", 

	description: " "};

	

MOVIE[28] = {

    title: 'Friesland Campina',

    file: "series3/Ep 08 FrieslandCampina.flv", 

	description: " "};

	

MOVIE[29] = {

    title: 'April',

    file: "series3/Ep 08 April.flv", 

	description: " "};
	
MOVIE[30] = {

    title: 'Etex',

    file: "series3/Ep 08 - Etex.flv", 

	description: " "};
	
/***************************************

* EPISODE 9 (Index 0) with 4 stories.

***************************************/

MOVIE[31] = {

    title: 'KOSEP',

    file: "series3/Ep 09 KOSEP.flv", 

	description: " "};

	

MOVIE[32] = {

    title: 'Olam',

    file: "series3/Ep 09 Olam.flv", 

	description: " "};

	

MOVIE[33] = {

    title: 'Renault Nissan',

    file: "series3/Ep 09 Renault Nissan.flv", 

	description: " "};
	
MOVIE[34] = {

    title: 'Barrick Gold',

    file: "series3/Ep 09 Barrick Gold.flv", 

	description: " "};
	
	
/***************************************

* EPISODE 10 (Index 0) with 4 stories.

***************************************/

MOVIE[35] = {

    title: 'Airbus',

    file: "series3/Ep 10 Airbus.flv", 

	description: " "};

	

MOVIE[36] = {

    title: 'Harman',

    file: "series3/Ep 10 Harman.flv", 

	description: " "};

	

MOVIE[37] = {

    title: 'Rezidor',

    file: "series3/Ep 10 Rezidor.flv", 

	description: " "};
	
MOVIE[38] = {

    title: 'TSMC',

    file: "series3/Ep 10 TSMC.flv", 

	description: " "};


/***************************************

* EPISODE 10 (Index 0) with 4 stories.

***************************************/

MOVIE[39] = {

    title: 'Monsanto',

    file: "series3/Ep 11 Monsanto.flv", 

	description: " "};

	

MOVIE[40] = {

    title: 'Ferrero',

    file: "series3/Ep 11 Ferrero.flv", 

	description: " "};

	

MOVIE[41] = {

    title: 'Sabic',

    file: "series3/Ep 11 sabic.flv", 

	description: " "};
	
MOVIE[42] = {

    title: 'Zenith Bank',

    file: "series3/Ep 11 Zenith Bank.flv", 

	description: " "};
