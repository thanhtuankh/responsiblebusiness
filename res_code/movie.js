/***************************************************************************************************
* Date: 30/08/08
* Author: Eric Brillet (Ekkko Systems)
* Description: Custom logic for this event.
* 
*/


/***************************************************************************************************
* Public function.
* Loads the given episode html snippet to the iframe.
*/
function loadEpisode(rSeriesNum, rEpisodeNum) {
	
	// Write HTML snippet to iFrame
	//var myEpisodeURL = "/e1/view/events_current/gi/2008/rbf/pg_htm/res_snippet/sr"+rSeriesNum+"ep"+rEpisodeNum+".html";
	var myEpisodeURL = "res_snippet/sr"+rSeriesNum+"ep"+rEpisodeNum+".html";
	writeToIFrame("Series"+rSeriesNum+"EpisodeSection", myEpisodeURL);
	
	// Load a movie to the Div and description/title to the span.
	// The starting movie number is based n the episode number mapped in array EPISODE_STARTINGMOVIE.
	//loadMovie(EPISODE_STARTINGMOVIE[rEpisodeNum]);
}


/***************************************************************************************************
* Private function.
* Loads the derived movie file into the movie player inside a div.
*/
function loadMovieFile(rMovieNum) {
	
	//alert('loadMovieFile(): hello');

	// Check to make sure given episode number is within the episode array size.
	if (rMovieNum > MOVIE.length) {
		alert('Sorry, problem loading movie, movie number ('+
				rMovieNum+') is greater than movie array ('+MOVIE.length+').');
		return;
	}

	// Check to make sure given movie number is within the movie array size.
	/*if (rMovieNum > MOVIE[rEpisodeNum-1].length) {
		alert('Sorry, problem loading movie, movie number ('+
				rMovieNum+') is greater than movie array ('+MOVIE[rEpisodeNum-1].length+').');
		return;
	}*/

	// ORIGINAL CONFIG
	/*var myVideoFile = 
		"{videoFile: 'res_video/"+MOVIE[rMovieNum-1].file+
		"', hideControls: false, autoPlay: false, loop: false, menuItems:[0,0,0,0,0,0]}";*/
	
	//v3
		var myVideoFile = 
		"{videoFile: 'res_video/"+MOVIE[rMovieNum-1].file + "'," +
		"autoPlay: false," + 
		"autoBuffering: true," +
		"loop: false, " +
		"controlBarGloss: 'low'," +
		"initialScale: 'scale', " +
		"showMenu: false, " +
		"showLoopButton: false, " +
		"showPlayListButtons: false, " +
		"showFullScreenButton: false }";
		
	//v2
	/*  var myVideoFile = 
		"{videoFile: 'res_video/"+MOVIE[rMovieNum-1].file+
		"', hideControls: false, autoPlay: false, showMenu: false, showLoopButton: false, " +
		"showPlayListButtons: false, showFullScreenButton: false, loop: false, usePlayOverlay: true, " +
		"showVolumeSlider: true, controlBarBackgroundColor: 0x808080, " +
		"controlBarGloss: 'low', autoBuffering: true, menuItems:[0,0,0,0,0,0]}"; */
		
	//v1	
	/*  v1 var myVideoFile = 
		"{videoFile: 'res_video/"+MOVIE[rMovieNum-1].file+
		"', hideControls: false, autoPlay: false, showMenu: false, showLoopButton: false, " +
		"showPlayListButtons: false, showFullScreenButton: false, loop: false, menuItems:[0,0,0,0,0,0]}"; */
		
		
		
                
         
				
	var so = new SWFObject("FlowPlayerLight.swf", "FlowPlayer", "400", "247", "7");
	so.addParam("scale", "noScale");
	so.addParam("wmode", "transparent");
	so.addVariable("config", myVideoFile);
	so.write("flashcontent");
	//alert('loadMovieFile(): Done!');
}


/***************************************************************************************************
* Public function.
* Loads the given movie into the movie player, updates the movie title and description at the
* same time.
*/
function loadMovie(rMovieNum) {
	
	//alert('loadMovieSection(): Loading movie ' + rMovieNum + '...');
	
	// OPTION 1: IFRAME RELOAD
	//writeToIFrame('MovieFrame', 'movie' + rMovieIndex + '.html');
	//alert('Done.');

	// OPTION 2: SPAN REWRITE
	// Retrieve Span object for the movie object.
	/*myMoviePlayerObject = parent.document.getElementById('MoviePlayer');
	if (!isObjectValid("loadMovieEpisode()", myMoviePlayerObject, "")) {
		alert('Problem retrieving Movie Player span.');
		return;
	}

	// Write the new movie to the movie object.
	myMoviePlayerObject.innerHTML =
		"<object " + 
		"id='FlowPlayer'  " + 
        "width='400'  " + 
		"height='247'  " + 
        "_base_href='http://www.ekkko.com/e1/view/events_current/gi/2008/rbf/pg_htm/'  " + 
        "data='FlowPlayer.swf'  " + 
        "type='application/x-shockwave-flash'> " + 
			"<param value='always' name='allowScriptAccess'/> " + 
			"<param value='FlowPlayer.swf' name='movie'/> " + 
			"<param value='high' name='quality'/> " + 
			"<param value='showAll' name='scaleMode'/> " + 
			"<param value='true' name='allowfullscreen'/> " + 
			"<param value='transparent' name='wmode'/> " + 
			"<param value='all' name='allowNetworking'/> " + 
			"<param value='config={  " + 
                "autoPlay: false,  " + 
                "loop: false,  " + 
                "initialScale: 'scale',  " + 
                "showMenu: false,  " + 
                "showLoopButton: false,  " + 
                "showPlayListButtons: false,  " + 
                "showFullScreenButton: false,  " + 
                "playList: [ { url: 'endessa_v192_a80s_400x225.flv' }]  " + 
                "}' name='flashvars'/> " + 
		"</object>";*/

	// OPTION 3: SWFOBJECT
	/*
	myVideoFile = 
	"{videoFile: '"+MOVIE_NAME[rMovieIndex-1]+"', hideControls: true, loop: false, menuItems:[0,0,0,0,0,0]}"
	alert('1.');

	var so = new SWFObject("FlowPlayer.swf", "FlowPlayer", "320", "240", "7");
	alert('2.');
	so.addParam("scale", "noScale");
	alert('3.');
	so.addParam("wmode", "transparent");
	so.addVariable("config", myVideoFile);
	so.write("flashcontent");
	alert('Done.');
	*/
	
	// OPTION 4: SWFOBJECT - NO IFRAME
	loadMovieFile(rMovieNum);
	
	// Retrieve span object for movie title.
	myMovieTitleObject = parent.document.getElementById('MovieTitle');
	if (!isObjectValid("loadMovieEpisode()", myMovieTitleObject, "")) {
		alert('Problem retrieving Movie Title span.');
		return;
	}
	myMovieTitleObject.innerHTML = MOVIE[rMovieNum-1].title;
	
	// Retrieve span object for movie description.
	myMovieDescrObject = parent.document.getElementById('MovieDescr');
	if (!isObjectValid("loadMovieEpisode()", myMovieDescrObject, "")) {
		alert('Problem retrieving Movie Description span.');
		return;
	}
	myMovieDescrObject.innerHTML = MOVIE[rMovieNum-1].description;
	window.scrollTo(0,0); //Scrolls to Top of page :D
}

