/***************************************************************************************************
* Date: 30/08/08
* Author: Eric Brillet (Ekkko Systems)
* Description: New logic for Web 2.0 interfaces
* 
*/

/***************************************************************************************************
* Public function.
* Writes a given comment to a given span.
*/
function writeStaticComment(rSpanName, rComment) {
	var mySpanObj = document.getElementById(rSpanName);
	if (!isObjectValid("writeStaticComment()", mySpanObj, ""))
		return;
	if (mySpanObj.innerHTML == "")
		mySpanObj.innerHTML = rComment;
	else
		mySpanObj.innerHTML = "";
}

/***************************************************************************************************
* Public function.
* Hides the comments in a given span.
*/
/*function hideStaticComment(rSpanName) {
	var mySpanObj = document.getElementById(rSpanName);
	if (!isObjectValid("hideStaticComment()", mySpanObj, ""))
		return;
	mySpanObj.innerHTML = "";
}*/

/***************************************************************************************************
* Hides the comments in a given span.
*/
function writeToIFrame(rFrameName, rHtmlFilename) {
	
	var myIFrameObj = parent.document.getElementById(rFrameName);
	if (!isObjectValid("writeToIFrame()", myIFrameObj, ""))
		return;
		
	myIFrameObj.contentWindow.document.location.href = rHtmlFilename;
}
