/***************************************************************************************************
* Date: 29/11/06
* Author: Eric Brillet (Codalworks)
* Description: Event processing logic for registration page
* This is a library of the methods called by the customised eventhandler javascript file "lib_eventhandler_custom.js"
* which is located in the event's res_code folder.
* More info can be found in the sample file "lib_eventhandler_custom.js".
* 
* MISC EXPERIMENTAL CODE:
* =======================
* // obj = document.getElementById('HotelHiddenFields');
* // obj.style.visibility = "visible";
*
* METHODS:
* ========
* Private Methods:
* ----------------
* getFormObj();
* getInputFieldObj();
* writeHiddenFields();
* disableInputField();
* enableInputField();
* resetInputField();
* disableInputFieldsArray();
* enableInputFieldsArray();
* resetInputFieldsArray();
* resetInputFields();
* 
* Public Methods:
* ---------------
* clearComments();
* writeComments();
* enableInputFields();
* disableInputFields();
*
* INSTRUCTIONS:
* =============
* This javascript library file must be referenced in the HEAD tag of the jsp page.
* Refer to the file "lib_eventhandler_custom.js" in this same folder as an example.  
* This file must be put in the res_code folder of the event.
* Then the JSP or HTM pages must link to this file, the parent "lib_eventhandler.js" and its corresponding 
* customised file "lib_eventhandler_custom.js".
*
* Instructions for editting the calling HTML file can be found in "lib_eventhandler_custom.js".
* 
*/

/***************************************************************************************************
* CONSTANTS
*/
DEBUG1 = false;
DEBUG2 = false;
TESTON = false;

REGFORMNAME = "RegForm";

WINDOWDELIM = 	
"\r\n------------------------------------------------------------" + 
"------------------------------------------------------------\r\n";

/*
* These icons are relative to /e1/hello/res_pics/
*/
DISPLAYICON_NONE =       "";
DISPLAYICON_WARNING =    "warning.gif";
DISPLAYICON_ARROWDOWN =  "arrowskip.gif";
DISPLAYICON_PENCIL =     "pencil.gif";


/***************************************************************************************************
* EXPERIMENTAL FUNCTION
*/
function processFormEvent(rFormObj) {
	
	alert('start');
	var targ;
	var e = window.event;
	alert(e);
	
	if (e.target) {
		targ = e.target;
		alert(targ);
	}
	else if (e.srcElement) {
		targ = e.srcElement;
		alert(targ);
	}
	
	if (targ.nodeType == 3) // defeat Safari bug
		targ = targ.parentNode;
		
	window.status = targ.name;
}


/***************************************************************************************************
* Original checkForm function now replace with below, which allows us to switch on or 
* off the checking of fields from <FORM> tag with <FORM onSubmit="checkForm(true);">
*/
function checkForm() {
	return checkForm(false);	
}


/***************************************************************************************************
* Public function.
* Checks all the input fields inside a form, and returns an error message if any of the hidden
* fields are duplicated.
*/
function checkForm(rDoCheck) {
	
	/*if (!TESTON) {
		return;
	}*/
	if (!rDoCheck) {
		return;
	}
	var myFormObj = getFormObj(REGFORMNAME);
	
	// Loop through element in the form
	// Retain those that occur more than once which are hidden
	var myElementName;
	var myCounter = 0;
	var myHiddenElementsArray = new Array();
	var isHidden = false;
	for (var i=0; i<myFormObj.elements.length; i++) {
		myElementName = myFormObj.elements[i].name;
		for (var j=0; j<myFormObj.elements.length; j++) {
			if (myFormObj.elements[j].name == myElementName) {
				myCounter++;
				if (myFormObj.elements[j].type == "hidden") {
					isHidden = true;	
				}
			}
		}
		if ((myCounter>1) && (isHidden)) {
			myHiddenElementsArray.push(myFormObj.elements[i]);
		}
		myCounter = 0;
		isHidden = false;
	}
	
	if (myHiddenElementsArray.length>0) {
		var myHiddenElementsString = "";
		for (var k=0; k<myHiddenElementsArray.length; k++) {
			myHiddenElementsString += " - " + myHiddenElementsArray[k].name + 
			" ("+myHiddenElementsArray[k].type+")" + "\r\n";
		}
		alert("e1 Javascript Warning => Form Validation:" + WINDOWDELIM + 
			  "The following HIDDEN fields were found as duplicates: \r\n" + myHiddenElementsString);
	}
}


/***************************************************************************************************
* Private function.
*/
function displayDebug(rDebugOn, rSource, rComment) {
	
	if (rDebugOn) {
		alert("e1 Debug => "+ rSource +":" + WINDOWDELIM + rComment);
	}
}


/***************************************************************************************************
* Private function.
*/
function displayError(rSource, rComment) {
	
	alert("e1 Javascript Error => "+ rSource +":" + WINDOWDELIM + rComment);
}


/***************************************************************************************************
* Returns true if the array is valid, ie is not null and length is greater than 0.
*/
function isArrayValid(rSource, rArray) {

	if ((rArray == null) || (rArray.length < 1)) {
		displayError(rSource, 
			"Invalid parameter passed in for rArray: \"" +  rArray + "\".");
		return false;
	}
	return true;
}


/***************************************************************************************************
* Returns true if String is valid, ie it is not null and also NOT empty.
*/
function isStringValid(rSource, rString) {

	if ((rString == null) || (rString == "") || (rString == " ")) {
		displayError(rSource, 
			"Invalid null or empty String parameter passed in with value: \"" +  rString + "\".");
		return false;
	}
	return true;
}


/***************************************************************************************************
* Returns true if object is valid, ie it is not null.
*/
function isObjectValid(rSource, rObject, rMessage) {

	if (rObject == null) {
		displayError(rSource, 
			"Null object \""+ rObject +"\" passed in.\r\n\r\n" + rMessage);
		return false;
	}
	return true;
}


/***************************************************************************************************
* Private function.
* Returns the Form Object given the form name.
* Used when we need to initialise all the input fields inside a reg page, and typically there was no user-
* generated event, hence we do not have a form object passed in from the event, and we have to 
* retrieve it from the form name.
*/
function getFormObj(rFormName) {

	displayDebug(DEBUG1, "getFormObj()", 
		"Getting default form object.");
	
	if (!isStringValid("getFormObj()", rFormName))
		return;

	// Look for this form first.
	var myNumFormsFound = 0;
	var myFormObj;
	for (var i=0; i<document.forms.length; i++) {
		myFormObj = document.forms[i];
		if (myFormObj.name == rFormName) {
			myNumFormsFound++;
		}
	}

	if (myNumFormsFound > 1) {
		displayError("getFormObj()", 
			"Invalid form name \"" + rFormName +  "\" passed in.  "+myNumFormsFound+" of these forms found.");
		return;
	}
	
	return myFormObj;
}


/***************************************************************************************************
* Private function.
* Given an input field name, returns the corresponding object.
* NOTE: Assumes the field is inside a form labelled by the constant REGFORMNAME.
*/
function getInputFieldObj(rInputFieldName) {

	displayDebug(DEBUG1, "getInputFieldObj()", 
		"Getting element object for string: \"" + rInputFieldName + "\"");

	if (!isStringValid("getInputFieldObj()", rInputFieldName))
		return;
	
	var myFormObj = getFormObj(REGFORMNAME);
	var myArrObj = new Array();
	var myArrIndex = 0;
	var myInputFieldObj;
	
	// Look for the given element.
	for (var i=0; i<myFormObj.elements.length; i++) {
		myInputFieldObj = myFormObj.elements[i];
		if (myInputFieldObj.name == rInputFieldName) {
			myArrObj[myArrIndex++] = myInputFieldObj;
		}
	}
	
	if (myArrObj.length == 0) {
		displayError("getInputFieldObj()", 
			"Invalid element name \"" + rInputFieldName +  "\" passed in.");
		return;
	}
	else if (myArrObj.length == 1) {
		return myArrObj[0];
	}
	else {
		return myArrObj;
	}
}


/***************************************************************************************************
* Private function.
*/
function getInputFieldValue(rInputFieldObj) {
	
	displayDebug(DEBUG1, "getInputFieldValue()", 
		"Retrieving element value for object: \"" + rInputFieldObj + "\"");

	if (!isObjectValid("getInputFieldValue()", rInputFieldObj, 
						""))
		return;
		
	if (rInputFieldObj.type == "text")
		return rInputFieldObj.value;
	else if (rInputFieldObj.type == "textarea")
		return rInputFieldObj.value;
	else if (rInputFieldObj.type == "hidden")
		return rInputFieldObj.value;
	else if (rInputFieldObj.type == "checkbox")
		return rInputFieldObj.value;
	else if (rInputFieldObj.type == "radio")
		return rInputFieldObj.value;
	else if (rInputFieldObj.type == "select")
		return rInputFieldObj.value;
	else if (rInputFieldObj.type == "select-one")
		return rInputFieldObj.value;
	else if (rInputFieldObj.type == "file")
		return rInputFieldObj.value;
	else
		// Assume it is an array of fields, eg checkboxes or radio buttons.
		return getInputFieldsArrayCheckedValue(rInputFieldObj);
}


/***************************************************************************************************
* Private function.
* Returns the value of the first input field inside the given array that is checked.
* Could lead to a possible bug when processing array of checkboxes, as it will return the first
* checkbox value that is checked, and not the csv values of all checked checkboxes.
*/
function getInputFieldsArrayCheckedValue(rInputFieldsArray) {

	displayDebug(DEBUG1, "getInputFieldsArrayCheckedValue()", 
		"Getting array element value for array: \"" + rInputFieldsArray + "\"");

	if (!isArrayValid("getInputFieldsArrayCheckedValue()", rInputFieldsArray))
		return;
	
	for (var i=0; i<rInputFieldsArray.length; i++) {
		if (rInputFieldsArray[i].checked == true)
			return rInputFieldsArray[i].value;
	}
	return "";
}


/***************************************************************************************************
* Private function.
* Writes the given comment to the SPAN defined by the field name.
* Eg if the field name is "NeedHotel" then the span should be:
* <span id="NeedHotel_Comments"> </span>
* If this span is not defined in the html file, an error will be thrown here.
* 
* NOTE: THis method assumes the CSS Style
*/
function writeComments(rInputFieldName, rDisplayIconFilename, rComment) {

	displayDebug(DEBUG1, "writeComments()", 
		 "Writing comment: \"" + rComment + "\"");

	if (!isStringValid("writeComments()", rInputFieldName))
		return;

	if (!isObjectValid("writeComments()", rDisplayIconFilename, 
						""))
		return;

	if (!isObjectValid("writeComments()", rComment, 
						""))
		return;

	var mySpanObj = document.getElementById(rInputFieldName + "_Comments");
	if (!isObjectValid("writeComments()", mySpanObj, 
						"PARAM: InputFieldName: "+rInputFieldName+"\r\n"+
						"PARAM: DisplayIconName: "+rDisplayIconFilename+"\r\n"+
						"PARAM: Comment: "+rComment+"\r\n\r\n" + 
						"TIP: Did you declare the span tag for the comments, ie \r\n" +
						"<span id=\""+rInputFieldName+"_Comments\"> </span>"))
		return;
						
	if (rDisplayIconFilename != DISPLAYICON_NONE) {
		mySpanObj.innerHTML = 
			"<img width=18 height=18 src=\"/e1/view/res_pics/"+ rDisplayIconFilename +"\"> " +
			"<span class=\"SPAN_JavascriptComment\">" +
			rComment + 
			"</Span>" +
			"<br><br>";
	}
	else {
		mySpanObj.innerHTML = 
			"<span class=\"SPAN_JavascriptComment\">" +
			rComment + 
			"</Span>" +
			"<br><br>";
	}
}


/***************************************************************************************************
* Private function.
* Writes the given fields to the given span as hidden HTML elements.
*/
function writeHiddenFields(rInputFieldName, rInputFieldsArr) {
	
	displayDebug(DEBUG1, "writeHiddenFields()", 
		 "Writing hidden fields for field name: \"" + rInputFieldName + "\"");

	if (!isStringValid("writeHiddenFields()", rInputFieldName))
		return;

	if (!isArrayValid("writeHiddenFields()", rInputFieldsArr))
		return;
	
	// Loop through each name inside the input array, extract it, then convert it to an object, and enable it.
	var myInputFieldName;
	var myInputFieldObj;
	var myHtml = "";
	for (var i=0; i<rInputFieldsArr.length; i++) {
		myInputFieldName = rInputFieldsArr[i];
		myHtml += "<input type=\"hidden\" name=\""+ myInputFieldName +"\" value=\"\"> ";
	}

	var mySpanObj = document.getElementById(rInputFieldName + "_HiddenFields");
	if (!isObjectValid("writeHiddenFields()", mySpanObj, 
						"PARAM: InputFieldName: "+rInputFieldName+"\r\n"+
						"PARAM: InputFieldsArr: "+rInputFieldsArr+"\r\n\r\n"+
						"TIP: Did you declare the span tag for the hidden fields, ie \r\n" +
						"<span id=\""+rInputFieldName+"_HiddenFields\"> </span>"))
		return;
		
	mySpanObj.innerHTML = myHtml;
}


/***************************************************************************************************
* Private function.
*/
function clearHiddenFields(rInputFieldName) {

	displayDebug(DEBUG1, "clearHiddenFields()", 
		 "Clearing hidden fields for field: \"" + rInputFieldName + "\"");

	if (!isStringValid("clearHiddenFields()", rInputFieldName))
		return;
	
	var mySpanObj = document.getElementById(rInputFieldName + "_HiddenFields");
	if (!isObjectValid("clearHiddenFields()", mySpanObj, 
						"PARAM: InputFieldName: "+rInputFieldName+"\r\n"+
						"TIP: Did you declare the span tag for the hidden fields, ie \r\n" +
						"<span id=\""+rInputFieldName+"_HiddenFields\"> </span>"))
		return;
		
	mySpanObj.innerHTML = "";
}


/***************************************************************************************************
* Private function.
*/
function disableInputField(rInputFieldObj) {

	displayDebug(DEBUG1, "disableInputField()", 
		 "Disabling input field: \"" + rInputFieldObj + "\"");

	if (!isObjectValid("disableInputField()", rInputFieldObj, 
						""))
		return;

	displayDebug(DEBUG2, "disableInputField()", 
		"Disabling input field: " + rInputFieldObj + " of name \"" +
		rInputFieldObj.name + "\" and of type \"" + 
		rInputFieldObj.type + "\".");

	if (rInputFieldObj.type == "text")
		rInputFieldObj.disabled = true;
		
	else if (rInputFieldObj.type == "textarea")
		rInputFieldObj.disabled = true;
		
	else if (rInputFieldObj.type == "hidden")
		rInputFieldObj.disabled = true;
		
	else if (rInputFieldObj.type == "checkbox")
		rInputFieldObj.disabled = true;
		
	else if (rInputFieldObj.type == "radio")
		rInputFieldObj.disabled = true;
		
	else if (rInputFieldObj.type == "select")
		rInputFieldObj.disabled = true;
		
	else if (rInputFieldObj.type == "select-one")
		rInputFieldObj.disabled = true;
		
	else if (rInputFieldObj.type == "file")
		rInputFieldObj.disabled = true;
	else
		disableInputFieldsArray(rInputFieldObj);
}


/***************************************************************************************************
* Private function.
*/
function disableInputFieldsArray(rInputFieldsArray) {

	displayDebug(DEBUG1, "disableInputFieldsArray()", 
		 "Disabling input fields for array: \"" + rInputFieldsArray + "\"");

	if (!isArrayValid("disableInputFieldsArray()", rInputFieldsArray))
		return;
	
	for (var i=0; i<rInputFieldsArray.length; i++) {
		if (rInputFieldsArray[i].type == "text")
			rInputFieldsArray[i].disabled = true;
			
		else if (rInputFieldsArray[i].type == "textarea")
			rInputFieldsArray[i].disabled = true;
			
		else if (rInputFieldsArray[i].type == "hidden")
			rInputFieldsArray[i].disabled = true;
			
		else if (rInputFieldsArray[i].type == "checkbox")
			rInputFieldsArray[i].disabled = true;
			
		else if (rInputFieldsArray[i].type == "radio")
			rInputFieldsArray[i].disabled = true;
			
		else if (rInputFieldsArray[i].type == "select")
			rInputFieldsArray[i].disabled = true;
			
		else if (rInputFieldsArray[i].type == "select-one")
			rInputFieldsArray[i].disabled = true;
			
		else if (rInputFieldsArray[i].type == "file")
			rInputFieldsArray[i].disabled = true;
	}
}


/***************************************************************************************************
* Private function.
*/
function enableInputFieldsArray(rInputFieldsArray) {

	displayDebug(DEBUG1, "enableInputFieldsArray()", 
		 "Enabling array element for object: \"" + rInputFieldsArray + "\"");
	
	if (!isArrayValid("enableInputFieldsArray()", rInputFieldsArray))
		return;
	
	for (var i=0; i<rInputFieldsArray.length; i++) {
		if (rInputFieldsArray[i].type == "text")
			rInputFieldsArray[i].disabled = false;
		else if (rInputFieldsArray[i].type == "textarea")
			rInputFieldsArray[i].disabled = false;
		else if (rInputFieldsArray[i].type == "hidden")
			rInputFieldsArray[i].disabled = false;
		else if (rInputFieldsArray[i].type == "checkbox")
			rInputFieldsArray[i].disabled = false;
		else if (rInputFieldsArray[i].type == "radio")
			rInputFieldsArray[i].disabled = false;
		else if (rInputFieldsArray[i].type == "select")
			rInputFieldsArray[i].disabled = false;
		else if (rInputFieldsArray[i].type == "select-one")
			rInputFieldsArray[i].disabled = false;
		else if (rInputFieldsArray[i].type == "file")
			rInputFieldsArray[i].disabled = false;
	}
}


/***************************************************************************************************
* Private function.
*/
function enableInputField(rInputFieldObj) {

	displayDebug(DEBUG1, "enableInputField()", 
		 "Enabling input field for object: \"" + rInputFieldObj + "\"");

	if (!isObjectValid("enableInputField()", rInputFieldObj, 
						""))
		return;

	displayDebug(DEBUG2, "enableInputField()", 
		 "Enabling this input field: " + rInputFieldObj + " of name \"" +
			rInputFieldObj.name + "\" and of type \"" + 
			rInputFieldObj.type + "\".");

	if (rInputFieldObj.type == "text")
		rInputFieldObj.disabled = false;
	else if (rInputFieldObj.type == "textarea")
		rInputFieldObj.disabled = false;
	else if (rInputFieldObj.type == "hidden")
		rInputFieldObj.disabled = false;
	else if (rInputFieldObj.type == "checkbox")
		rInputFieldObj.disabled = false;
	else if (rInputFieldObj.type == "radio") {
		rInputFieldObj.disabled = false;
	}
	else if (rInputFieldObj.type == "select") {
		rInputFieldObj.disabled = false; /* Need a way to make this element visible in IE */
		rInputFieldObj.visibility = "visible";
	}
	else if (rInputFieldObj.type == "select-one") {
		rInputFieldObj.disabled = false;
	}
	else if (rInputFieldObj.type == "file")
		rInputFieldObj.disabled = false;
	else
		// For radio buttons and checkboxes
		enableInputFieldsArray(rInputFieldObj);
}


/***************************************************************************************************
* Private function.
*/
function resetInputField(rInputFieldObj) {
	
	displayDebug(DEBUG1, "resetInputField()", 
		 "Resetting element for object: \"" + rInputFieldObj + "\"");

	if (!isObjectValid("resetInputField()", rInputFieldObj, 
						""))
		return;

	displayDebug(DEBUG2, "resetInputField()", 
		 "Resetting this object: " + rInputFieldObj + " of name \"" +
			rInputFieldObj.name + "\" and of type \"" + 
			rInputFieldObj.type + "\".");
	
	if (rInputFieldObj.type == "text")
		rInputFieldObj.value = "";
	else if (rInputFieldObj.type == "textarea")
		rInputFieldObj.value = "";
	else if (rInputFieldObj.type == "hidden")
		rInputFieldObj.value = "";
	else if (rInputFieldObj.type == "checkbox")
		rInputFieldObj.checked = false;
	else if (rInputFieldObj.type == "radio")
		rInputFieldObj.checked = false;
	else if (rInputFieldObj.type == "select")
		rInputFieldObj.value = -1;
	else if (rInputFieldObj.type == "select-one")
		rInputFieldObj.value = -1;
	else if (rInputFieldObj.type == "file")
		rInputFieldObj.value = "";
	else
		// Assume it is an array of fields, eg checkboxes or radio buttons.
		resetInputFieldsArray(rInputFieldObj);
}


/***************************************************************************************************
* Private function.
*/
function resetInputFieldsArray(rInputFieldsArray) {

	displayDebug(DEBUG1, "resetInputFieldsArray()", 
		 "Resetting input fields for array: \"" + rInputFieldsArray + "\"");
	
	if (!isArrayValid("resetInputFieldsArray()", rInputFieldsArray))
		return;

	for (var i=0; i<rInputFieldsArray.length; i++) {
		if (rInputFieldsArray[i].type == "text")
			rInputFieldsArray[i].value = "";
		else if (rInputFieldsArray[i].type == "textarea")
			rInputFieldsArray[i].value = "";
		else if (rInputFieldsArray[i].type == "hidden")
			rInputFieldsArray[i].value = "";
		else if (rInputFieldsArray[i].type == "checkbox")
			rInputFieldsArray[i].checked = false;
		else if (rInputFieldsArray[i].type == "radio")
			rInputFieldsArray[i].checked = false;
		else if (rInputFieldsArray[i].type == "select")
			rInputFieldsArray[i].value = -1;
		else if (rInputFieldsArray[i].type == "select-one")
			rInputFieldsArray[i].value = -1;
		else if (rInputFieldsArray[i].type == "file")
			rInputFieldsArray[i].value = "";
	}
}


/***************************************************************************************************
* Private function.
*/
function resetInputFields(rInputFieldsArr) {

	displayDebug(DEBUG1, "resetInputFields()", 
		 "Resetting elements for input array: \"" + rInputFieldsArr + "\"");

	if (!isArrayValid("resetInputFields()", rInputFieldsArr))
		return;

	// Loop through each name inside the input array, extract it, then convert it to an object, and enable it.
	var myInputFieldName;
	var myInputFieldObj;
	for (var i=0; i<rInputFieldsArr.length; i++) {
		
		myInputFieldName = rInputFieldsArr[i];
		myInputFieldObj = getInputFieldObj(myInputFieldName);
		resetInputField(myInputFieldObj);
	}
}


/***************************************************************************************************
* Public function.
* When enabling form elements, we also need to clear the corresponding hidden elements.
* These hidden elements are already set inside the given span, hence the need to pass the span name.
*/
function enableInputFields(rInputFieldName, rInputFieldsArr, rIconFilename, rComment) {

	displayDebug(DEBUG1, "enableInputFields()", 
		 "Enabling fields for field name: \"" + rInputFieldName + "\"");

	if (!isArrayValid("enableInputFields()", rInputFieldsArr))
		return;

	// First we clear the hidden fields inside the given span.
	clearHiddenFields(rInputFieldName);

	// Loop through each name inside the input array, extract it, then convert it to an object, and enable it.
	var myInputFieldName;
	var myInputFieldObj;
	for (var i=0; i<rInputFieldsArr.length; i++) {
		myInputFieldName = rInputFieldsArr[i];
		myInputFieldObj = getInputFieldObj(myInputFieldName);
		enableInputField(myInputFieldObj);
	}

	// Write the comment.
	writeComments(
		rInputFieldName, 
		rIconFilename, 
		rComment);
}


/***************************************************************************************************
* Public function.
* When disabling form elements, we also need to set the corresponding hidden elements.
* These hidden elements are set inside the given span, hence the need to pass the span name.
*/
function disableInputFields(rInputFieldName, rInputFieldsArr, rIconFilename, rComment) {

	displayDebug(DEBUG1, "disableInputFields()", 
		 "Disabling fields for field name: \"" + rInputFieldName + "\"");

	if (!isArrayValid("disableInputFields()", rInputFieldsArr))
		return;
		
	// First set the values to blank.
	resetInputFields(rInputFieldsArr);
	
	var myInputFieldName;
	var myInputFieldObj;
	for (var i=0; i<rInputFieldsArr.length; i++) {
		myInputFieldName = rInputFieldsArr[i];
		myInputFieldObj = getInputFieldObj(myInputFieldName);
		disableInputField(myInputFieldObj);
	}
	
	// Set the hidden fields.
	writeHiddenFields(rInputFieldName, rInputFieldsArr);
	
	// Write the comment.
	writeComments(
		rInputFieldName, 
		rIconFilename, 
		rComment);
}


/*
function disableInputFieldsNew(rInputFieldName, rInputFieldsArr, rIconFilename, rComment, rSpanContent) {
	
	disableInputFields(rInputFieldName, rInputFieldsArr, rIconFilename, rComment)
	hideSpan(rInputFieldName, rSpanContent);
}

function enableInputFieldsNew(rInputFieldName, rInputFieldsArr, rIconFilename, rComment, rSpanContent) {
	
	enableInputFields(rInputFieldName, rInputFieldsArr, rIconFilename, rComment)
	showSpan(rInputFieldName, rSpanContent);
}

function hideSpan(rInputFieldName, rSpanContent) {

	var mySpanObj = document.getElementById(rInputFieldName + "_Span");
	rSpanContent = mySpanObj.innerHTML;
	mySpanObj.innerHTML = "";
}

function showSpan(rInputFieldName, rSpanContent) {
	
	var mySpanObj = document.getElementById(rInputFieldName + "_Span");
	mySpanObj.innerHTML = rSpanContent;
}
*/
